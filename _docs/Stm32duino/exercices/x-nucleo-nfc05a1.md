---
title: Projet d'identification radiofréquence avec une X-NUCLEO-NFC05A1
description: Projet d'identification radiofréquence avec une X-NUCLEO-NFC05A1 sous STM32duino
---

# Projet de serrure à identification radiofréquence (RFID) avec une X-NUCLEO-NFC05A1

Ce tutoriel est un petit projet utilisant [la technologie de radio-identification (RFID)](https://fr.wikipedia.org/wiki/Radio-identification) de STMicroelectronics, à laide de la carte d'extension [X-NUCLEO-NFC05A1](https://www.st.com/en/ecosystems/x-nucleo-nfc05a1.html).

Plus précisément, **nous souhaitons simuler un dispositif de type "serrure RFID", avec contrôle d'accès**. L'utilisateur dispose d'un badge identifié à l'aide d'un numéro unique (associé à son nom). Lorsque ce badge est lu par la X-NUCLEO-NFC05A1, le montage interroge une base de donnée et, si le badge y est enregistré, il y mémorise la date et l'heure, puis il prend une décision (dans notre cas ce sera activer un petit servomoteur pour simuler l'ouverture d'une serrure).

## Matériel requis

La réalisation de ce système nécessitera la mise œuvre de plusieurs modules et technologies :

1. Un lecteur [RFID X-NUCLEO-NFC05A1](https://www.st.com/en/ecosystems/x-nucleo-nfc05a1.html) et plusieurs [badges RFID de type ISO/IEC 14443A/B (13.56 MHz)](https://boutique.semageek.com/fr/248-tag-rfid-mifare-1356mhz-3007245738481.html).
2. L'horloge temps réel (RTC) intégrée au microcontrôleur choisi (le STM32L476RG) et [un module Grove UART Wi-Fi V2](https://wiki.seeedstudio.com/Grove-UART_Wifi_V2/) (avec un point d'accès à Internet) pour remettre à l'heure la RTC de temps en temps.
3. Un module lecteur de carte SD sur bus SPI (par exemple [celui d'Adafruit](https://www.adafruit.com/product/4682)) et une carte SD de petite capacité (4Gb seront infinment plus que nécessaire !) pour enregistrer la base de données des accès. **Attention**, [le shield pour carte SD de Seeed Studio](https://wiki.seeedstudio.com/SD_Card_shield_V4.0/) ne fonctionne pas avec les cartes NUCLEO de STMicroelectronics (mauvais câblage pour le bus SPI).
4. Un afficheur à cristaux liquides [Grove LCD 16x2](lcd_16x2) pour interagir avec l'utilisateur.
5. Un petit [module Grove servomoteur](servomoteur) pour simuler l'ouverture d'une serrure.
6. Une carte programmable à microcontrôleur [NUCLEO-L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html) pour piloter l'ensemble. N'importe quelle autre carte NUCLEO ferait l'affaire à condition de modifier les broches si nécessaire, pour connecter le module Wi-Fi sur un UART du microcontrôleur. 

## Exercice 1 : Lecture de l'identifiant unique d'un badge NFC avec la X-NUCLEO-NFC05A1

## Exercice 2 : Réglage de la RTC du STM32L476RG avec le protocole NTP

## Exercice 3 : Ecriture et recherche d'enregistrements structurés sur une carte SD

## Exercice 4 : Réalisation de la serrure RFID

