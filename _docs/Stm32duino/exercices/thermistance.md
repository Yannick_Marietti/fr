---
title: Capteur de température (thermistance)
description: Mise en œuvre du module capteur de température Grove v1.2 avec STM32duino
---

# Capteur de température (thermistance)

Ce tutoriel explique comment mettre en œuvre un [module capteur de température Grove v1.2 avec STM32duino](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/). Ce module est constitué d'une **thermistance à coefficient de température négatif** de type NCP18WF104F03RC dont vous trouverez la fiche technique [ici](https://files.seeedstudio.com/wiki/Grove-Temperature_Sensor_V1.2/res/NCP18WF104F03RC.pdf). Vous trouverez une vidéo à propos des thermistances [ici](https://youtu.be/wjL7xOGqAqg).

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur positionné sur 3.3V**. 
2. Une carte NUCLEO de STMicroelectronics supportée par STM32duino (toutes le sont !)
3. Un [module Grove - Temperature Sensor v1.2](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/)

**Le module Grove Temperature Sensor v1.2 :**
<div align="center">
<img alt="Grove NCP18WF104F03RC" src="images/8_temperature/capteur_temp.png" width="250px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Dans sa plage de fonctionnement linéaire (-40 à 125 °C, avec une précision de 1,5 °C), la résistance de la thermistance reste proportionnelle à la température ambiante. Cette résistance, en variant, fait également varier la tension de sortie d'un pont diviseur de tension qui intégré au module Grove. Cette tension est numérisée (on dit plutôt échantillonnée) par un ADC via une broche d'entrée analogique (dans notre exemple, on connecte le module à la broche A0). Notre programme devra recalculer la valeur de la température à partir de la conversion de l'ADC et l'afficher dans le terminal série.

## Bibliothèque(s) requise(s)

Aucune bibliothèque n'est nécessaire pour cet exemple, le sketch qui suit est suffisant.

## Le sketch Arduino

> **Le sketch ci-dessous peut être [téléchargé par ce lien](SKETCHS.zip)**.

Lancez l'IDE Arduino, ouvrez un nouveau sketch vide et copiez-y le code qui suit :

```c
/* Objet du sketch :
Mesure de température à partir d'une thermistance d'un module Grove Temperature sensor V1.2 relié à l'entrée A0
L'ADC interne fonctionnant sur 3.3V, le capteur doit être alimenté en 3.3V sur la carte d'adaptation Grove.
*/

// Définition des constantes necessaires pour la conversion.
const float R1 = 10000;
float logR2, R2, T;
// Coefficients de Steinhart-Hart pour la conversion
const float c1 = 0.001129148, c2 = 0.000234125, c3 = 0.0000000876741; 

void setup() {
  Serial.begin(9600); // On initialise l'UART du terminal série à 9600 bauds
  pinMode(A0,INPUT);  // On met le capteur en entrée analogique
}

void loop() {
  uint16_t Vo = analogRead(A0);  // Lecture de la valeur échnatillonnée par l'ADC
  R2 = R1 * (1023.0 / (float)Vo - 1.0);               // Calcule la résistance du thermistor
  logR2 = log(R2);                                    // Conversion implicite de Vo en float pour le calcul
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2)); // Convertit la température en Kelvin.
  T = T - 273.15;                                     // Convertit des Kelvins en Celcius.
 // T = (T * 9.0)/ 5.0 + 32.0;                        // Convertit des Celsius en Fahrenheits.

  Serial.print("Temperature: ");
  Serial.print(T,1); // Une seule décimale après la virgule
  //Serial.println(" K");
  Serial.println(" C");
  //Serial.println(" F");

  delay(5000); // Temporisation de cinq secondes
}
```
Après avoir vérifié que l'IDE Arduino est correctement configurée, notamment qu'elle est connectée au port COM attribué au ST-LINK de votre carte (sous Windows), cliquez sur "Téléverser". **Assurez-vous également que le commutateur d'alimentation électrique du Grove base shield est bien positionné sur 3.3V**.

## Affichage de la température sur le terminal série

Si tout s'est passé correctement vous devriez observer les mesures de température qui défilent sur le terminal série de l'IDE Arduino, à l'écoute du ST-LINK, toutes les cinq secondes. 
La précision du ce capteur est étonnante dans notre test : 21.1 - 21.2°C selon lui, à comparer à 21.2°C selon [un système de mesure Velleman DEM500](https://www.velleman.eu/products/view/?country=fr&lang=fr&id=420578) !
