---
title: Afficheur Grove LCD 16 caractères x 2 lignes avec rétroéclairage RGB
description: Mettre en œuvre un afficheur Grove LCD 16 caractères x 2 lignes avec rétroéclairage RGB sous STM32duino
---
# Afficheur Grove LCD 16x2 avec rétroéclairage RGB
