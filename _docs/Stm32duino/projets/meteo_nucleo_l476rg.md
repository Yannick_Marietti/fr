---
title: Construire une station météo connectée avec STM32duino
description: Tutoriel pour construire une station météo connectée avec STM32duino sur la base d'une carte NUCLEO-L476RG
---

# Construire une station météo connectée avec STM32duino

Ce tutoriel vous apprendra comment construire une station météo connectée avec l’API STM32duino, portage de l’API Arduino sur l’architecture STM32 de STMicroelectronics, [disponible ici](https://GitHub.com/stm32duino). Il vous apportera les notions de base pour mieux comprendre et mettre en pratique la programmation des microcontrôleurs.

Ce tutoriel est avant tout à vocation pédagogique, il se concentre exclusivement sur la partie électronique - hardware et software - de la station météo et n’aborde pas les problématiques de la création de son boitier, du bon positionnement de celle-ci et de ses capteurs, des précautions à prendre pour que les valeurs mesurées soient fiables et reproductibles. Vous trouverez aisément une vaste documentation sur ces sujets qui doivent impérativement être pris en compte si vous souhaitez fabriquer un objet de qualité professionnelle.

Tout le logiciel qui sera utilisé est libre ; ce document étant une synthèse inspirée et adaptée d’exemples trouvés sur Internet. La plupart des sketchs Arduino disponibles en ligne pour des objets connectés font l’impasse sur des fonctions bien utiles, par souci de concision ou pour rester compatible avec des cartes de développement peu performantes. Cette dernière contrainte est levée par la carte Nucleo L476RG que nous avons sélectionnée car elle est équipée d’un microcontrôleur disposant d’une mémoire de stockage et de ressources matérielles conséquentes auxquelles l’API STM32duino donne un accès presque complet.

A l’issue de ce tutoriel vous disposerez d’exemples et de compétences pour la conception de n’importe quel objet connecté amateur ; vous n’aurez qu’à changer les capteurs et les modules RF de votre station selon vos besoins et à apporter des modifications aux codes sources présentés. Vous pourrez les améliorer et les adapter à vos propres projets, et ils vous feront gagner un temps précieux.

> **Toutes les ressources (scripts et version PDF du tutoriel) de ce projet peuvent être téléchargées [par ce lien](meteo.zip).**

**Amusez-vous bien !**


## Partie 1 : le matériel

Nous décrivons dans cette première partie les composants électroniques de notre station météo. Nous nous focaliserons sur les informations utiles à cet objectif. Le tableau qui suit rappelle les **principaux composants et modules** que nous utiliserons pour notre station :

|Matériel|Description|Illustration|
|:-|:-|:-:|
|[NUCLEO-L476RG](https://www.st.com/en/evaluation-tools/nucleo-l476rg.html)|Carte de prototypage avec microcontrôleur STM32L476RG de STMicroelectronics|<img alt="NUCLEO-64" src="images/Nucleo64.jpg" width="360px">|
|[X-NUCLEO-IKS01A2](https://www.st.com/en/ecosystems/x-nucleo-iks01a2.html)|Carte d'extension avec capteurs MEMS environnementaux et de mouvements STMicroelectronics|<img alt="X-NUCLEO IKS01A2" src="images/iks01a2.jpg" width="150px">|
|[Module Grove SCD30](https://wiki.seeedstudio.com/Grove-CO2_Temperature_Humidity_Sensor-SCD30/)|Module Grove (société Seeed studio) de mesure du CO<sub>2</sub>, de l'humidité et de la température équipé d'un capteur Sensirion SCD30|<img alt="Grove SCD 30" src="images/scd30.jpg" width="250px">|
|[Module Wi-Fi ESP01](https://www.espressif.com/en/products/socs/esp8266)|Module Wi-Fi piloté par UART conçu avec une puce ESP8266 de la société Espressif|<img alt="ESP-01" src="images/ESP-01.jpg" width="150px">|
|Module Bluetooth HC-06|Module Bluetooth piloté par UART HC-06|<img alt="HC-06" src="images/hc-06.jpg" width="170px">|
|Module carte SD|Module carte SD (2GB) sur bus SPI ME126|<img alt="ME126" src="images/ME126.jpg" width="140px">|

Tous ces composants sont expliqués plus en détails dans les sections 1 à 4 qui suivent :

1. [La carte NUCLEO-L476RG](nucleo_l476rg)
2. [Les modules et capteurs externes](modules)
3. [Les périphériques intégrés au microcontrôleur STM32L476RG](stm32l476rg)
4. [Vue d’ensemble : connectivité, protocoles et services](protocoles_services)

## Partie 2 : Le logiciel

Dans cette deuxième partie, nous détaillons, pas à pas, la partie logicielle de notre station météo connectée. Après une revue des notions essentielles pour comprendre ce que fait l’IDE Arduino en arrière-plan nous examinerons différents sketchs STM32duino décrivant chacun une fonctionnalité de la station.

### Les chaînes de compilation et le modèle STM32duino

Vous trouverez [**en suivant ce lien**](toolchain_stm32duino) des informations et explications sur la chaîne de compilation de Arduino pour STM32.

### Revue des sketchs STM32duino

Cette section détaille le sketch STM32duino de notre station météo, qui comporte plus de 1700 lignes. J’ai pris le parti d’expliquer les différentes fonctionnalités qu’il contient (enregistrement sur carte SD, mode basse consommation, etc.) paragraphe après paragraphe, en ajoutant celles-ci l’une après l’autre, jusqu’à aboutir au sketch complet. Pour ne pas augmenter de façon déraisonnable le nombre de pages de ce document, les sketchs n’y sont pas intégralement recopiés à chaque étape, je détaille seulement les changements importants et les fonctions ajoutées. Bien évidemment tous les sketchs sont disponibles et vous pourrez les consulter, les compiler et les charger sur la carte NUCLEO-L476RG à loisir

L’**installation de STM32duino dans l’IDE Arduino** est expliquée sur [cette page](https://github.com/stm32duino/Arduino_Core_STM32/wiki). Je supposerai que vous disposez déjà d’un environnement de développement configuré correctement et fonctionnel pour compiler et charger les sketchs dans la carte NUCLEO. Je déconseille vivement aux débutants d’utiliser d’autres IDE que celle d’Arduino ; la bonne configuration d’IDE alternatives, qui apportent notamment le débogage en temps réel ou l’auto-complétion, telles que [Platform IO](https://docs.platformio.org/en/latest/what-is-platformio.html), n’étant pas évidente compte tenu de leur faible niveau de maturité à la date où ce tutoriel est rédigé.

Je précise enfin que ce tutoriel **n’a pas pour objectif de vous enseigner la programmation**. Il suppose que vous êtes relativement à l’aise avec les bases des langages C et C++ et ne prétend pas être un modèle en la matière. Le code présenté se veut fonctionnel, raisonnablement clair et peu optimisé de ce fait. Mais il contient très certainement pléthores d’erreurs et faiblesses qui seront corrigées au fil du temps, dans des révisions futures du présent document.
    
 > L'ensemble des sketchs de notre projet peuvent être [**téléchargés ici**](meteo.zip).

 - [**Lire les capteurs environnementaux** de température, pression, humidité relative et CO<sub>2</sub>](sketch1)
 - [**Enregistrer les mesures sur une carte SD** pour pouvoir les exploiter après](sketch2)
 - [**Utiliser le mode Basse Consommation** pour économiser l'énergie](sketch3)
 - [**Utiliser le chien de garde indépendant** pour relancer l'application en cas de "freeze"](sketch4)
 - [**Configurer et connecter le Wi-Fi** pour poster plus tard les mesures en ligne](sketch5)
 - [**Gérer la date, l’heure et le fuseau horaire** pour mettre automatiquement l'horloge de la carte à l'heure à partir d'Internet](sketch6)
 - [**Utiliser le protocole MQTT avec ThingsBoard**, pour poster les mesures sur un tableau de bord accessible par Internet](sketch7)
 - [**Utiliser le protocole HTTP avec OpenWeather**, pour obtenir des prévisions météorologiques](sketch8)
 - [**Communication série en Bluetooth** pour paramétrer et piloter la station météo avec un smartphone](sketch9)
 - [**Sauvegarder des paramètres dans la mémoire Flash** pour pouvoir les changer sans avoir à recompiler le firmware](sketch10)
 - [**Le sketch complet**](sketch_final)

## Conclusion et perspectives

L’étude de cas que nous avons réalisée par ce tutoriel s’est délibérément concentrée sur une carte NUCLEO-L476RG et des protocoles de communication bien documentés et faciles à mettre en œuvre dans un objectif pédagogique. Si vous avez assimilé toutes les notions évoquées, les variantes et les possibilités de perfectionnement de la station météo qui s’offrent à vous sont pléthoriques !

Voici quelques améliorations et développements que vous pourriez envisager :

 * **Ajouter un afficheur e-Paper sur l’un des UART encore libres**, [par exemple celui-ci](https://www.waveshare.com/wiki/4.3inch_e-Paper_UART_Module#Resources), pour que les données mesurées par la station et les prévisions météos obtenues par OpenWeather soient consultables d’un coup d’œil.

  * **Installer un serveur ThingsBoard complet sur un ordinateur local** (un PC sous Linux ou un Raspberry Pi par exemple) que vous configureriez et connecteriez à Internet, selon [les instructions données sur ce site](http://www.steves-internet-guide.com/thingsboard-mqtt-dashboard/).

  * **Exploiter les « callbacks » du protocole MQTT**. Il serait également possible d’envoyer des notifications ou des commandes à la station en exploitant la communication bidirectionnelle offerte par le protocole MQTT, comme sur [cet exemple](https://eskimon.fr/faire-communiquer-son-arduino-avec-un-appareil-android).

 * Remplacer le logiciel émulateur de terminal RS232 utilisé pour communiquer en Bluetooth avec la station par **un programme sur votre smartphone** Android plus élégant, réalisé avec l’application [MIT App Inventor](https://appinventor.mit.edu/).
	
 * Remplacer la carte Nucleo L476RG et le module HC-06 par [**une carte Nucleo WB55**](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html) qui communique nativement en [Bluetooth basse consommation](http://tvaira.free.fr/bts-sn/activites/activite-ble/bluetooth-ble.html) à l’aide de la bibliothèque [STM32duinoBLE](https://GitHub.com/stm32duino/STM32duinoBLE) et d’une application cliente pour votre smartphone, écrite avec MIT App Inventor.

 * Ajouter **un module extérieur qui communique en [LoRa](https://fr.wikipedia.org/wiki/LoRaWAN)** ou en WiFi avec la station, sur le modèle des [stations météo Netatmo](https://www.netatmo.com/fr-fr/weather/weatherstation). Vous pourriez par exemple utiliser une carte [Nucleo WL55](https://www.st.com/content/st_com/en/products/evaluation-tools/product-evaluation-tools/mcu-mpu-eval-tools/stm32-mcu-mpu-eval-tools/stm32-nucleo-boards/nucleo-wl55jc.html).

 * Prendre le temps de concevoir et imprimer en 3D un joli boitier pour la station de sorte qu’elle ressemble à un véritable produit finit plutôt qu’à un montage expérimental pour agrémenter le décor d’un documentaire de ARTE sur les savants fous !

 * Etc.

Si vous lisez ces dernières lignes, alors mon objectif est atteint : vous disposez désormais des connaissances de base qui vous permettront d’envisager sereinement le développement de vos futurs objets connectés. Si vous avez l’ambition de persévérer en programmation embarquée avec l’architecture STM32, nous vous conseillons de compléter votre apprentissage par celui de [l’environnement STM32 Cube et l’API HAL](https://www.st.com/content/st_com/en/support/learning/stm32-education/stm32-step-by-step.html).

**Continuez à bien vous amuser !**

## Annexes
1. [Mappage périphériques - broches pour la NUCLEO-L476RG](nucleo_l476rg_pins)
2. [Configuration du module HC-06 avec la NUCLEO-L476RG](config_hc06)
3. [Configuration du module ESP-01 avec la NUCLEO-L476RG](config_esp01)
4. [Anatomie du microcontrôleur STM32L476RG](glossaire_l476rg)

