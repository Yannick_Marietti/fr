---
title: Projets et applications avec STM32duino
description: Liste des projets et applications avec STM32duino
---

# Projets et applications avec STM32duino

## Station météo connectée avec une carte NUCLEO-L476RG

[**Ce tutoriel**](meteo_nucleo_l476rg) explique pas à pas comment réaliser une station météo connectée sur la base d'une carte NUCLEO-L476RG de STMicroelectronics.
