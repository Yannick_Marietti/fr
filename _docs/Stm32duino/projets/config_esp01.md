---
title: Configuration du module ESP-01 avec la NUCLEO-L476RG
description: Configuration du module ESP-01 avec la NUCLEO-L476RG
---

# Configuration du module ESP-01 avec la NUCLEO-L476RG

Pour changer son débit de communication (baudrate) vous pourriez être amenés à reconfigurer votre module ESP-01 avec des commandes « AT ». La liste des commandes « AT » pour l’ESP-01 est disponible [ici](https://www.espressif.com/sites/default/files/documentation/4a-esp8266_at_instruction_set_en.pdf). Une explication de la façon de procéder est donnée [ici](https://www.instructables.com/ESP8266-WiFi-Module-for-Dummies/). Rien de complexe : il suffit d’envoyer les bonnes commandes au module via son port série. Le script qui suit vous permettra d’envoyer des commandes « AT » sur un module ESP-01 connecté aux broches PD2 et PC12 de l’UART5 d’une carte NUCLEO-L476RG.

```c++
HardwareSerial esp01(PD2, PC12);

void setup(){

  Serial.begin(9600);
  Serial.println("ENTER AT Commands:");
  esp01.begin(115200);
}

void loop(){
  
  if (esp01.available()){
    Serial.write(esp01.read());
  }
  
  if (Serial.available()){
    esp01.write(Serial.read());
  }  
}
```
