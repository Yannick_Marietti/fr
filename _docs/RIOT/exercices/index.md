---
title: Exercices MicroPython avec RIOT OS pour la carte P-NUCLEO-WB55 et NUCLEO-WL55JC
description: Exercices MicroPython avec RIOT OS pour la carte P-NUCLEO-WB55 et NUCLEO-WL55JC

---
# Exercices MicroPython avec RIOT OS pour les cartes P-NUCLEO-WB55 et NUCLEO-WL55

Les exercices avec RIOT OS pour les cartes P-NUCLEO-WB55 et NUCLEO-WL55 arrivent très vite !

Ce [répertoire](https://gitlab.com/stm32python/assets/-/tree/master/public/RIOT/exercices) contient quelques scripts MicroPython pour les cartes P-NUCLEO-WB55 et NUCLEO-WL55JC.

En attendant, vous pouvez suivre les exercices du [cours RIOT](https://GitHub.com/RIOT-OS/riot-course/blob/master/README.md) en utilisant `BOARD=p-nucleo-wb55` et `BOARD=nucleo-wl55jc`.

## Hello World !

Depuis la console (PuTTY sur Windows, pyterm ou minicom sur Linux/MacOSX), entrez les instructions Python suivantes:
```python
print("Hello World " * 6)
```

Le résultat est :
```python
>>> print("Hello World " * 6)
Hello World Hello World Hello World Hello World Hello World Hello World  
```

## Clignotement des 3 DELs

Bientôt !

## Utilisation des 3 boutons SM1, SM2 et SM3

Bientôt !

