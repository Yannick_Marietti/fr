---
title: Téléchargement STM32python
description: Liens de téléchargement des firmwares et tutoriels, liens vers des ressources sur des sites partenaires

---
# Téléchargements STM32python

## Logiciels pour développer

 - [Notepad++](https://notepad-plus-plus.org/)
 - [TeraTerm](https://osdn.net/projects/ttssh2/)
 - [Thonny](https://thonny.org/)
 - [PuTTY](https://www.PuTTY.org/)
 - [PyScripter](https://sourceforge.net/projects/pyscripter/)
 - [Mu](https://codewith.mu/) (avec le mode Pyboard)
 - [Geany](https://www.geany.org/)

## Firmware(s)

  - [Firmware 1.19.1](../../assets/firmware/fw_upy_nucl_wb55_1.19.1.bin) (en test, aucun problème relevé)
  - [Firmware 1.17](../../assets/firmware/fw_upy_nucl_wb55_1.17.bin) (éprouvé)

## Logiciels pour manipuler la flash des STM32

Dans quelques circonstances (décrites au fil des tutoriels) vous pourrez être amené à effacer la mémoire flash du microcontrôleur STM32 sur votre carte NUCLEO, ou encore à mettre à jour les firmwares d'autres composants sur celle-ci. Pour ces opérations vous aurez besoin du logiciel [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) et de [ce tutoriel](../Kit/stm32cubeprogrammer).

## Scripts MicroPython pour les tutoriels de ce site

- **Tutoriels pour démarrer avec la carte NUCLEO-WB55**
Vous trouverez [ici](../../assets/Script/NUCLEO_WB55.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Démarrer avec la carte NUCLEO-WB55](STARTWB55).

- **Tutoriels avec la carte X-NUCLEO-IKS01A3**
Vous trouverez [ici](../../assets/Script/IKS01A3.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec la carte X-NUCLEO-IKS01A3](IKS01A3).

- **Tutoriels avec des modules externes (Grove, Adafruit, DFRobot, etc.)**
Vous trouverez [ici](../../assets/Script/MODULES.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec des modules externes](grove).

- **Tutoriels avec le BLE**
Vous trouverez [ici](../../assets/Script/BLE.zip) un fichier ZIP qui rassemble tous les codes sources présentés dans la section [Tutoriels avec le BLE](BLE).

## Autres ressources

- [**Lampe connectée**](http://icnisnlycee.free.fr/index.php/61-stm32/ble-en-MicroPython/78-exemple-ble-lampe-connectee)<br>
Piloter une lampe à distance via BLE avec MIT App Inventor. **Merci à Julien Launay !**<br>

- [**Station météo**](../../assets/projects/station_meteo.zip)<br>
Prototyper une station météo inspirée du produit Ikea Klockis. **Merci à Christophe Priouzeau & Gérald Hallet !**<br>
