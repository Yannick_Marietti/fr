---
title: Guide de démarrage rapide sous Windows
description: Guide de démarrage rapide sous Windows

---
# Guide de démarrage rapide sous Windows

Dans cette section figurent les instructions pour installer le firmware MicroPython **distribué sur ce site** pour la carte NUCLEO-WB55 qui est notre cible principale, à l'aide d'un environnement informatique de type Microsoft Windows. Vous trouverez **dans l'annexe 1** (plus bas) des instructions pour installer les firmwares (disponibles sur [le site officiel de la fondation MicroPython](https://MicroPython.org/download/)) pour d'autres cartes de STMicroelectronics.

Dans ce guide de démarrage, nous allons:
- Programmer la carte avec le micrologiciel
- Utiliser un terminal de communication UART (TeraTerm ou PuTTY)
- Programmer sur la NUCLEO notre premier script Python

## Configuration électronique

**Vous aurez besoin d’un câble USB vers micro-USB.**

Cette configuration se résume à :

- Assurez-vous que le cavalier est positionné sur ***USB_STL***
- Connectez un câble micro USB sur le port ***ST_LINK*** en ***dessous des LED4 et LED5***

Voici comment doit être configuré le kit :

<br>
<div align="center">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="350px">
</div>
<br>

Ces modifications permettent de configurer le STM32WB55 en mode programmation "ST Link".<br>
Nous pourrons ainsi mettre à jour le firmware (fichier binaire) en passant par le connecteur USB_STL.

## Programmation des cartes

Téléchargez le firmware depuis [cette page](../Telechargement) (fichier .bin)

Reliez la carte NUCLEO-WB55(USB_STLINK) avec le câble USB à votre ordinateur.

### Installation du micrologiciel

Ouvrir le lecteur ***NOD_WB55*** :

<br>
![image](images/Nod_wb55.PNG)
<br>

Faire un glisser-déposer du firmware.<br>
Le micrologiciel est maintenant intégré (il n'est plus visible dans le lecteur *NOD_WB55*).<br>
Placez le cavalier sur ***USB_MCU*** ainsi que le câble micro USB sur le connecteur ***USB_USER*** sous le ***bouton RESET*** :

<br>
<div align="center">
<img alt="Nucleo_WB55_Config_MCU" src="images/Nucleo_WB55_Config_MCU.png" width="350px">
</div>
<br>

***Redémarrez la carte NUCLEO***

Votre carte NUCLEO est maintenant capable d'interpréter un fichier ".py".<br>
Regardons les fichiers générés par le système MicroPython avec l'explorateur Windows :

Ouvrir le périphérique PYBFLASH avec l'explorateur Windows :

<br>
![Image](images/dfu_util_3.png)
<br>

Nous verrons plus tard comment éditer les scripts Python disponibles dans le système de fichier PYBFLASH.

**Remarques importantes :**
Le script `boot.py` peut être modifié par les utilisateurs avancés, il permet d'initialiser MicroPython et notamment de choisir quel script sera exécuté après le démarrage de MicroPython, par défaut le script `main.py`.

## Installation de l'environnement de programmation

Trois solutions s'offrent à vous:
- Utiliser la version portable ou installer (droits admin requis) [Notepad++](https://notepad-plus-plus.org/) avec la coloration syntaxique Python et un émulateur de terminal série ([PuTTY](https://www.PuTTY.org/), [TeraTerm](https://osdn.net/projects/ttssh2/)...)
- Installer (droits admin requis) [Thonny](https://thonny.org/) et son terminal intégré
- Utiliser la version portable de [PyScripter](https://sourceforge.net/projects/pyscripter/) et un émulateur de terminal série ([PuTTY](PuTTY), [TeraTerm](https://osdn.net/projects/ttssh2/)...).


## Configuration du terminal de communication

Maintenant que MicroPython est présent sur le kit NUCLEO-WB55, nous voudrions tester la communication avec Python. Le test consiste à envoyer une commande Python et à vérifier que l'exécution se déroule correctement.

La communication s'effectue par USB par le biais d'un port série, nous avons donc besoin d'un logiciel capable d'envoyer les commandes Python sous forme de texte à la carte et de recevoir le résultat de l'exécution.

Ouvrir TeraTerm après l'avoir installé ou bien PuTTY "portable" si vous n'avez pas les droits d'administration.

Sélectionnez l'option ***Serial*** et le ***port COM*** correspondant à votre carte (**COMx: USB Serial Device**) :

<br>
![Image](images/TeraTerm.PNG)<br><br>
<br>

Si le COM correspondant à la carte n'est pas disponible, vérifiez que le kit est bien énuméré sur le bon port COM, voir **Annexe 3 : Identifier le port COM attribué par Windows à votre carte**, plus bas dans cette section.

Configurez les champs suivants dans le menu ***Setup*** / ***Serial Port*** :

<br>
![Image](images/TeraTerm_Setup.png)
<br>

Une nouvelle fenêtre s'affiche :

<br>
![Image](images/TeraTermDisplay_1.png)
<br>

Si nécessaire, appuyez sur *CTRL+D* pour faire un **Reset software**.

Vous pouvez maintenant exécuter des commandes Python (en mode REPL) :

```python
print("Hello World")
```

<br>
![Image](images/TeraTermDisplay_2.png)
<br>

Nous avons maintenant terminé la programmation du firmware MicroPython.<br>
Nous pouvons exécuter des fonctions Python au travers du port COM.<br>
Notre kit de développement NUCLEO-WB55 est désormais prêt à être utilisé avec Python. Gardez la fenêtre TeraTerm ouverte, elle vous sera utile pour visualiser l'exécution des scripts.

## Ecrire un script MicroPython avec Notepad++ et l'exécuter avec TeraTerm

Nous allons voir dans cette partie comment éditer avec Notepad++ et exécuter un script Python sur notre carte NUCLEO-WB55. Lancez Notepad++ après l'avoir installé (vous pouvez aussi utiliser la version portable).Cet outil vous permettra d'éditer vos scripts Python.

Ouvrez le fichier ***main.py*** présent sur la Nucleo (représenté par le lecteur **PYBFLASH**).

Notre premier script va consister à afficher 10 fois le message `MicroPython est génial` avec le numéro du message à travers le port série du connecteur USB user de la carte NUCLEO.

Ecrivez l'algorithme suivant dans l'éditeur de script :

<br>
![Image](images/Notepad_test1.png)
<br>

Prenez soin de sauvegarder le fichier (*CTRL+S*) ***main.py*** sur la carte (lecteur PYBFLASH).<br>
Faites un software reset de la carte en appuyant sur *CTRL+D* dans votre terminal série (TeraTerm par exemple), le script est directement interprété !

<br>
![Image](images/TeraTerm_test1.png)
<br>

Le script s'est exécuté avec succès ; nous voyons bien notre message s'afficher 10 fois dans le terminal.<br>
Vous venez d'écrire votre premier script Python embarqué dans une carte NUCLEO-WB55!

**Remarque :** Notepad++ n'est pas configuré par défaut pour utiliser des tabulations ; il les simule par plusieurs espaces consécutifs. Pour le codage en MicroPython, il est préférable de modifier ce comportement.

Pour ce faire, on commence par forcer l'affichage des espaces et tabulations, en allant dans le menu "View" (ou "Affichage" pour la version française), puis dans le sous-menu "Show Symbol" (VF : "Symboles spéciaux" où on coche les options "Show White Space and TAB" (VF : "Afficher les blancs et les tabulations") et "Show Indent guide" (VF : "Afficher le guide d'indentation").

<br>
![Image](images/Menu_1.png)
<br>

Ensuite, on demande à Notepad++ d’effectivement remplacer les espaces par des tabulations dans les scripts Python, en allant dans le menu “Settings” (VF : “Paramètres”), puis dans la boite “Preferences” (VF : “Préférences”). Dans la colonne de gauche il faut sélectionner “Language” (VF : “Langage”). Dans l’encadré à droite “Tab Setting” (VF : “Tabulations”) on sélectionne “python” et enfin dans l’encadré “Use default value” (VF : “Valeur par défaut”) il faut désactiver la case “Replace by space” (VF : “Insérer des espaces”).

<br>
![Image](images/Menu_2.png)
<br>

## Annexe 1 : Installation d'un firmware MicroPython au format ".dfu"

> Remerciements : Franck HERGNIOT, Lycée Algoud-Laffemas HF-FP 

La communauté MicroPython propose un grand nombre de firmwares pour les cartes de STMicroelectronics. [Vous trouverez leur liste exhaustive en suivant ce lien](https://MicroPython.org/stm32/). Vous pourriez par exemple éprouver le besoin d'utiliser [le firmware le plus récent pour la NUCLEO-WB55](https://MicroPython.org/download/NUCLEO_WB55/) ou encore celui pour une autre carte éventuellement plus adaptée à votre projet telle que la [NUCLEO-L432KC](https://MicroPython.org/download/NUCLEO_L432KC/) par exemple. Mais un problème se pose : ces firmwares sont distribués dans un format ".dfu" alors que toutes les manipulations indiquées sur ce site portent sur des firmwares au format ".bin".

Plusieurs procédures sont possibles pour installer ces firmwares. Vous trouverez notamment une méthode d'installation des ".dfu" dans [le tutoriel pour Linux](../install_linux/index). Sous Windows en particulier, vous pouvez les convertir au format ".bin" selon la procédure qui suit :

1. Récupérez le firmware au format ".dfu" qui vous intéresse sur le site [https://MicroPython.org/download/](https://MicroPython.org/download/).

2. Convertir le firmware du format ".dfu" au format ".bin" avec l'outil **Dfu file manager** distribué par STMicroelectronics.<br>
 - Commencez par télécharger le fichier *en.stsw-stm32080_v3.0.6.zip* depuis [cette page](https://www.st.com/en/development-tools/stsw-stm32080.html)  
 - Installez ensuite *DfuSe_Demo_V3.0.6_Setup.exe* contenu dans *en.stsw-stm32080_v3.0.6.zip*. Ceci devrait créer dans le menu démarrer de votre PC un raccourci *Dfu file manager* dans un dossier *STMicroelectronics*.
 - Lancez *Dfu file manager* et sélectionnez l'option *"I want to EXTRACT S19, HEX or BIN files from a DFU one."*, puis valider avec le bouton `OK`.

<br>
<div align="center">
<img alt="DFU file manager convert to bin" src="images/DFU_file_manager_1.jpg" width="300px">
</div>
<br>

 3. Sélectionnez ensuite le firmware dans la boite "Images" de la fenêtre suivante. Sélectionnez l'option "Multiple Bin" en dessous et validez avec le bouton `Extract`.

<br>
<div align="center">
<img alt="DFU file manager extract bin" src="images/DFU_file_manager_2.jpg" width="400px">
</div>
<br>

 4. Le fichier ".bin" créé se trouve dans le même dossier que le fichier ".dfu". Par exemple, dans le cas testé par mes soins, le firmware téléchargé se nomme *NUCLEO_WB55-20220108-unstable-v1.17-320-g1892d0374.dfu* et l'image binaire extraite se trouve dans le même dossier sous le nom *NUCLEO_WB55-20220108-unstable-v1.17-320-g1892d0374_00_08000000.bin*.<br>
 Ce nom de fichier excessivement long est source de problèmes potentiels pour la suite. Aussi, il est préférable de le raccourcir avec la fonction "renommer" du gestionnaire de fichiers de Windows. Nous choisissons comme nouveau nom *NUCLEO_WB55.bin*.

 5. Si votre carte est une NUCLEO-WB55 vous disposez du firmware au format bin et vous pouvez suivre les instructions spécifiques à celle-ci au tout début de cette section. Si vous installez MicroPython sur une autre carte, vraisemblablement avec **un seul** port USB, la procédure est encore plus simple :
 - Reliez la carte avec le câble USB à votre ordinateur. Ouvrir le lecteur *NOD_XXX*.
 - Faire un glissé/déposé du fichier Firmware (.xx.bin) dans le lecteur *NOD_XXX*. Le micrologiciel est maintenant intégré (il n’est plus visible dans le lecteur *NOD_XXX*, le ST-Link l'a déplacé dans la mémoire flash du SoC STM32WB55RG).
 - Connectez à présent votre émulateur de port série favori (Thonny, PuTTY, Mu ...) pour dialoguer avec la machine virtuelle MicroPython. 

## Annexe 2 : Précautions à prendre pour ne pas corrompre le système de fichiers MicroPython

Vous pourrez rencontrer occasionnellement des difficultés lorsque vous modifiez le fichier *main.py* ou d'autres fichiers situés sur le lecteur *PYBFLASH*. Le système de fichiers de celui-ci, de type FAT, peut être accidentellement corrompu si vous ne respectez pas quelques bonnes pratiques. Afin d'éviter ces problèmes de corruption de la FAT du lecteur PYBFLASH, nous vous **recommandons de prendre les précautions suivantes** :

- Le plus important : **évitez autant que possible d'éditer et modifier directement les scripts MicroPython directement sur le lecteur _PYBFLASH_**. Travaillez sur vos script dans un dossier enregistré sur votre PC/MAC puis faites un glisser-déplacer (ou une copie) de ceux-ci sur *PYBFLASH* lorsque vous être prêts à les tester sur la carte NUCLEO.

- Quand vous effacez un fichier contenu sur *PYBFLASH*, sous Windows **utilisez la combinaison de touches SHIFT+ SUPPR.** 

- Si malgré notre conseil de ne pas le faire, vous éditez un fichier sur *PYBFLASH*, **ne pas oublier de sauvegarder le fichier avant de déconnecter le câble USB qui relie le PC à votre carte NUCLEO.**

- Quand vous voulez déconnecter le câble USB du PC, **ne pas oublier de démonter/éjecter proprement _PYBFLASH_.**

- Quand vous voulez tester une nouvelle version de votre code, **utilisez CTRL+C et CTRL+D dans le terminal série.**
    - CTRL+C : pour arrêter l’exécution courante.
    - CTRL+D : Demande à MicroPython de faire une synchronisation du système de fichiers FAT et un RESET logiciel.

- Quand vous voulez tester une nouvelle version de votre code et que MicroPython indique qu’il y a une erreur (non visible) dans le code, **vous devez démonter/éjecter *PYBFLASH*, déconnecter et reconnecter le câble USB au PC/MAC.**

Si, malgré ces précautions, *PYBFLASH* devient inaccessible, vous n'aurez d'autre choix que réinstaller le micrologiciel comme indiqué au début de cette page ; vous pourriez même être contraint à réinitialiser la mémoire flash du microcontrôleur comme évoqué dans la section [FAQ](../../Kit/faq) avec l'outil [STM32CubeProgrammer](../../Kit/stm32cubeprogrammer).

## Annexe 3 : Identifier le port COM attribué par Windows à votre carte

Chaque fois que vous connectez la NUCLEO-WB55 à votre PC par un câble USB, un port `COM` est créé et alloué par Windows au port série virtuel exposé par le firmware MicroPython. Cette annexe explique comment identifier le port COM attribué à votre carte MicroPython, sachant que Windows attribue peut être simultanément d'autres ports COM à des périphériques USB connectés au PC tels que clavier, souris, Web CAM...

Ecrivez `gestionnaire de périphériques` ( `devices manager` en anglais) dans la barre de recherche Windows, puis cliquez sur `Ouvrir` :

<br>
<div align="center">
<img alt="Port COM" src="images/port_COM.png" width="600px">
</div>
<br>

Une nouvelle fenêtre s'ouvre :

<br>
<div align="center">
<img alt="Windows Devices Manager" src="images/gestion_peripheriques.png" width="600px">
</div>
<br>

Relevez le numéro du port ***COM***. Dans l'exemple ci-dessus, le kit NUCLEO-WB55 est branché sur le port ***COM3***. C'est ce numéro qu'il faudra rentrer dans votre émulateur de terminal série (TeraTerm...).
