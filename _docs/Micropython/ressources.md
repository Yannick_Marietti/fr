---
title: Ressources
description: Ressources

---
# Ressources

## Ouvrages

 [MicroPython et Pyboard, Python sur microcontrôleur : de la prise en main à l'utilisation avancée (Editions ENI)](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906) de Dominique MEURISSE

## Documentation en ligne

**Site officiel de MicroPython :**

 [https://MicroPython.org/](https://MicroPython.org/)

**Documentation générale sur MicroPython :**

 [http://docs.MicroPython.org/en/latest/](http://docs.MicroPython.org/en/latest/)

**Exemple d’utilisation de la librairie pyb ; très utile, en tenant compte des différences de broches entre la NUCLEO-WB55 et la Pyboard :**
 
 [http://docs.MicroPython.org/en/latest/pyboard/quickref.html#general-board-control](http://docs.MicroPython.org/en/latest/pyboard/quickref.html#general-board-control)

**Les cartes de STMicroelectronics pour lesquelles un firmware MicroPyhton est disponible :**
 
 [https://MicroPython.org/stm32/](https://MicroPython.org/stm32/)

**Code source du projet MicroPython :**

 [https://GitHub.com/MicroPython/MicroPython](https://GitHub.com/MicroPython/MicroPython)

**Site officiel de Python 3 :**

 [https://www.python.org/](https://www.python.org/)

**Documentation technique de la NUCLEO-WB55 :**

 [https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html)

**Documentation de l’application Android STBLESensor :**

 [https://GitHub.com/STMicroelectronics/STBlueMS_Android](https://GitHub.com/STMicroelectronics/STBlueMS_Android)

**Ressources pour Pyton 3 embarqué (pilotes) :**
 - [Bibliothèques et pilotes EPS8266 pour MicroPython](https://GitHub.com/mchobby/esp8266-upy)
 - [Une synthèse de pilotes et bibliothèques pour MicroPython](https://awesome-MicroPython.com/)
 - [Bibliothèques et pilotes pour Adafruit CircuitPython (facilement transposables à MicroPython)](https://GitHub.com/adafruit/Adafruit_CircuitPython_Bundle/tree/master/libraries/drivers)
 - [Bibliothèques et pilotes Seeed-Studio grove.py (pour Raspberry Pi essentiellement)](https://GitHub.com/Seeed-Studio/grove.py)
 - [Bibliothèques et pilotes pour la carte OpenMV (pour MicroPython)](https://GitHub.com/openmv/openmv/tree/master/scripts/libraries) 
