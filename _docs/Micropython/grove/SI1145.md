---
title: Capteur de lumière solaire SI1145
description: Mise en œuvre du capteur de lumière solaire SI1145 Grove avec MicroPython
---

# Capteur de lumière solaire SI1145 et indice UV

Ce tutoriel explique comment mettre en œuvre un module capteur de lumière solaire Grove avec MicroPython.Il est équipé d'un capteur SI1145 de SiLabs qui exécute un algorithme capable de déduire l'indice ultraviolet (UV) à partir d'une mesure de lumière visible et d'une mesure de lumière infrarouge (IR). Le SI1145 ne dispose donc pas d'un capteur d'ultraviolets.<br>

Le pilote du SI1145 fournit des mesures en unités arbitraires qui, en principe, pourraient être converties en unités d'intensité lumineuse (Lux), à condition de disposer des équipements requis pour réaliser sa calibration. De ce fait, à moins de corriger le pilote pour cela, nous vous déconseillons d'utiliser le module pour estimer autre chose que l'indice UV.

Vous trouverez [ici](https://fr.wikipedia.org/wiki/Indice_UV) une explication de l'indice UV et des effets des ultraviolets solaires sur le métabolisme.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. [Un capteur de lumière solaire SI1145 Grove](https://wiki.seeedstudio.com/Grove-Sunlight_Sensor/)

**Le module Grove capteur de lumière solaire :**
<div align="center">
<img alt="Grove sunlight sensor" src="images/Grove_sunlight_sensor_view.jpg" width="280px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Connectez le capteur sur **une broche I2C**.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Commencez par copier le fichier *si1145.py* dans le dossier *PYBFLASH*. Créez ensuite un fichier *main.py* dans *PYBFLASH* et copiez-collez dans celui-ci le code qui suit :

```python
# Exemple adapté de https://GitHub.com/neliogodoi/MicroPython-SI1145
# Objet du script : Mise en œuvre du module grove I2C capteur de lumière solaire,
# basé sur le capteur SI1145 pour mesurer l'indice UV.

from time import sleep_ms # Pour gérer les temporisations
from machine import I2C # Pour gérer l'I2C
import si1145 # Pour gérer le capteur 

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c1 = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c1.scan()))

# Instanciation du capteur
sensor = si1145.SI1145(i2c=i2c1)

while True :
	# Indice UV
	uv_index = sensor.read_uv
	
	# Valeur caractéristique de l'intensité du rayonnement infrarouge
	ir_analog = sensor.read_ir
	
	# Valeur caractéristique de l'intensité du rayonnement visible
	visible_analog = sensor.read_visible
	
	# Affichage
	print(" Indice UV: %d\n IR: %d (AU)\n Visible: %d (AU)\n" % (uvindex, ir, visible))
	
	# Temporisation de 5 secondes
	sleep_ms(5000)
```

## Affichage sur le terminal série

Une fois le script lancé avec CTRL+D, une série de valeurs s'affiche dans le terminal série de l'USB user toutes les 5 secondes. A l'extérieur, on obtient :

<br>

<div align="center">
<img alt="Grove sunlight sensor output" src="images/Grove_sunlight_sensor_out.jpg" width="700px">
</div>

<br>

L'indice UV passe à 2 sous l'exposition directe à la lumière solaire matinale du mois de juillet et redevient nul une fois que le capteur est placé à l'ombre.<br>
Remarquez également comment les signaux analogiques des lumières visible et infrarouge augmentent face au Soleil.

En intérieur, dans la pénombre (un peu de lumière solaire diffusée au travers des fenêtres) on obtient les valeurs suivantes :
- Indice UV: 0
- IR: 260 (AU)
- Visible: 260 (AU)

Si on éclaire le capteur avec une LED / flash de téléphone portable on obtient :
- Indice UV: 0
- IR: 385 (AU)
- Visible: 286 (AU)

Ceci permet de prendre conscience de l'intensité de la lumière solaire et de la complexité de son spectre par comparaison avec nos éclairages domestiques (qui ne produisent pas d'UV). Par ailleurs, sachez que la lumière du Soleil ne contient plus d'UV après avoir traversé une fenêtre fermée car ils sont absorbés par ses vitres en silice.
