---
title: Capteur de température de précision MCP9808
description: Mise en œuvre un module Grove capteur de température de précision MCP9808 sur bus I2C en MicroPython
---

# Capteur de température de précision MCP9808

Ce tutoriel explique comment mettre en œuvre un module Grove capteur de température de précision MCP9808 sur bus I2C en MicroPython. La fiche technique du MCP9808 est disponible [ici](https://ww1.microchip.com/downloads/en/DeviceDoc/25095A.pdf).

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module Grove MCP9808](https://wiki.seeedstudio.com/Grove-I2C_High_Accuracy_Temperature_Sensor-MCP9808/)

**Le module Grove capteur de température de précision MCP9808 :**

<div align="center">
<img alt="Gove MCP9808 module" src="images/Gove_MCP9808_module.jpg" width="300px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Branchez le module sur l'un des connecteurs I2C du Grove base shield.


## Le code MicroPython

Ce code est adapté depuis [cette source](https://GitHub.com/mchobby/esp8266-upy/tree/master/mcp9808).

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Il faut ajouter le fichier *mcp9808.py* dans le répertoire du périphérique *PYBLASH*.<br>
Editez maintenant le script *main.py* et collez-y le code qui suit :

```python
# Objet du script :
# Mesure de température toutes les secondes avec un module capteur de température Grove I2C MCP9808
# Allume ou éteint les LED de la carte selon les valeurs des températures, les affiche sur le port série
# Source : https://GitHub.com/mchobby/esp8266-upy/blob/master/mcp9808/examples/

from machine import I2C # Pilote du contrôleur I2C
from mcp9808 import MCP9808 # Pilote du MCP9808
import pyb # Pour gérer les GPIO
from time import sleep_ms # Pour temporiser

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation du MCP9808
sensor = MCP9808(i2c)

# Instanciation des LED
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	# Lecture de la température
	temp = sensor.get_temp

	# Gestion des LED et des attributs
	if temp > 25:
		led_rouge.on()
		attribut = "Chaud !"
	elif temp > 18 and temp <= 25:
		led_vert.on()
		attribut = "Confortable"
	else:
		led_bleu.on()
		attribut = "Froid !"

	led_rouge.off()
	led_vert.off()
	led_bleu.off()

	print("Température : %.1f °C (%s)" %(temp,attribut))

	# Temporisation de 5 secondes
	sleep_ms(5000)
```

## Sortie sur le port série de l'USB USER

Appuyez sur CTRL+D dans le terminal PuTTY et observez les valeurs qui défilent toutes les cinq secondes :

<div align="center">
<img alt="Grove - MCP9808 sortie" src="images/grove_mcp9808_output.png" width="380px">
</div>
