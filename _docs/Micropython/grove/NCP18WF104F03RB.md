---
title: Capteur de température (thermistance)
description: Mise en œuvre du module capteur de température Grove v1.2 avec MicroPython
---

# Capteur de température (thermistance)

Ce tutoriel explique comment mettre en œuvre un [module capteur de température Grove v1.2 avec MicroPython](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/). Ce module est constitué d'une **thermistance à coefficient de température négatif** de type NCP18WF104F03RC dont vous trouverez la fiche technique [ici](https://files.seeedstudio.com/wiki/Grove-Temperature_Sensor_V1.2/res/NCP18WF104F03RC.pdf).<br>
Un tutoriel plus général sur les thermistances est déjà disponible sur notre site, [ici](thermistance), vous trouverez également une vidéo à ce propos [ici](https://youtu.be/wjL7xOGqAqg).

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) **avec son commutateur positionné sur 3.3V**.
2. La carte NUCLEO-WB55
3. Un [module Grove - Temperature Sensor v1.2](https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/)

**Le module Grove Temperature Sensor v1.2 :**
<div align="center">
<img alt="Grove NCP18WF104F03RC" src="images/Grove_NCP18WF104F03RC.jpg" width="250px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Connectez le capteur sur **une broche analogique**.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Créez un fichier *main.py* dans *PYBFLASH* et copiez-collez dans celui-ci le code qui suit :

```python
"""
Mesure de température à partir d'une thermistance d'un module Grove Temperature sensor V1.2 relié à l'entrée A0
L'ADC interne fonctionnant sur 3.3V, le capteur doit être alimenté en 3.3V sur la carte d'adaptation Grove.

Rappel: La valeur d'une thermistance CTN obéit à la relation R=R0 x exponentielle (B( 1/T -1/298))
avec B=4275°Kelvin R0=100KOhm et Ten °Kelvin= T en °C +273

La tension U du capteur est reliée à A0 = 3.3V x R1 /( R1+R) avec R1=100K=R0
En déduire U en fonction de R0 puis T en °C:

  U=3.3/1+exponentielle (B( 1/T -1/298))
  T= 1/((ln(3.3/U -1) /B)+1/298)

Voir https://wiki.seeedstudio.com/Grove-Temperature_Sensor_V1.2/#reference
Source : F HERGNIOT, Lycée Algoud-Laffemas, Valence
"""

from time import sleep_ms # Pour temporiser
from pyb import Pin, ADC # Pilotes des broches et de l'ADC
from math import log # Fonction logarithme (népérien)

adc_A0 = ADC('A0') # Initialisation de l'ADC sur l'entrée A0 
print("Mesure de temperature à partir d'une thermistance Groove sencor V1.2") 

# Règle de 3 pour déduire la tension aux bornes de la thermistance à partir de la
# valeur convertie de l'ADC
ratio = 3.3/4096

def mesure_temperature_CTN_v12():

	B = const(4275) # coeff B de la thermistance
	R0 = const(100000) # R0 = 100k x correctif précision avec vraie mesure

	N = adc_A0.read() # Conversion de 12 bits - 4096 valeurs de 0 à 3.3V
	U= N * ratio # On déduit la tension de la valeur convertie de l'ADC

	temp = 1.0/(log((3.3/U) -1)/B+1/298)-273 # Calcule la température à partir de la tension

	return temp


def demo():

	while True:
		temperature = mesure_temperature_CTN_v12()
		
		# Affiche la température sous forme de flottant xx,x
		print("La valeur de la temperature est : %.1f °C" %temperature)
		sleep_ms(1000)

if __name__ == '__main__':
	demo()
```
## Affichage sur le terminal série

**Assurez-vous que le commutateur d'alimentation électrique du Grove base shield est bien positionné sur 3.3V**.
Appuyez sur CTRL+D dans le terminal série connecté à la NUCLEO-WB55. Une série de valeurs sera affichée dans le terminal :

<br>
<div align="center">
<img alt="Grove temperature sensor v1.2 output" src="images/thermistance_output.jpg" width="400px">
</div>
<br>

La précision du ce capteur est étonnante : 22.7 - 22.8°C selon lui, à comparer à 22.7°C selon un [système de mesure Velleman DEM500](https://www.velleman.eu/products/view/?country=fr&lang=fr&id=420578) !
