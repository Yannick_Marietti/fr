---
title: Alarme de mouvement
description: Création d'une alarme de mouvement avec un buzzer et un capteur de mouvements PIR sous MicroPython
---

# Alarme de mouvement

Dans ce tutoriel nous utiliserons en parallèle le [buzzer](buzzer) ainsi que le détecteur de mouvement [PIR motion sensor](mouvement).
Lorsqu'un mouvement sera détecté, le buzzer produira un son (alarme) pendant une seconde.<br>
Pour générer le signal d'alarme, on enverra au buzzer un signal modulé en largeur d'impulsion (PWM). Vous trouverez une explication de ce qu'est la PWM [ici](buzzer).

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [buzzer Grove](https://wiki.seeedstudio.com/Grove-Buzzer/)
4. Un [détecteur de mouvement PIR Grove](https://wiki.seeedstudio.com/Grove-PIR_Motion_Sensor/)

**Le buzzer Grove (à gauche) et le capteur de mouvement PIR Grove (à droite) :**

<div align="center">
<img alt="Grove - Buzzer" src="images/buzzer.png" width="200px">
<img alt="Grove - PIR Motion Sensor" src="images/Grove_-_PIR_Motion_Sensor.jpg" width="300px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Branchez le buzzer sur le connecteur *D3* et le détecteur sur le connecteur *D2*.
Si un mouvement est détecté, l'interruption se déclenche ce qui provoque l'activation de la LED et du buzzer.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Editez le fichier *main.py* et collez y le code qui suit, avant de l'enregistrer dans le répertoire du périphérique PYBLASH :<br>

```python
# Objet du script : conception d'un système d'alarme.
# Un capteur de mouvement PIR est configuré en interruption sur la broche D2.
# Un buzzer est connecté sur la broche D3 et piloté par une PWM.
# Matériel requis :
#   - Un capteur PIR (de préférence fonctionnant en 3.3V)
#   - Un buzzer (de préférence fonctionnant en 3.3V)
# On utilise une interruption pour capturer le signal du détecteur de mouvements.

from time import sleep_ms
from pyb import Pin, Timer

# Configuration des LED
led_bleu = pyb.LED(1)
led_rouge = pyb.LED(3)

# Configuration du buzzer
frequency = 440
buz = Pin('D3')

# D3 génère une PWM avec TIM1, CH3
timer1 = Timer(1, freq=frequency)
channel3 = timer1.channel(3, Timer.PWM, pin=buz)

# Configuration du capteur PIR
PIR_Pin = Pin('D2', Pin.IN)
motion = False

# Fonction de gestion de l'interruption du capteur PIR
def handle_interrupt(pin):
	global motion
	motion = True
	global interrupt_pin
	interrupt_pin = PIR_Pin

# Activation de l'interruption du capteur PIR
PIR_Pin.irq(trigger=PIR_Pin.IRQ_RISING, handler=handle_interrupt)

# Boucle principale
while True:
	if motion:
		
		print('Mouvement détecté !')
		led_rouge.on()
		led_bleu.off()

		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		channel3.pulse_width_percent(5)
		
		sleep_ms(1000) # Le buzzer sonne pendant une seconde
		print('Détecteur de mouvement activé')
		led_bleu.on()
		led_rouge.off()

		# Rapport cyclique paramétré à 0% (le buzzer n'est plus alimenté)
		channel3.pulse_width_percent(0)
		motion = False

```

## Affichage sur le terminal série de l'USB User

Appuyez sur CTRL+D dans le terminal PuTTY et observez les valeurs qui défilent :

<div align="center">
<img alt="Alarm output" src="images/alarm_output.png" width="700px">
</div>
