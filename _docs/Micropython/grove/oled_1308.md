---
title: Afficheur OLED 1308
description: Mise en œuvre d'un afficheur OLED 0.96 pouces Grove avec MicroPython
---

# Afficheur OLED 1308

Ce tutoriel explique comment mettre en œuvre un afficheur I2C OLED 0.96 pouces Grove avec MicroPython.

# Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module afficheur OLED 0.96 pouces Grove](https://wiki.seeedstudio.com/Grove-OLED_Display_0.96inch/)

**L'afficheur OLED 0.96 pouces Grove :**

<div align="center">
<img alt="Grove - OLED Display 0.96 inch" src="images/Grove-OLED-0.96.png" width="700px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Cet afficheur doit être connecté sur une prise I2C du Grove base shield.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* :

```python
# Objet du script :
# Affiche un texte sur un afficheur OLED contrôlé par un SSD1308.

from time import sleep_ms # Pour temporiser
from machine import Pin, I2C # Pilotes des entrées-sorties et du bus I2C
import ssd1308 # Pilote de l'afficheur

# Initialisation du périphérique I2C
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Paramétrage des caractéristiques de l'écran
largeur_ecran_oled = 128
longueur_ecran_oled = 32
oled = ssd1308.SSD1308_I2C(largeur_ecran_oled, longueur_ecran_oled, i2c)

# Envoi du texte à afficher sur l'écran OLED
oled.text('MicroPython OLED!', 0, 0)
oled.text('    I2C   ', 0, 10)
oled.text('Trop facile !!!', 0, 20)
oled.show() # Affichage !
```
Enregistrez les modifications avec (CTRL + S sous Notepad++). Vous pouvez lancer le script avec Ctrl + D sur le terminal PuTTY et observez les messages qui s'affichent sur le module OLED.
