---
title: Capteur de pression intégré MPX5700AP
description: Mise en œuvre du capteur de pression intégré [MPX5700AP](https://docs.rs-online.com/6bdd/0900766b8138445c.pdf) Grove en MicroPython
---

# Capteur de pression intégré MPX5700AP

Ce tutoriel explique comment mettre en œuvre le [kit de mesure de pression Grove](https://wiki.seeedstudio.com/Grove-Integrated-Pressure-Sensor-Kit/) en MicroPython. Le capteur utilisé est basé sur un composant MPX5700AP qui renvoi une valeur analogique proportionnelle à la pression, entre 0 et 4095.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [kit de mesure de pression Grove](https://wiki.seeedstudio.com/Grove-Integrated-Pressure-Sensor-Kit/)

<div align="center">
<img alt="Grove - capteur de pression intégré MPX5700AP" src="images/Grove-MPX5700AP.jpg" width="550px">
<img alt="Grove - capteur de pression intégré MPX5700AP broches" src="images/Grove-PX5700AP-pin.jpg" width="450px">
</div>

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Ce code est un exemple trivial de conversion analogique-numérique. Le script suppose que le capteur est branché sur la broche A0 du Grove base shield. La conversion de la valeur analogique échantillonnée en une pression absolue (physique) mérite cependant quelques explications.

Dans un premier temps, on ne branche pas la seringue au capteur, la pression qu'il mesure est donc la pression atmosphérique absolue du lieu. On fait dix acquisitions sur l'ADC et on les somme, ce qui nous donne la valeur de `pref_analog` (6883 dans notre cas).
Ensuite, à l'aide d'un capteur  [BME820](bme280) ou du [LPS22HH](https://stm32python.gitlab.io/fr/docs/MicroPython/IKS01A3/lps22hh) de la carte d'extension IKS01A3, on mesure la pression atmosphérique absolue du lieu. Dans notre cas, on a trouvé `pref_hpa`.

On fait *l'hypothèse que la réponse du capteur est linéaire*, tout à fait légitime après consultation de [la fiche technique du MPX5700AP](https://docs.rs-online.com/6bdd/0900766b8138445c.pdf) :

<div align="center">
<img alt="Grove - capteur de pression intégré MPX5700AP" src="images/Linearite.jpg" width="500px">
</div>

Editez maintenant le script *main.py* sur le disque *PYBFLASH* et collez-y ce code :

```python
# Lecture et numérisation du signal d'un capteur Grove de pression intégré

from pyb import Pin, ADC # Gestion de la broche analogique et de l'ADC
import time # Gestion du temps et des temporisations
from MicroPython import const # Pour déclarer des constantes (entières)

# Pression de référence (atmosphérique) : 956 hPa
pref_hpa = const(956)

# Somme de dix valeurs analogiques pour 956 hPa
pref_analog = const(6883)

ratio = pref_hpa / pref_analog

# Instance du convertisseur analogique-numérique
adc = ADC(Pin('A0'))

while True:
	
	valeur = 0 # Valeur analogique
	compteur = 0 

	# On effectue dix conversions analogiques-numériques de la pression
	while compteur < 10:
		valeur = valeur + adc.read()
		compteur = compteur + 1

	# Conversion de la valeur analogique en pression
	pression = valeur * ratio

	print("Valeur analogique = %d" %valeur)

	print("Pression = %d hPa" %pression)

	# Pause pendant 1 s
	time.sleep(1)
```

## Affichage sur le terminal série de l'USB User

On appuie et on relâche la seringue connectée au capteur de pression :

<div align="center">
<img alt="One Wire output" src="images/MPX5700AP_output.png" width="300px">
</div>
