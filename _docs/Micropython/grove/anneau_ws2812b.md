---
title: Anneau de LED Neopixel WS2812B et WS2813
description: Mise en œuvre d'un anneau de LED WS2812B / WS2813 avec MicroPython
---

# Anneau de LED Neopixel WS2812B et WS2813

Ce tutoriel explique comment mettre en œuvre un anneau de LED WS2812B ou WS2813 avec MicroPython.

## Description

Un ruban/anneau de LED WS2812B ou WS2813, dit **Neopixel** est constitué d'une succession de LED RGB adressables. 
On peut définir l'intensité et la couleur de chaque LED.
Le principe est exactement le même que celui d'un [ruban de LED Neopixel](https://stm32python.gitlab.io/fr/docs/MicroPython/grove/ws2812b), il n'y a que la disposition des LED qui change.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/).
2. La carte NUCLEO-WB55.
3. Un [anneau de LED RGB Grove](https://www.seeedstudio.com/Grove-RGB-LED-Ring-16-WS2813-Mini-p-4201.html).

### Le montage

La sortie MOSI du SPI1 de la carte ST Nucleo est utilisée pour envoyer les données au contrôleur WS2812B ou ou WS2813

| Ruban de LED    | NUCLEO-WB55       |
| :-------------: | :---------------: |
|      5V         |         5V        |
|      DI         |         D11       |
|      GND        |         GND       |

### Le code MicroPython pour allumer toutes les LED en rouge

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Il faut récupérer et ajouter le fichier *ws2812bstm32.py* dans le répertoire du périphérique *PYBLASH*. Editez maintenant le script *main.py* et collez-y le code qui suit :

```python
# Objet du script :
# Programmer un anneau de LED piloté par un contrôleur WS2812.
# Il s'agit de faire défiler les couleurs de l'arc en ciel sur l'anneau.
# Cet exemple utilise un contrôleur SPI paramétré avec un débit (baudrate) de 4000000 bauds/s

from machine import SPI
import ws2812bstm32	# Bibliothèque pour gérer le ruban de LED
from time import sleep_ms

# Le WS2812 est connecté au MOSI du bus SPI
spi = SPI(1)
led_number = const(12)

sleep_ms(1000)
# Initialisation dyu ruban de LED
# Pour le STM32WB55 le débit DOIT être de 4000000 bauds
strip = ws2812bstm32.WS2812B(spi_bus=1, ledNumber=led_number, intensity=0.3, baudrate=4000000)

# Efface le ruban
strip.clear()
strip.show()

# Vitesse de defilement et couleur par defaut
vitesse = 40

# Couleur par défaut
rouge = 0
vert = 0
bleu = 255

# Animation de demarrage avec allumage des LED dans un ordre successif
for index in range(0, led_number):
	strip.put_pixel(index, rouge, vert, bleu)
	strip.show()
	sleep_ms(70)


# Faire defiler les couleurs de l'arc en ciel
# Se base sur les couleurs primaires (rouge - vert - bleu)
while(1):
	for bleu in range (255, -1, -5):						# Passage du bleu a vert
		vert = 255 - bleu
		for index in range(0, led_number):
			strip.put_pixel(index, rouge, vert, bleu)
		strip.show()
		sleep_ms(vitesse)
	for vert in range (255, -1, -5):						# Passage du vert a rouge
		rouge = 255 - vert
		for index in range(0, led_number):
			strip.put_pixel(index, rouge, vert, bleu)
		strip.show()
		sleep_ms(vitesse)
	for rouge in range (255, -1, -5):						# Passage du rouge a bleu
		bleu = 255 - rouge
		for index in range(0, led_number):
			strip.put_pixel(index, rouge, vert, bleu)
		strip.show()
		sleep_ms(vitesse)
```
