---
title: Servomoteur
description: Mettre en œuvre un servomoteur avec MicroPython
---

# Servomoteur
Ce tutoriel explique comment mettre en œuvre un servomoteur avec MicroPython.

<h2>Description</h2>
Le servomoteur (ou *servo*) est un type de moteur électrique. C'est un boitier contenant :

 - une partie mécanique avec un moteur (souvent très petit) et des engrenages pour avoir une vitesse plus faible mais un couple plus important.
 - une partie électronique avec un capteur agissant comme un potentiomètre dont la résistance varie en fonction de l'angle.

On le retrouve dans différents domaines, notamment industriel, pour ouvrir ou fermer des vannes mais aussi dans le modélisme, pour des voitures télécommandées par exemple.

Un servomoteur est donc un moteur dont on contrôle l'angle de rotation, qui peut varier de 0 à 180°, en fonction du signal envoyé par la carte NUCLEO-WB55. Ce signal est de type [PWM](../../Kit/glossaire).


**Qu'est-ce que la PWM ?**

La _modulation en largeur d'impulsion (PWM pour "Pulse Width Modulation")_ est une méthode qui permet de simuler un signal analogique en utilisant une source numérique.
On génère un signal carré, de fréquence donnée, qui est à +3.3V (haut) pendant une proportion paramétrable de la période et à 0V (bas) le reste du temps. C’est un moyen de moduler l’énergie envoyée à un actuateur. Usages : commander des moteurs, faire varier l’intensité d’une LED, faire varier la fréquence d’un buzzer... La proportion de la durée d'une période pendant laquelle la tension est dans l'état "haut" est appelée _rapport cyclique_.

Dans notre cas la PWM est un signal périodique de 50Hz (soit 20ms) dont on fait varier le rapport cyclique entre 5% (1 ms / 20 ms) et 10% (2 ms / 20 ms) pour faire tourner le bras du servo de 0° à 180°, conformément aux schémas qui suivent :

<br>
<div align="center">
<img alt="Description PWM servomoteur" src="images/servomoteur-pwm.svg" width="400px">
</div>
<br>

> Crédit image : [Wikimedia](https://upload.wikimedia.org/wikipedia/commons/f/f6/TiemposServo.svg)

**Programmer des PWM sur la carte NUCLEO-WB55**

En pratique, la génération de signaux PWM est l’une des fonctions assurées par les _timers_, des composants intégrés dans le microcontrôleur qui se comportent comme des compteurs programmables. Le microcontrôleur de notre carte contient plusieurs timers, et chacun d'entre eux pilote plusieurs _canaux_ (_channels_ en anglais). Certains canaux de certains timers sont finalement connectés à des broches de sorties du Microcontrôleur (GPIO). Ce sont ces broches là qui vont pouvoir être utilisées pour générer des signaux de commande PWM.

Même si MicroPython permet de programmer facilement des broches en mode sortie PWM, vous aurez cependant besoin de connaître :
 1. Quelles broches de la NUCLEO-WB55 sont des sorties PWM ;
 2. A quels timers et canaux sont connectées ces broches dans le STM32WB55.

La figure ci-dessous répond à ces deux questions :

<br>
<div align="center">
<img alt="PWM WB55" src="images/pwm_wb55.jpg" width="400px">
</div>
<br>

<h2>Montage</h2>

Le servomoteur dispose d'une connectique avec des couleurs qui lui sont propres (mais qui sont standardisées). Il faut cependant bien vérifier la correspondance des couleurs en fonction des connecteurs. Dans notre cas nous les connectons de cette façon sur la carte :

<br>
<div align="center">
<img alt="Schéma de montage servomoteur" src="images/servomoteur-schema.png" width="800px">
</div>
<br>

La correspondance entre le servomoteur et la carte NUCLEO-WB55 est la suivante :

| Servomoteur       | Couleur du fil    | ST Nucleo         |
| :-------------:   | :---------------: | :---------------: |
|       Signal      |      Orange       |         D6        |
|       GND         |      Marron       |         GND       |
|       5V          |      Rouge        |         5V        |


<h2>Le code MicroPython</h2>

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

**Etape 1 :** Pour faire fonctionner le programme nous devons dans un premier temps importer la méthode de temporisation `sleep` ainsi que la classe `Pyb` pour accéder aux broches et aux compteurs (timers), de cette façon :

```python
from time import sleep # Pour temporiser
import pyb # Pour accéder  aux broches et aux compteurs (timers)
```

**Etape 2 :** Ensuite il faut vernier initialiser la patte sur laquelle est connecté le servomoteur. On configure également la PWM avec une fréquence de 50Hz.

```python
servo = pyb.Pin('D6')
tim_servo = pyb.Timer(1, freq=50)
```

**Etape 3 :** Il ne nous reste plus qu'à faire tourner le servo. Pour cela on crée une boucle infinie qui fait pivoter le servo de 90° toutes les cinq secondes.

```python
# Objet du script :
# Piloter un servomoteur
# Exemple de valeurs de PWM pour le servomoteur Grove livré avec les kits 
# de base pour Arduino.

from time import sleep # Pour temporiser
import pyb # Pour accéder  aux broches et aux compteurs (timers)

servo = pyb.Pin('D6')

Timer_Num = 1 # Numéro du timer
Timer_Cha = 1 # Canal du timer
Timer_Freq = 50 # Fréquence du timer (en Hz)

while True:

	print("Servomoteur à 0 degrés")
	tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq) # Démarrage du timer
	tim_servo.channel(Timer_Cha, pyb.Timer.PWM, pin=servo, pulse_width_percent=5)	# Servomoteur à 0 degrés
	sleep(5) # temporisation de 5 secondes

	print("Servomoteur à 90 degrés")
	tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq) # Démarrage du timer
	tim_servo.channel(Timer_Cha, pyb.Timer.PWM, pin=servo, pulse_width_percent=2)	# Servomoteur à 90 degrés
	sleep(5) # temporisation de 5 secondes
	
	print("Servomoteur à -90 degrés")
	tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq) # Démarrage du timer
	tim_servo.channel(Timer_Cha, pyb.Timer.PWM, pin=servo, pulse_width_percent=9.5)	# Servomoteur à -90 degrés
	sleep(5) # temporisation de 5 secondes


tim_servo.deinit() # Arrêt du timer
```

Sans doute à cause d'une implémentation limitée des PWM du STM32WB55 en MicroPython, il est nécessaire de démarrer et d'arrêter le timer après chaque changement de rapport cyclique (pulse_width_percent). En principe, la ligne  `tim_servo = pyb.Timer(Timer_Num, freq=Timer_Freq)` ne devrait donc figurer qu'une seule fois dans le code, avant `while True:`.

Notez également que les valeurs de largeur d'impulsions qui fixent le rapport cyclique et donc, l'angle de rotation du moteur, peuvent être modifiées ou ajustées selon les moteurs ou les situations. Par exemple,	avec le servo Grove on peut aussi utiliser :

 - `pulse_width_percent=12.5 # Servomoteur à 90 degrés`
 - `pulse_width_percent=7.5 # Servomoteur à 0 degré`
 - `pulse_width_percent=3 # Servomoteur à -90 degrés`

Le zéro degrés ne sera pas placé dans la même direction, mais les angles décrits seront respectés.
Il est aussi possible de balayer tous les angles intermédiaires en faisant varier pulse_width_percent entre 3 et 12,5.

**Remarque** : Le programme n'exécutera jamais (en l'absence de bogue tout du moins...) la dernière ligne ```tim_servo.deinit() # Arrêt du timer du servomoteur```.
Mais elle figure ici afin que vous ayez connaissance de la méthode ```deinit()``` spécifique aux timers, destinée à les stopper.

<h2>Résultat</h2>

Après avoir sauvegardé et redémarré le script sur votre carte NUCLEO-WB55 vous pourrez voir le servomoteur tourner de 90° toutes les deux secondes.
Pour aller plus loin vous pouvez également utiliser le joystick d'une manette pour contrôler le servomoteur comme expliqué [dans le tutoriel sur le module Grove adaptateur de la manette Nintendo NunChuk](nunchuk).
