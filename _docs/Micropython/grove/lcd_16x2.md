---
title: Afficheur LCD 16 caractères x 2 lignes Grove V2.0 (ou plus)
description: Mettre en œuvre un afficheur LCD 16 caractères x 2 lignes V2.0 avec MicroPython
---

# Mise en œuvre de l'afficheur LCD 16x2 Grove V2.0 (ou plus)

Ce tutoriel explique comment mettre en œuvre un module Grove afficheur I2C LCD 16 caractères x 2 lignes V2.0 (ou plus) avec MicroPython.

## Matériel requis

1. La carte NUCLEO-WB55
2. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
3. Un [module LCD Grove 16x2 V2.0 (ou plus)](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)

**Les modules Grove LCD 16x2 :**

<div align="center">
<img alt="Grove - 16x2 LCD" src="images/grove-lcd_16x2_display.jpg" width="650px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Objectif de l'exercice

Afficher des messages sur le LCD.

**Mise en œuvre**
Dans cet ordre ...
1. Connecter la carte **Grove Base Shield pour Arduino** sur la NUCLEO-WB55.
2. Connecter l'afficheur sur un connecteur I2C.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Copier la bibliothèque *i2c_lcd.py* dans le dossier *PYBFLASH*.
Copiez le code suivant dans le fichier *main.py* situé également dans le dossier *PYBFLASH* :

```python
# Objet du script :
# Mise en œuvre du module Grove LCD 16x2 I2C
# Bibliothèques pour le LCD copiées et adaptées depuis ce site : 
# https://GitHub.com/Bucknalla/MicroPython-i2c-lcd

from time import sleep # pour temporiser
from machine import I2C # Pilote du bus I2C
from i2c_lcd import lcd # Pilote du module LCD 16x2

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 
# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep(1)

# Liste des adresses I2C des périphériques présents
print("Adresses I2C utilisées : " + str(i2c.scan()))

# Instanciation de l'afficheur
afficheur = lcd(i2c)

while True:

	# Positionne le curseur en colonne 1, ligne 1
	colonne = 1
	ligne = 1
	afficheur.setCursor(colonne - 1,ligne -1)

	# Affiche "Hello World"
	afficheur.write('Hello World')

	# Positionne le curseur en colonne 1, ligne 2
	colonne = 1
	ligne = 2
	afficheur.setCursor(colonne - 1,ligne -1)

	# Affiche "Bonjour Monde"
	afficheur.write('Bonjour Monde')

	# Attends cinq secondes
	sleep(5)

	# Efface l'afficheur
	afficheur.clear()

	# Attends cinq secondes
	sleep(5)
```

## Remarque importante : I2C et Pull-up pour des révisions antérieures à la V2.0

Attention, cet afficheur, qui fonctionne sur un bus I2C, peut poser une difficulté aux non initiés à l'électronique : **pour qu'une communication I2C fonctionne avec un périphérique, il faut que ses broches SCL et SDA aient un potentiel porté à +3.3V**.

En pratique, **comme expliqué sur [la page Wiki de l'afficheur](https://wiki.seeedstudio.com/Grove-16x2_LCD_Series/)**, des résistances dites "de tirage" (pull-up en anglais) ont été ajoutées pour répondre à cette problématique sur la version 2.0 de l'afficheur. Donc *si vous souhaitez utiliser un module Grove LCD 16x2 dans sa première révision*, il ne fonctionnera pas à moins que vous ajoutiez en parallèle, sur les broches SCL et SDA, les résistances de tirage requises. 

Cette problématique est bien expliquée [ici](https://www.fabriqueurs.com/afficheur-lcd-2-lignes-de-16-caracteres-interface-i2c/) et par le schéma ci-dessous :

<br>
<div align="center">
<img alt="Grove - 16x2 LCD" src="images/lcb_rgb_grove_pullup.png" width="600px">
</div>
<br>

Cette remarque est évidemment applicable à **n'importe quel autre module I2C** que vous viendriez connecter à la NUCLEO-WB55 et qui ne serait pas lui-même en mesure d'apporter ces résistances de tirages polarisées à +3.3V.
