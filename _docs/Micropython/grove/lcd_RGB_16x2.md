---
title: Afficheur LCD RGB 16 caractères x 2 lignes
description: Mettre en œuvre un afficheur LCD RGB 16 caractères x 2 lignes avec MicroPython
---
# Mise en œuvre de l'afficheur LCD RGB 16x2

Ce tutoriel explique comment mettre en œuvre un module Grove afficheur I2C LCD RGB 16 caractères x 2 lignes avec MicroPython.

## Matériel requis

1. La carte NUCLEO-WB55
2. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
3. Un [module LCD RGB Grove **5V**](https://wiki.seeedstudio.com/Grove-LCD_RGB_Backlight/)
4. Une carte X-NUCLEO IKS01A3

**Le module afficheur Grove LCD RGB :**

<div align="center">
<img alt="Grove - RGB LCD" src="images/lcb_rgb_grove.png" width="330px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Objectifs de l'exercice

- Mesurer la température (en degrés Celsius) de l'air ambiant toutes les secondes
- Afficher la température sur le LCD RGB Grove et sur le terminal de l'USB USER
- Ajuster la couleur de fond du LCD selon la température mesurée

**Mise en œuvre**
Dans cet ordre ...
1. Connecter la carte **Grove Base Shield pour Arduino** sur la NUCLEO-WB55
2. Connecter la carte X-NUCLEO-IKS01A3 sur la carte NUCLEO-WB55
3. Connecter l'afficheur sur **I2C1** et **pensez à mettre de commutateur VCC de Grove Base Shield sur 5V !** Autrement, le LCD ne s'allumera pas.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Copier les bibliothèques *i2c_lcd_screen.py*, *i2c_lcd_backlight.py*, *i2c_lcd* et *STTS751.py* dans le dossier *PYBFLASH*.
Copiez le code suivant dans le fichier *main.py* situé également dans le dossier *PYBFLASH* :

```python
# Bibliothèques pour le LCD RGB copiées depuis ce site : https://GitHub.com/Bucknalla/MicroPython-i2c-lcd
# Objet du script :
# Mesure la température (en degrés Celsius) de l'air ambiant toutes les secondes
# Affiche la température sur le LCD RGB Grove et sur le terminal de l'USB USER
# Ajuste la couleur de fond du LCD et allume les LED selon la température lue
# Cet exemple nécessite un shield X-NUCLEO IKS01A3, un Grove Base Shield pour Arduino et un LCD RGB Grove.
# Attention, le LCD RGB Grove doit être alimenté en 5V, pensez à placer le commutateur du Grove Base Shield
# pour Arduino sur la bonne position !
# NB : C'est le shield X-NUCLEO IKS01A3 qui apporte les résistances de PULL-UP sur les broches 
# SCL et SDA de l'I2C, indispensables au bon fonctionnement du LCD RGB Grove.

from machine import I2C # Bibliothèque pour gérer l'I2C
import STTS751 # Bibliothèque pour gérer le capteur MEMS de température 
import pyb # Bibliothèque qui sera utilisée pour gérer les LED
import time # Bibliothèque qui sera utilisée pour gérer les temporisations
import i2c_lcd # Bibliothèque pour gérer l'affichaur LCD RGB Grove

# On utilise l'I2C n°1 de la carte NUCLEO-WB55 pour communiquer avec le capteur
i2c = I2C(1) 

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
time.sleep_ms(1000)

sensor = STTS751.STTS751(i2c)

#Instance de la classe Display
lcd = i2c_lcd.Display(i2c)
lcd.home()

# Initialisation des trois LED
led_bleu = pyb.LED(1)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(3)

while True:

	# On arrondit la température mesurée à 1 décimale
	temp = round(sensor.temperature(),1)
	
	# Transforme la valeur numérique de la température en sa représentation affichable
	# sous la forme d'une chaîne de caractères.
	stemp = str(temp)
	
	# Affichage sur le port série de l'USB USER
	print("Température : " + stemp + "°C (", end='')

	lcd.move(0,0) # On se place en colonne 0, ligne 0
	lcd.write('Temperature (C)') # On écrit à partir de la position du curseur la chaîne "Temperature (C)"
	lcd.move(0,1) # On se place en colonne 0, ligne 1
	lcd.write(stemp) # On écrit la représentation affichable de la température
	
	if temp > 25 :
		led_rouge.on()
		print("chaud)")
		lcd.color(255,0,0) # Rétroéclairage du LCD : rouge
	elif temp > 18 and temp <= 25 :
		led_vert.on()
		print("confortable)")
		lcd.color(0,255,0) # Rétroéclairage du LCD : vert
	else:
		led_bleu.on()
		print("froid)")
		lcd.color(0,0,255) # Rétroéclairage du LCD : bleu

	# On éteint les LED
	led_rouge.off()
	led_vert.off()
	led_bleu.off()
	
	# Temporisation d'une seconde
	time.sleep_ms(1000)
```

## Remarque importante : I2C et Pull-up

Attention, cet écran qui fonctionne sur un bus I2C peut poser une difficulté aux non initiés à l'électronique : **pour qu'une communication I2C fonctionne avec un périphérique, il faut que ses broches SCL et SDA aient un potentiel porté à +3.3V**.

En pratique, lorsqu'on utilise une carte d'extension X-NUCLEO-IKS01A3 avec un Grove Base Shield connecté par dessus, comme nous le faisons dans ce tutoriel, ce problème est masqué car la X-NUCLEO-IKS01A3 intègre les **résistances de tirage** (ou de **pull-up**, en anglais) et les alimentations qui permettent de porter SCL et SDA à +3.3V.

Donc si vous souhaitez utiliser le LCD RGB Grove en le branchant sur un Grove Base Shield directement **sans la X-NUCLEO-IKS01A3** il ne fonctionnera pas à moins que vous ajoutiez en parallèle, sur les broches SCL et SDA, les résistances de tirage requises. Cette problématique est bien expliquée [ici](https://www.fabriqueurs.com/afficheur-lcd-2-lignes-de-16-caracteres-interface-i2c/) et par le schéma ci-dessous :

<br>
<div align="center">
<img alt="Grove - RGB LCD" src="images/lcb_rgb_grove_pullup.png" width="600px">
</div>
<br>

Cette remarque est évidemment applicable à **n'importe quel autre module I2C** que vous viendriez connecter à la NUCLEO-WB55 et qui ne serait pas lui-même en mesure d'apporter ces résistances de tirages polarisées à +3.3V.
