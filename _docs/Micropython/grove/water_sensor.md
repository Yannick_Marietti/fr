---
title: Lecture du niveau d'eau
description: Acquisition du niveau d'eau avec un capteur d'eau
---

# Capteur niveau d'eau

Ce tutoriel explique comment mettre en œuvre un capteur de niveau d'eau.

<h2>Description</h2>
Connecter un capteur d'eau à une carte STM32 est un excellent moyen de détecter une fuite, un déversement, une inondation, une pluie, etc. 
Il peut ainsi être utilisé pour détecter la présence, le niveau, le volume et/ou l'absence d'eau.


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un capteur d'eau Grove

**Le capteur d'eau :**

<div align="center">
<img alt="Grove - Water sensor" src="images/Water_Sensor.png" width="300px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Le capteur est connecté sur le connecteur A0 du shield Grove.


<h2>Le code MicroPython</h2>

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

```python
# Lecture et numérisation du signal d'un capteur de niveau d'eau (water sensor)
# Attention : le capteur doit être alimenté en 5V pour donner une réponse entre 0 et 4095.

from pyb import ADC, Pin 		# Convertisseur analogique-numérique et GPIO
from time import sleep 			# Pour les temporisations

# Instanciation et démarrage du convertisseur analogique-numérique
adc = ADC(Pin('A0'))

while True:
	# Numérise la valeur lue, produit un résultat variable dans le temps dans l'intervalle [0 ; 4095]
	Mesure = adc.read()
	print("Niveau d'eau : %d " %(Mesure))
	sleep(1) # Temporisation d'une seconde
```

<h2>Résultat</h2>

Et voilà ! Nous pouvons à présent observer, dans un terminal, la valeur acquise en faisant varier le niveau d'eau.
