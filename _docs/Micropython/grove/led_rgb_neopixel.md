---
title: LED RGB Neopixel (WS2813 Mini)
description: Mise en œuvre d'un module Grove LED RGB Neopixel (WS2813 Mini) avec MicroPython
---

# LED RGB Neopixel (WS2813 Mini)

Ce tutoriel explique comment mettre en œuvre un module Grove LED RGB Neopixel ([WS2813 Mini](WS2813-Mini.pdf)) avec MicroPython. Ce module doit être connecté à une broche numérique. Son pilote est exactement le même que celui utilisé pour les [rubans Neopixel](ws2812b).

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/) ;
2. La carte NUCLEO-WB55 ;
3. Un [module Grove LED RGB Neopixel (WS2813 Mini)](https://www.seeedstudio.com/Grove-RGB-LED-WS2813-Mini-p-4269.html).

**Le module Grove LED RGB Neopixel (WS2813 Mini) :**

<br>
<div align="center">
<img alt=" LED RGB Neopixel (WS2813 Mini)" src="images/Grove_module_RGB_LED_Neopixel.jpg" width="380px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Connectez le module sur **une broche numérique** (D2 dans notre exemple).

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**. 

Commencez par copier le fichier *neopixel.py* dans le dossier *PYBFLASH*.<br>
Créez ensuite un fichier *main.py* dans *PYBFLASH* et copiez-collez dans celui-ci le code qui suit :

```python
# Objet du script : Mise en œuvre d'un module Grove RGB LED (WS2813 Mini).
# On fait varier aléatoirement la couleur de la LED.

import neopixel # Pilote pour la LED Neopixel
from random import seed, randint # Pour générer des nombres entiers aléatoires
from pyb import Pin # Pour gérer les broches
from time import sleep_ms, ticks_ms # Pour temporiser et mesurer le temps écoulé

# On initialise la LED Neopixel sur la broche D2
_NB_LED = const(1)
np = neopixel.NeoPixel(Pin('D2'), _NB_LED)

# Valeurs initiales de l'intensité sur les trois canaux
# (inutile en pratique, mais rend le code plus lisible).

rouge = 0
vert = 0
bleu = 0

# Initialise le générateur d'entiers aléatoires avec un nombre 
# de ticks processeurs
seed(ticks_ms())

# Intensité maximum des couleurs (255 au plus)
_INTENSITE_MAX = const(128)

# Boucle sans clause de sortie
while True:

	# On détermine aléatoirement valeurs de l'intensité sur les trois canaux
	# (entier compris entre 0 et intensite_max)

	rouge = randint(0, _INTENSITE_MAX)
	vert = randint(0, _INTENSITE_MAX)
	bleu = randint(0, _INTENSITE_MAX)

	# Valeurs de l'intensité sur les trois canaux pour toutes les LED
	for i in range(_NB_LED):
		np[i] = (rouge, vert, bleu)

	# On affiche
	np.write()

	# On temporise un quart de seconde
	sleep_ms(250)
```

Appuyez sur CTRL+D dans le terminal série connecté à la NUCLEO-WB55 et observer la LED qui change de couleur au cours du temps.
