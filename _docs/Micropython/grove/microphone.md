---
title: Enregistrement d’un fichier audio sur carte micro-SD
description: Enregistrement d’un fichier audio sur carte micro-SD
---

### Enregistrement d’un fichier audio sur carte micro-SD

Nous allons voir dans ce chapitre deux nouvelles fonctionnalités, l’enregistrement d’un fichier audio au format WAV et la sauvegarde de fichier sur la carte SD.

![SDCard](images/microphone_1.png)

_La taille mémoire Flash de la carte NUCLEO-STM32WB55 étant limitée, la fonction audio est dépendante de la fonctionnalité d’enregistrement sur carte SD._

Pour réaliser ce tutoriel vous devrez être en possession :

1. Du dernier firmware MicroPython en révision 4 et avoir mis à jour la carte Nucleo.
2. D’un microphone provenant de la carte d’extension STEVAL-MIC003V1, 1V1 ou 2V1.
![Micro STEVAL-MIC003V1](images/microphone_2.png)
3. D’un support pour micro SD et d’une carte SD. (https://www.adafruit.com/product/254)
![Support SDCard](images/microphone_3.png)

Voici comment câbler les différents composants avec la carte Nucleo :

![Montage Fritzing](images/microphone_4.png)

Une fois le câblage réalisé, vous pouvez maintenant enregistrer un fichier audio sur la carte µSD.

Pour cela voici les commandes python à utiliser :

``` python
import pyb

audio = pyb.Sai() #Initialisation du périphérique audio SAI

# La méthode record prend 2 paramètres
# 1 : Le nom du Fichier.
# 2 : La durée d'enregistrement en nombre de secondes

audio.record('test_audio.wav', 5)
```

_Il est conseillé de rebrancher la carte NUCLEO pour re-synchroniser les fichiers présents avec Windows._

Le fichier audio ‘test_audio.wav’ devrait ensuite apparaître dans la liste des fichiers stockés sur la carte SD.

Vous pourrez télécharger le fichier en utilisant l’explorateur Windows.
