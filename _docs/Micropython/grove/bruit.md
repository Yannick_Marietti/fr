---
title: Sonomètre
description: Mise en œuvre du module capteur de sons Grove avec MicroPython
---

Ce tutoriel explique comment mettre en œuvre un module capteur de sons Grove analogique avec MicroPython.

# Sonomètre

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [capteur de son Grove](https://wiki.seeedstudio.com/Grove-Sound_Sensor/)

**Le capteur de son Grove :**

<div align="center">
<img alt="Grove sound sensor" src="images/capteur_sonore.png" width="200px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ce capteur peut être utilisé comme détecteur de bruit ; sa sortie est proportionnelle au niveau sonore environnant.
Il doit être connecté sur une prise analogique du Grove Base Shield, nous choisissons le **connecteur A1**. Cet exemple est une nouvelle illustration, très simple, de la conversion d'un signal en tension variable dans le temps (la réponse d'un microphone) en un signal numérique, via un ADC.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* du du périphérique *PYBLASH* et copiez-y le code qui suit :

```python
# Objet du script : Mise en œuvre du capteur de son Grove

from time import sleep_ms	# Pour temporiser
from pyb import Pin, ADC	# Gestion des broches et du coonvertisseur 
							# analogique-numérique (ADC)

adc = ADC(Pin('A1')) # On "connecte" l'ADC à la broche A1

while True :
	sleep_ms(500) # Temporisation de 500 millisecondes
	print("Valeur de l'ADC (prop. volume sonore) : " + str(adc.read())) 
```

Une valeur entière, entre 0 et 1024, proportionnelle à l'intensité du son ambiant, sera affichée dans le terminal PuTTY toutes les 0.5s.
