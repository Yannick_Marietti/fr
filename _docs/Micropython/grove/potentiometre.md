---
title: Potentiomètre
description: Mise en œuvre du potentiomètre Grove avec MicroPython
---

# Potentiomètre

Ce tutoriel explique comment mettre en œuvre un potentiomètre analogique Grove avec MicroPython. Le potentiomètre, *rotary angle sensor* en anglais, permet de moduler manuellement une tension d’entrée comprise entre 0V et +3.3V (pour notre carte). Après conversion de cette tension (par l’ADC de la broche analogique sur laquelle est connecté le potentiomètre) le microcontrôleur reçoit une valeur entière comprise entre 0 et 4095.
Cette valeur peut ensuite servir à piloter d’autres actuateurs, par exemple à faire varier l’intensité sonore d’un buzzer.


## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. [Un potentiomètre Grove](https://wiki.seeedstudio.com/Grove-Rotary_Angle_Sensor/) connecté sur la **broche / le connecteur A1**.

**Le potentiomètre Grove :**
<div align="center">
<img alt="Le potentiomètre Grove" src="images/potentiometre.png" width="300px">
</div>

> Crédit images : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* et collez-y le code qui suit :

```python
# Objet du script :
# Exemple de configuration d'un ADC pour numériser une tension d'entrée variable grâce à un potentiomètre.
# Pour augmenter la précision, on calcule la moyenne de la tension d'entrée sur 500 mesures (1 mesure / milliseconde)
# Matériel requis : potentiomètre (Grove ou autre), idéalement conçu pour fonctionner entre 0 et 3.3V et connecté sur A0.

import pyb # pour gérer les GPIO
from time import sleep_ms # Pour temporiser

print( "L'ADC avec MicroPython c'est facile" )

# Tension de référence / étendue de mesure de l'ADC : +3.3V
varef = 3.3

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = varef / (RESOLUTION - 1)

# Initialisation de l'ADC sur la broche A0
adc_A0 = pyb.ADC(pyb.Pin( 'A0' ))

# Initialisations pour calcul de la moyenne
Nb_Mesures = 500
Inv_Nb_Mesures = 1 / Nb_Mesures

while True: # Boucle "infinie" (sans clause sortie)
	
	somme_tension = 0
	moyenne_tension = 0
	
	# Calcul de la moyenne de la tension aux bornes du potentiomètre

	for i in range(Nb_Mesures): # On fait Nb_Mesures conversions de la tension d'entrée
		
		# Lit la conversion de l'ADC (un nombre entre 0 et 4095 proportionnel à la tension d'entrée)
		valeur_numerique = adc_A0.read()
		
		# On calcule à présent la tension (valeur analogique) 
		tension = valeur_numerique * quantum

		# On l'ajoute à la valeur calculée à l'itération précédente
		somme_tension = somme_tension + tension

		# Temporisation pendant 1 ms
		sleep_ms(1)
	
	# On divise par Nb_Mesures pour calculer la moyenne de la tension du potentiomètre
	moyenne_tension = somme_tension * Inv_Nb_Mesures 
	
	# Affichage de la tension moyenne sur le port série de l'USB USER
	print( "La valeur moyenne de la tension est : %.2f V" %moyenne_tension)
````
## Affichage sur le terminal série de l'USB User

Lancez le script sous PuTTY avec CTR+D et faite tourner la molette du potentiomètre.<br>
Vous observez le défilement des valeurs de l'ADC qui s'affichent :

<div align="center">
<img alt="Sortie Potentiometre" src="../STARTWB55/images/output_potentiometre.png" width="500px">
</div>

