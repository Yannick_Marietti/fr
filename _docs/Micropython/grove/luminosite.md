---
title: Capteur de luminosité, photodiode
description: Mise en œuvre du module capteur de luminosité Grove avec MicroPython
---

# Capteur de luminosité, photodiode

Ce tutoriel explique comment mettre en œuvre un module capteur de lumière analogique Grove avec MicroPython.

<h1>Lecture de la luminosité</h1>

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module capteur de luminosité Grove](https://wiki.seeedstudio.com/Grove-Light_Sensor/)

**Le capteur de luminosité :**

<div align="center">
<img alt="Grove light sensor" src="images/capteur-de-lumiere.png" width="280px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

Ce capteur utilise une photorésistance afin de mesurer l'intensité lumineuse de son environnement, dont la valeur diminue lorsqu'elle est éclairée. Cet exemple est une nouvelle illustration, directe, de la conversion d'un signal en tension variable dans le temps (la réponse de la photodiode) en une réponse numérique, via un ADC.<br>
Si la conversion en une grandeur physique (exprimée en Lux) de l'échantillonnage de l'ADC est en théorie possible, c'est une opération difficile qui nécessite une description complète du capteur de luminosité utilisé, description malheureusement indisponible pour celui... Si vous souhaitez un capteur capable de mesurer avec précision l'intensité lumineuse ambiante en lux, nous vous conseillons le module Grove basé sur le [TSL2561](tsl2561).

Connectez le capteur sur la **broche A1**.

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Editez le fichier *main.py* et collez y le code qui suit, avant de l'enregistrer dans le répertoire du périphérique PYBLASH :<br>

```python
# Lecture et numérisation du signal d'un capteur Grove de luminosité (LS06-S phototransistor)
# Attention : le capteur doit être alimenté en 5V pour donner une réponse entre 0 et 4095.

from pyb import ADC, Pin # Convertisseur analogique-numérique et GPIO
from time import sleep # Pour les temporisations

# Instanciation et démarrage du convertisseur analogique-numérique
adc = ADC(Pin('A1'))

while True:
	# Numérise la valeur lue, produit un résultat variable dans le temps dans l'intervalle [0 ; 4095]
	Mesure = adc.read()
	print("Luminosité %d (unités arbitraires)" %Mesure)
	sleep(1) # Temporisation d'une seconde
```
Une valeur sera affichée dans le terminal série de l'USB user toutes les 0.5s.

<h1>Pour aller plus loin</h1>

Dans cette partie nous allons voir comment faire une lampe de nuit automatique. Pour cela nous allons utiliser une LED et une photorésistance.

## Matériel requis

1. Une carte d'extension de base Grove
2. La carte NUCLEO-WB55
3. Un capteur de luminosité Grove
4. Une LED couplée avec une résistance de 220 Ohms

**Régler l'intensité d'une LED :**
En temps normal, une LED est alimentée par une tension provenant d'une patte numérique qui ne peut fournir que deux niveaux : haut (0V) et bas (3,3V ou 5V). Par conséquent la LED sera soit éteinte, soit allumée à une intensité fixe.
Afin de moduler plus finement l'intensité d'une LED, *il faut lui fournir plusieurs niveau de tensions*. C'est là qu'intervient l'utilisation d'un timer pour générer un signal PWM, lequel passe de l'état haut à l'état bas à une fréquence élevée. Cette alternance de niveaux de tension (le rapport cyclique) est équivalente à appliquer une tension moyenne comprise entre les deux états. Vous l'avez compris : changer le rapport cyclique de la PWM permet  de changer la luminosité de la LED.

Par exemple : on a une PWM alimentée de 0V à 5V avec une fréquence de 1kHz et un rapport cyclique de 50%. On obtient alors 50% de 5V soit 2,5V ; la LED sera alimentée avec une tension équivalente à 2,5V.

Pour réaliser le montage, connectez le capteur de la photorésistance sur la **broche A1** et celui de la LED sur la **broche D3**.

## Le code MicroPython

```python
# Objet du script :
# Conception d'une veilleuse d'obscurité ... 
# - Lit l'intensité lumineuse ambiante avec un capteur Grove de luminosité (LS06-S phototransistor)
# - Allume une LED (module Grove) avec une intensité inversement proportionnelle à la lumière
#   ambiante.

from time import sleep_ms
from pyb import ADC, Timer, Pin

# Photorésistance sur A1 (analogique)
adc = ADC(Pin('A1'))

#LED sur D3 (sortie PWM)
led = Pin('D3')

# Tension de référence / étendue de mesure de l'ADC : +3.3V
varef = 3.3

# Résolution de l'ADC 12 bits = 2^12 = 4096 (mini = 0, maxi = 4095)
RESOLUTION = const(4096)

# Quantum de l'ADC
quantum = varef / (RESOLUTION - 1)

#Configuration du timer : timer 1 channel 3
tim1 = Timer(1, freq=1000)
ch3 = tim1.channel(3, Timer.PWM, pin=led)

while True:

	#Récupération de la valeur de la photorésistance
	valeur = adc.read()
	
	#Change l'intensité de luminosité de la LED connectée sur D3
	
	ch3.pulse_width_percent(1 - valeur * quantum)
	
	# Temporisation d'un dixième de seconde
	sleep_ms(100)
```

Mettez votre doigt sur le capteur, vous devriez remarquer que la LED augmente sa luminosité. A contrario, si vous mettez une source de lumière proche du capteur alors la LED devrait réduire sa luminosité, voire même s'éteindre complètement.
