---
title: Télémètres infrarouge VL53L0X et VL53L1X
description: Mise en œuvre du télémètre infrarouge VL53L0X Grove en MicroPython
---

# Télémètres infrarouge VL53L0X et VL53L1X

Ce tutoriel explique comment mettre en œuvre le [télémètre infrarouge VL53L0X de STMicroelectronics](https://www.st.com/en/imaging-and-photonics-solutions/vl53l0x.html) en MicroPython. Celui-ci utilise le protocole de communication I2C.<br>
Le VL53L0X mesure la distance selon le même principe qu'un sonar ou un radar *actif*. Il émet une onde qui se propage devant lui jusqu'à ce qu'elle rencontre un obstacle qui la réfléchit partiellement. Lorsque l'onde réfléchie atteint le capteur, celui-ci la détecte et mesure le temps de son aller-retour. Connaissant la vitesse de propagation de l'onde, le VL53L0X calcule la distance à l'obstacle qui l'a renvoyée vers lui, c'est pourquoi on dit qu'il s'agit d'un *capteur de temps de vol* (ou *time of flight* en anglais, ce qui donne l'acronyme ToF).<br>
Dans le cas d'un sonar l'onde utilisée est une *onde sonore* (des vibrations de l'air). Dans le cas du VL53L0X qui nous intéresse ici, c'est une *onde électromagnétique cohérente* (de la lumière laser) de fréquence (couleur) infrarouge.<br>
La mesure de la distance est plus rapide et plus précise qu'avec un sonar. La portée maximum du VL53L0X est de **deux mètres**.
Dans l'archive téléchargeable de cet exemple se trouve également *le pilote du [capteur VL53L1X](https://www.st.com/en/imaging-and-photonics-solutions/vl53l1x.html)*, une version plus récente du VL53L0X ayant une portée de **quatre mètres**.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un module [télémètre infrarouge VL53L0X Grove (Time of Flight distance sensor)](https://wiki.seeedstudio.com/Grove-Time_of_Flight_Distance_Sensor-VL53L0X/)
4. Un [module LED Grove](https://wiki.seeedstudio.com/Grove-LED_Socket_Kit/)

**Le télémètre infrarouge VL53L0X Grove :**

<div align="center">
<img alt="Grove - VL53L0X" src="images/grove-vl53l0x.jpg" width="500px">
</div>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

*Attention* le capteur de distance est situé au *verso* du module Grove, sur la face opposée à celle du connecteur !

## Le code MicroPython

Ce code a été adapté à partir [de ce site](https://GitHub.com/uceeatz/VL53L0X). 

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Il faut récupérer le fichier *VL53L0X.py* et le copier dans le répertoire du périphérique *PYBLASH*.
Le module télémètre devra être branché sur un connecteur I2C du Grove Base Shield et le module LED sur sa prise D6.

Le programme réalise les fonctions suivantes :
 - Mise en œuvre d'une PWM pour réaliser un [variateur de lumière](variateur_lumiere).
 - Contrôle de l'intensité lumineuse au moyen d'un capteur de distance VL53L0x.

L'intensité de la LED varie en fonction de la distance mesurée par le capteur (plus la distance est courte, plus la lumière est intense). Pour rendre l'application plus réaliste (mais aussi plus bruyante !) vous pouvez remplacer le module LED par un module buzzer de Grove, et vous fabriquerez ainsi un mini radar de recul.

Editez maintenant le script *main.py* :

```python
# Source : https://GitHub.com/uceeatz/VL53L0X
# Objets du Script : 
# - Mise en œuvre d'une PWM pour réaliser un variateur de lumière.
# - Contrôle de l'intensité lumineuse au moyen d'un capteur de distance VL53L0x.
# L'intensité de la LED varie en fonction de la distance mesurée par le capteur
# (plus la distance est courte, plus la lumière est intense).
# Matériel (en plus de la carte NUCLEO-WB55) :
# - un Grove Base Shield
# - une LED connectée sur D6 du Grove Base Shield.
# - un capteur Time of Flight Grove connecté sur une prise I2C du Grove Base Shield.

from pyb import Pin, Timer # Pilotes des entrées-sorties et des timers
from time import sleep_ms # Pour temporiser

# initialisation de la PWM 
p = Pin('D6') 
ti = 1 # Timer 
ch = 1 # Canal
tim = Timer(ti, freq=1000) # Fréquence de la PWM fixée à 1kHz
ch = tim.channel(ch, Timer.PWM, pin=p)
i = 0

# Déclage du capteur (en mm, différent pour chaque module)
SENSOR_OFFSET = const(0)

from machine import I2C
import VL53L0X # Bibliothèque pour le VL53L0X

#Initialisation du bus I2C numéro 1 du STM32WB55 
i2c = I2C(1)

# Pause d'une seconde pour laisser à l'I2C le temps de s'initialiser
sleep_ms(1000)

tof = VL53L0X.VL53L0X(i2c) # Instance du capteur

tof.start() # démarrage du capteur

while True:

	try:
		distance = max(tof.read() - SENSOR_OFFSET, 0) # mesure de distance
		print("Distance : %1d mm" %distance)
		# Pause de 5 millisecondes
		sleep_ms(5)
	except:
		print("Erreur de mesure")
		break
		
	# Convertit la distance en un nombre entre 0 et 100
	rapport_cyclique = 100 - min( distance // 10 , 100)
	
	# Applique ce rapport cyclique à la PWM de la LED 
	ch.pulse_width_percent(rapport_cyclique)
	
	# Pause de 5 millisecondes
	sleep_ms(5)

tof.stop() # arrêt du capteur
```

## Pour aller plus loin : afficher les distances avec ST BLE Sensor et utiliser le VL53L1X

Vous trouverez dans [**l'archive téléchargeable MODULES.ZIP**](../../../assets/Script/MODULES.zip) un autre ensemble de scripts qui démontre comment afficher les valeurs de distances renvoyées par le VL53L0X avec l'application smartphone, [**ST BLE Sensor**](../BLE/STBLESensor) via une connexion sans fil avec la radio BLE du STM32WB55.<br>
Vous y trouverez également un exemple avec une version plus récente du VL53L0X, **le VL53L1X**, ayant une portée de **quatre mètres** (merci à Julien NGUYEN).
