---
title: Tutoriels avec des modules Grove et autres
description: Tutoriels avec des modules Grove et autres
---

# Tutoriels avec des modules Grove et autres

## Démarrage

Pour la plupart de ces tutoriels, vous devrez disposer de la [*carte d’extension Grove pour Arduino*](https://wiki.seeedstudio.com/Base_Shield_V2/) (ou *Grove Base Shield for Arduino* en anglais).
C'est une carte au format Arduino qui vient se connecter sur la NUCLEO-WB55 et permet de brancher aisément des capteurs et des actionneurs digitaux, analogiques, UART et I2C avec la connectique propriétaire [Grove](http://wiki.seeedstudio.com/Grove_System/).

<br>
<div align="center">
<img alt="Grove base shield pour Arduino" src="images/grove_base_shield.jpg" width="670px">
</div>
<br>

> Crédit images : [Seeedstudio](http://wiki.seeedstudio.com/Grove_System/)

Elle offre :
* Six connecteurs D2, D3... pour des périphériques numériques c'est à dire pilotés par un niveau logique 0 (0V) ou 1 (+3.3V) ;
* Quatre connecteurs A0, A1... pour des périphériques qui envoient un signal analogique en entrée (entre 0 et +3.3V ou 0 et +5V selon la position du petit interrupteur qui l'équipe) ;
* Quatre connecteurs pour des périphériques dialoguant avec le protocole I2C ;
* Un connecteur pour un port série (UART) ;
* Un commutateur 3,3V / 5V qui permet de sélectionner la tension d'alimentation des modules que vous connecterez sur ses fiches.

## La NUCLEO-WB55 n'aime pas les modules 5V !

**Prenez garde aux tensions d'alimentation des modules externes que vous connectez sur la NUCLEO-WB55 !** Certains nécessitent du 5V, d'autres fonctionnent aussi bien avec du 5V et du 3.3V, d'autres enfin fonctionnent exclusivement en 3,3V et seront peut-être endommagés par du 5V. **Lisez bien leurs fiches techniques avant de les brancher !** <br>
Nous vous recommandons également de prendre connaissance des [**précautions d'utilisations des cartes NUCLEO exposées dans ce tutoriel**](../../Kit/nucleo).


Dans la liste des périphériques qui nécessitent d'être alimentés **exclusivement** en 5V sur la NUCLEO-WB55, ci-dessous, figure seulement [le module LCD RGB Grove](lcd_RGB_16x2). Il présente **une autre particularité** : il ne dispose pas des résistances de tirage (pull-up) requises par le bus I2C auquel il est connecté et ne fonctionnera pas si ces résistances ne sont pas ajoutées par vos soins.

D'une façon générale, **la plus grande attention** est requise si vous souhaitez utiliser des périphériques fonctionnant exclusivement en 5V avec la NUCLEO-WB55. 
- S'il s'agit de capteurs qui injectent du courant dans le microcontrôleur, **ils pourraient purement et simplement détruire celui-ci**. Donc, si vous ne maîtrisez pas le sujet, nous vous conseillons vivement de **ne pas en utiliser du tout**.
- S'il s'agit de périphériques en logique 5V **pilotés** par le STM32, ils devraient fonctionner correctement.

Il est à noter que la plupart des modules actuels sont vendus pour être alimentés en 5V ou en 3,3V (grâce à un régulateur de tension). Ils ne posent bien sûr aucun problème.

## Précision importante concernant l'adressage sur le bus I2C

Vous constaterez que la plus grande partie des modules présentés ci-après utilisent **le protocole I2C** et doivent donc être connectés au bus du même nom sur la carte NUCLEO-WB55. I2C est l'acronyme de "Inter-Integrated Circuit" (en français : bus de communication intégré inter-circuits). Il s'agit d'un bus série fonctionnant selon un protocole inventé par Philips. Pour dialoguer sur un bus I2C, chaque module "esclave" qui s'y trouve branché **est identifié par une adresse codée sur 7 bits** afin de dialoguer avec le contrôleur maître intégré au STM32WB55.

Avec la prolifération des modules I2C, **il pourrait arriver que deux ou plusieurs modules que vous auriez connectés sur un bus I2C aient la même adresse**, ce qui conduirait inévitablement à des plantages. Pour éviter ce type de conflits, vous devrez consulter les fiches techniques de vos modules et, pour ceux qui le permettent, prendre soin de modifier (si nécessaire) leur adresse I2C, généralement codée dans leur firmware.

Les tableaux ci-dessous rappellent, entre parenthèses, dans la colonne *Connectique/protocole*, l'adresse I2C par défaut - celle qui est écrite dans la classe pilote - des différents modules que nous avons utilisés. Si vous choisissez d'autres modules, il est possible que leur adresse change et que vous soyez obligé de la passer en paramètre lors de l'instanciation de leur pilote. Ce sera parfois le cas, par exemple, selon que vous utiliserez un même capteur implémenté dans un module Grove de Seeed Studio ou dans un module Adafruit.

## Liste des tutoriels 

Vous trouverez ici quelques tutoriels essentiellement [pour des modules au format Grove de la société Seeed Studio](http://wiki.seeedstudio.com/Grove_System/). 
Ils se transposeront facilement à des modules d'autres fabricants pour peu qu'ils utilisent les mêmes protocoles et bus. 

Nous aborderons et expliquerons au gré des tutoriels des notions de programmation embarquée et d'architecture des microcontrôleurs qui vous seront indispensables pour progresser. Lorsque vous rencontrerez un acronyme que vous ne connaissez pas, n'hésitez pas à consulter le [glossaire](../../Kit/glossaire).

**1. Afficheurs & LED**

|**Tutoriel**|**Connectique / protocole**|
|:-|:-:|
[Afficheur 4x7 segments TM1637](tm1637)|analogique/spécifique|
[Afficheur 8x7 segments TM1638](tm1638)|numérique/spécifique|
[Afficheur Grove LCD 16 caractères x 2 lignes V2.0 (ou plus)](lcd_16x2)|I2C (0x3E)|
[Afficheur Grove LCD RGB 16 caractères x 2 lignes](lcd_RGB_16x2)|I2C (rétroéclairage : 0x62, afficheur : 0x3E)|
[Afficheur matrices de LED 8x8 MAX7219](max7219)|SPI|
[Afficheur OLED 1308](oled_1308)|I2C (0x3C)|
[Anneau LED Neopixel WS2812B](anneau_ws2812b)|numérique/spécifique|
[Barre de LED MY9221](ledbar_MY9221)|numérique|
[LED infrarouge](del_ir)|numérique|
[LED RGB chainable](led_rgb_chainable)|numérique/spécifique|
[LED RGB Neopixel (WS21813 Mini)](led_rgb_neopixel)|numérique/spécifique|
[Rubans Neopixel WS2812B et WS2813](ws2812b)|SPI ou numérique/spécifique|

**2. Capteurs spatiaux et de mouvement**

|**Tutoriel**|**Connectique/protocole**|
|:-|:-:|
[Accéléromètre 3 axes MMA7660FC](MMA7660FC)|I2C (0x4C)|
[Capteur de choc](knock)|numérique|
[Capteur de gestes PAJ7620U2](paj760u2)|I2C (0x73)|
[Capteur d'inclinaison](inclinaison)|numérique|
[Centrale inertielle LSM303D](LSM303D)|I2C (0x1E)|
[Détecteur de mouvement PIR](mouvement)|numérique|
[Module suiveur de ligne](line_finder)|numérique|
[Module GPS](gps)|UART|
[Sonar à ultrasons](distance_ultrason)|UART|
[Télémètres infrarouge VL53L0X et VL53L1X](vl53l0x)|I2C (0x29)|

**3. Capteurs environnementaux**

|**Tutoriel**|**Connectique/protocole**|
|:-|:-:|
[Capteur de dioxyde de carbone MH-Z16](mhz16)|UART|
[Capteur de dioxyde de carbone SCD30](scd30)|I2C (0x61)|
[Capteur de gaz multicanaux MiCS-6814](MICS6814)|I2C (0x04)|
[Capteur de gouttes de pluie](raindrop)|analogique/ADC|
[Capteur de lumière numérique TSL2561](tsl2561)|I2C (0x29)|
[Capteur de lumière numérique TSL2591](tsl2591)|I2C (0x29)|
[Capteur de luminosité, photodiode](luminosite)|analogique/ADC|
[Capteur de lumière solaire SI1145](SI1145)|I2C (0x60)|
[Capteur de niveau d'eau](water_sensor)|analogique/ADC|
[Capteur de pression, température et humidité BME280](bme280)|I2C (0x76)|
[Capteur de pression, température, humidité et qualité d'air BME680](bme680)|I2C (0x76)|
[Capteur de pression et température BMP280](bmp280)|I2C (0x77)|
[Capteur de qualité de l'air SGP30](sgp30)|I2C (0x58)|
[Capteur de température et d'humidité SHT31](sht31)|I2C (0x44)
[Capteur de température et d'humidité TH02](th02)|I2C (0x40)
[Capteur de température de précision MCP9808](mcp9808)|I2C (0x18)|
[Capteurs de température et d'humidité DHT11 et DHT22](DHT)|numérique/spécifique|
[Capteur de température (thermistance)](NCP18WF104F03RB)|analogique/ADC|
[Capteur de pression intégré MPX5700AP](MPX5700AP)|analogique/ADC|
[Capteurs d'humidité du sol](soil_moisture)|analogique/ADC|
[Compteur de particules HM3301](HM3301)|I2C (0x40)|
[Sonomètre](bruit)|analogique/ADC|
[Sonde étanche de température DS18X20](ds1820)|numérique/One-Wire|
[Thermistance (explication complète)](thermistance)|analogique/ADC|

**4. Entrée, enregistrement et saisie de données**

|**Tutoriel**|**Connectique/protocole**|
|:-|:-:|
[Adaptateur Nintendo NunChuk](nunchuk)|I2C (0x52)|
[Bouton et anti-rebond](bouton)|numérique|
[Clavier matriciel](keypad)|numérique|
[Interrupteur tactile](toucher)|numérique|
[Joystick](joystick)|analogique/ADC|
[Lecteur RFID 125 kHz Grove](RFID125kHz)|UART|
[Manette Nintendo SNES](snes)|SPI|
[Matrice d'interrupteurs tactiles TTP226](TTP226)|numérique|
[Module carte SD](sd_card_module)|SPI|
[Module RFID RC522 13.56 MHz](rfid)|SPI|
[Potentiomètre](potentiometre)|analogique/ADC|

**5. Actuateurs**

|**Tutoriel**|**Connectique/protocole**|
|:-|:-:|
[Buzzer](buzzer)|numérique/PWM|
[Contrôle moteur avec un pont en H](moteur)|numérique/PWM|
[Contrôle moteur avec un relais ou un transistor](relais_transistor)|numérique|
[Contrôle moteur pas à pas](moteur_pas_a_pas)|numérique|
[Servomoteur](servo)|numérique/PWM|

**6. Autres modules et petits projets**

|**Tutoriel**|**Connectique/protocole**|
|:-|:-:|
[Alarme de mouvement](alarme)|numérique/PWM|
[Emetteur - Récepteur code Morse](morse)|numérique & analogique|
[Horloge temps-réel DS1307](rtc_DS1307)|I2C (0x68)|
[Horloge temps-réel PCF85063TP](rtc_PCF85063TP)|I2C (0x51)|
[Variateur de lumière](variateur_lumiere)|numérique/PWM|
[Station météo inspirée du produit Ikea Klockis](../../../assets/projects/station_meteo_klockis.zip)|I2C|
