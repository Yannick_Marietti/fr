---
title: Clavier matriciel
description: Mise en œuvre de la lecture d'une touche avec un clavier matriciel
---

# Keypad

Ce tutoriel explique comment récupérer la touche appuyée sur un clavier matriciel avec MicroPython.

<h2>Description</h2>

Un clavier matriciel, également appelé keypad en anglais, permet de récupérer des caractères (chiffres 0 à 9, lettres A à D, symboles * et #).

Ses touches sont disposées en matrice de 4x4 ou 3x4 ; ce sont de simples boutons poussoirs.
Le dispositif est passif et comprend 7 ou 8 pattes physiques :
 - 4 pattes reliées aux touches d'une même colonne
 - 3 ou 4 pattes reliées aux touches d'une même ligne

<br>
<div align="center">
<img alt="Fonctionnement keypad" src="images/keypad-decomposition.png" width="80%">
</div>
<br>

> Crédit image : [Mbed](https://os.mbed.com/components/Keypad-12-Button-COM-08653-ROHS/)

Le clavier doit être utilisé comme indiqué comme ci-dessous :
1. Quatre pattes de la carte doivent être configurées comme sorties et quatre autres comme entrées. Des résistances de tirage peuvent être ajoutées afin de fixer l'état logique quand aucune touche n'est pressée.
2. Les pattes en sorties sont mises à l'état haut et on lit l'état des pattes en entrée. En pressant une touche cela fera apparaître un état logique haut sur une des pattes d'entrée.
3. En combinant les 0 et les 1 sur les pattes en sortie on peut déterminer quel bouton est pressé.


<h2>Montage</h2>

**Montage 1 :** Pour un clavier 3x4 on réalise le montage suivant :

<br>
<div align="center">
<img alt="Schéma de montage 1 keypad" src="images/keypad-schema1.png" width="70%">
</div>
<br>

Lors du câblage faites attention à bien laisser une patte à chaque extremum de la connectique pour ce clavier ci.

**Montage 2 :** Pour un clavier 4x4 on réalise le montage suivant :

<br>
<div align="center">
<img alt="Schéma de montage 2 keypad" src="images/keypad-schema2.png" width="70%">
</div>
<br>

Comme expliqué précédemment le dispositif est passif et n'a donc pas besoin d'alimentation. On peut également rajouter des résistances de tirage de 1kOhms.

<h2>Le code MicroPython</h2>

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Afin de simplifier le code et le rendre plus propre nous avons créé une bibliothèque externe *keypad.py*. Téléchargez-la et déposez-la dans la carte NUCLEO-WB55. Cette bibliothèque nous permet d'éviter de programmer les étapes décrites plus haut. Cependant vous pouvez toujours l'ouvrir et observer la structure du code.

**Etape 1 :** Pour faire fonctionner le programme nous devons importer la bibliothèque téléchargée précédemment.
Dans le cas du **montage 1** avec un clavier 3x4 :

```python
from keypad import Keypad3x4
```
Dans le cas du **montage 2** avec un clavier 4x4 :

```python
from keypad import Keypad3x4
```

**Etape 2 :** On crée l'entité du clavier.
Dans le cas du **montage 1** avec un clavier 4x4 :

```python
clavier = Keypad3x4()
```

Dans le cas du **montage 2** avec un clavier 4x4 :

```python
clavier = Keypad4x4()
```

**Etape 3 :** Enfin on lis la touche et on l'affiche dans le terminal série.

```python
while True:
	touche = clavier.read_key()
	print(touche)
```

<h2>Résultat</h2>

Il ne vous reste plus qu'à sauvegarder l'ensemble puis à redémarrer votre carte NUCLEO-WB55. Essayez les différentes touches, celles-ci devraient s'afficher dans le moniteur série.

<h2>Pour aller plus loin</h2>

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Vous trouverez dans le fichier *Clavier matriciel/Pour aller plus loin/*  de l'archive *MODULES.zip* le script MicroPython *main.py* qui implémente un digicode. Ce script n'utilise pas la bibliothèque *keypad.py** et permet peut être de mieux comprendre le principe de fonctionnement du keypad.

**Principe de fonctionnement du digicode :** 
- On définit un code par défaut (123A).
- L'utilisateur peut saisir en boucle un code à 4 chiffres.
- Si le code de l'utilisateur correspond au code en mémoire alors la LED verte de la carte s'allume pendant 2 secondes et le message "Code juste" apparaît sur le terminal. 
- En revanche, si les codes ne correspondent pas, la LED rouge s'allume et le message "Code faux" est inscrit sur le terminal.

L'utilisateur peut choisir de lui-même définir le code secret en pressant le premier bouton poussoir de la carte (SW1). La LED bleue s'allume pendant toute la durée de la saisie du nouveau code et 2 secondes après. Sur le terminal, on peut voir l'instruction de saisir un nouveau code et ce code est inscrit une fois qu'il a été renseigné.

**Attention, on ne peut pas saisir 2 fois le même caractère à la suite avec ce code !**

En cas de problème vérifiez votre câblage et/ou votre code pour voir si vous utilisez bien le bon type de clavier.
