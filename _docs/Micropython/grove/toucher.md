---
title: Interrupteur tactile
description: Mise en œuvre d'un interrupteur tactile avec MicroPython
---

# Interrupteur tactile

Ce tutoriel explique comment mettre en œuvre un interrupteur tactile avec MicroPython. Il sera dans l’état *ON* s’il y a contact et *OFF* dans le cas contraire.

## Matériel requis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [module capteur tactile Grove](https://wiki.seeedstudio.com/Grove-Touch_Sensor/)

**L'interrupteur tactile Grove (Touch sensor) :**

<br>
<div align="center">
<img alt="Grove touch sensor" src="images/grove_touch_sensor.jpg" width="350px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

On trouve bien sûr d'autres implémentations matérielle du capteur tactile que celle de SeeedStudio. Voici par exemple une version qui doit être câblée "à la main". Le capteur ne possède que 3 broches qu'il faut connecter à GND, VCC (alimentation, câbles rouge et noir) et D4 (signal, câble jaune).

<br>
<div align="center">
<img alt="Câblage interrupteur tactile" src="images/capteur_tactile.png" width="400px">
</div>
<br>

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Editez maintenant le script *main.py* sur le disque *PYBFLASH* et collez-y le code qui suit :

```python
# Objet du script : Mise en œuvre d'un capteur/interrupteur tactile

from pyb import Pin
from time import sleep_ms # Pour temporiser

p_in = Pin('D4', Pin.IN, Pin.PULL_UP)

while True :
	time.sleep_ms(500) # Temporisation de 500 millisecondes

	if p_in.value() == 1: # Si on touche le capteur
		print("ON")
	else: # Autrement
		print("OFF")
```
