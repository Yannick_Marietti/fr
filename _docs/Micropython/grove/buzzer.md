---
title: Buzzer et PWM
description: Mise en œuvre du buzzer Grove avec MicroPython et explication de la PWM
---

# Buzzer et PWM

Ce tutoriel explique comment mettre en œuvre un buzzer Grove à l'aide d'un signal **modulé en largeur d'impulsion (PWM)**, une technologie que nous allons présenter à cette occasion.

## Prérequis

1. Une [carte d'extension de base Grove](https://wiki.seeedstudio.com/Base_Shield_V2/)
2. La carte NUCLEO-WB55
3. Un [buzzer Grove](https://wiki.seeedstudio.com/Grove-Buzzer/)

**Le buzzer Grove :**

Branchez le buzzer sur le connecteur D3 du Grove base shield.
Celui-ci vibre et produit un son lorsqu'on lui transmet une tension.
Il est possible de modifier la fréquence du son _en générant un signal de type PWM_ sur la broche de commande du buzzer.

<br>
<div align="center">
<img alt="Grove buzzer" src="images/buzzer.png" width="180px">
</div>
<br>

> Crédit image : [Seeed Studio](http://wiki.seeedstudio.com/Grove_System/)

## Qu'est-ce que la PWM ?

La _modulation en largeur d'impulsion (PWM pour "Pulse Width Modulation")_ est une méthode qui permet de simuler un signal analogique en utilisant une source numérique.
On génère un signal carré, de fréquence donnée, qui est à +3.3V (haut) pendant une proportion paramétrable de la période et à 0V (bas) le reste du temps. C’est un moyen de moduler l’énergie envoyée à un périphérique. Usages : commander des moteurs, faire varier l’intensité d’une LED, faire varier la fréquence d’un buzzer... La proportion de la durée d'une période pendant laquelle la tension est dans l'état "haut" est appelée _rapport cyclique_. La figure ci-après montre trois signaux PWM avec des rapports cycliques de 25%, 50% et 75% (de bas en haut).

<br>
<div align="center">
<img alt="Grove buzzer" src="images/pwm.png" width="700px">
</div>
<br>

**Programmer des PWM sur la carte NUCLEO-WB55**

En pratique, la génération de signaux PWM est l’une des fonctions assurées par les _timers_, des composants intégrés dans le microcontrôleur qui se comportent comme des compteurs programmables. Le STM32WB55 contient plusieurs timers, et chacun d'entre eux pilote plusieurs _canaux_ (ou _channels_ en anglais). Certains canaux de certains timers sont finalement connectés à des broches de sorties du Microcontrôleur (GPIO). Ces broches là pourront être utilisées pour générer des signaux de commande PWM.

 MicroPython permet de programmer facilement des broches en mode sortie PWM, mais vous aurez cependant besoin de connaitre :
 1. Quelles broches de la NUCLEO-WB55 sont des sorties PWM possibles ;
 2. A quels timers et canaux sont connectées ces broches dans le STM32WB55.
 
 La figure et la table ci-dessous fournissent ces deux informations :

<br>
<div align="center">
<img alt="PWM WB55" src="images/pwm_wb55.jpg" width="450px">
<img alt="PWM WB55" src="images/pwm_wb55_table.jpg" width="250px">
</div>
<br>

## Le code MicroPython

> **Les scripts présentés ci-après sont disponibles dans [la zone de téléchargement](../../../assets/Script/MODULES.zip)**.

Le code consiste à faire lire "en boucle" au buzzer un tableau *frequency* encodant une gamme de notes musicales. Le canal et le timer de la broche PWM D3 utilisée sont identifiés à partir du plan de mappage de la figure ci-dessus.

```python
# Objet du script : Jouer un jingle sur un buzzer (Grove ou autre).
# Cet exemple fait la démonstration de l'usage de la PWM pour la broche D3 sur laquelle est
# branché le buzzer.

from pyb import Pin, Timer

# Liste des notes qui seront jouées par le buzzer
frequency = [262, 294, 330, 349, 370, 392, 440, 494]

# D3 génère une PWM avec TIM1, CH3
BUZZER = Pin('D3')

while True :
	# Itération entre 0 et 7
	for i in range (0,7) :
		# On ajuste la fréquence pendant l'itération
		tim1 = Timer(1, freq=frequency[i])
		ch3 = tim1.channel(3, Timer.PWM, pin=BUZZER)
		# Rapport cyclique paramétré à 5% (le buzzer est alimenté 5% du temps d'une période)
		ch3.pulse_width_percent(5)
```
