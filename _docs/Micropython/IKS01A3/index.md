---
title: Tutoriels pour la carte d'extension X-NUCLEO IKS01A3
description: Tutoriels en MicroPython pour la carte d'extension X-NUCLEO IKS01A3 
---

# Tutoriels pour la carte d'extension X-NUCLEO IKS01A3

## Présentation de la X-NUCLEO IKS01A3

La carte d’extension [X-NUCLEO IKS01A3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html#overview) fait la démonstration de plusieurs capteurs MEMS (systèmes micro-électro-mécaniques) de STMicroelectronics :

* [LSM6DSO](https://www.st.com/content/st_com/en/products/mems-and-sensors/inemo-inertial-modules/lsm6dso.html#tools-software) : Accéléromètre 3D + Gyroscope 3D
* [LIS2MDL](https://www.st.com/content/st_com/en/products/mems-and-sensors/e-compasses/lis2mdl.html) : Magnétomètre 3D
* [LIS2DW12](https://www.st.com/content/st_com/en/products/mems-and-sensors/accelerometers/lis2dw12.html) : Accéléromètre 3D
* [LPS22HH](https://www.st.com/content/st_com/en/products/mems-and-sensors/pressure-sensors/lps22hh.html) : Baromètre (260 à 1260 hPa)
* [HTS221](https://www.st.com/content/st_com/en/products/mems-and-sensors/humidity-sensors/hts221.html) : Capteur d’humidité relative
* [STTS751](https://www.st.com/content/st_com/en/products/mems-and-sensors/temperature-sensors/stts751.html) : Capteur de température de précision (–40 °C à +125 °C)

Les photos qui suivent montrent :
- Une X-NUCLEO IKS01A3 connectée (par les connecteurs Arduino) sur une NUCLEO-WB55 (à gauche).
- La liste et la position des différents capteurs installés sur une  X-NUCLEO IKS01A3 (à droite).

|**X-NUCLEO IKS01A3 sur NUCLEO-WB55**|**Capteurs de la X-NUCLEO IKS01A3**|
|:---:|:---:|
|<img alt="X-NUCLEO IKS01A3 connectée à NUCLEO_WB55" src="images/WB55_IKS01.jpg" width="410px">|<img alt="X-NUCLEO IKS01A3" src="images/IKS01A3.png" width="380px">|

Une fois la carte d'extension placée sur les connecteurs Arduino, ses capteurs sont raccordés au bus *I2C* de la carte NUCLEO-WB55. Lorsque vous la branchez assurez-vous de bien respecter le marquage des connecteurs (i.e. le bon alignement avec les connecteurs Arduino) : CN9 -> CN9, CN5 -> CN5, etc.

La carte d’extension X-NUCLEO IKS01A3 dispose également d'un emplacement au format DIL 24 broches pour lui ajouter des capteurs I2C supplémentaires (par exemple, le gyroscope [A3G4250D](https://www.st.com/en/evaluation-tools/steval-mki125v1.html)).

**Remarque :** De nombreux modules I2C proposés par Seeed Studio, DFRobot et Adafruit (entre autres sociétés), qui utilisent les mêmes MEMS que la X-NUCLEO IKS01A3. De ce fait, ils fonctionneront sans modifications importantes des fichiers *main.py* que nous proposons et des classes contenant leurs pilotes. 

### Précisions sur les capteurs MEMS

Les capteurs MEMS ont révolutionné les applications électroniques et sont à la base – avec des capteurs d’image et de distance sans cesse plus performants – de nombreuses fonctions incontournables des smartphones et des objets connectés en général.  Ils sont également les composants clés des centrales inertielles des drones et des casques et manettes de réalité virtuelle. Une excellente introduction aux technologies MEMS est disponible [dans cette fiche EDUSCOL](https://eduscol.education.fr/sti/sites/eduscol.education.fr.sti/files/ressources/pedagogiques/5616/5616-les-technologies-mems-ens.pdf).

Les MEMS les plus communs sont :

- *L'accéléromètre tridimensionnel*. Ce MEMS mesure les accélérations *(a<sub>x</sub>, a<sub>y</sub>, a<sub>z</sub>)* appliquées selon trois axes orthogonaux *(x, y, z)*, exprimées en millivolts par g (mV/g). Si l'objet sur lequel il est fixé est immobile et posé sur le sol (terrestre), l'accéléromètre mesurera l'accélération de la gravité, dirigée vers le centre de la Terre, constante et d'intensité 1g ou encore 9.81 m/s<sup>2</sup>. Si son support se déplace, l'accéléromètre mesurera en plus les accélérations dynamiques du mouvement, les vibrations, etc. Un accéléromètre est donc très sensible aux signaux parasites, encore appelés *bruit*.<br>
L'accéléromètre 3D est adapté aux applications comme la détection de mouvement, la reconnaissance de mouvement, l'orientation d'affichage et la détection de chute libre.

- *Le gyroscope tridimensionnel*. Ce MEMS mesure les vitesses angulaires de rotation *(g<sub>x</sub>, g<sub>y</sub>, g<sub>z</sub>)* autour de trois axes orthogonaux *(x, y, z)*, exprimées en radians par seconde (rad/s). Il est très précis pour détecter des désorientations et est insensible au bruit, mais son utilisation pour calculer des angles de rotation est délicate car elle nécessite d'intégrer (sommer) au cours du temps les vitesses angulaires. Cette technique cumule rapidement les erreurs de mesure ; on dit que le gyroscope *dérive*.<br>
Le gyroscope 3D est adapté aux applications comme le contrôle de mouvement, les appareils électroménagers et la robotique. Pour des applications plus exigeantes, pour stabiliser un drone par exemple, on devra le coupler avec un accéléromètre 3D et avoir recours à des algorithmes de [*fusion de données*](https://fr.wikipedia.org/wiki/Fusion_de_donn%C3%A9es).  

- *Le magnétomètre tridimensionnel*. Ce MEMS mesure l'intensité du champ magnétique *(m<sub>x</sub>, m<sub>y</sub>, m<sub>z</sub>)* autour de trois axes orthogonaux *(x, y, z)*, exprimées en milli gauss (mg) ou en microteslas (µT). Ces capteurs sont très sensibles aux matériaux qui les environnent et doivent généralement *être calibrés*.<br>
Pour en savoir plus à leur sujet, notamment leur application immédiate consistant à programmer une boussole afin de suivre un cap, nous vous invitons à suivre [ce tutoriel](lis2mdl).

- *Le capteur de pression*. Ce MEMS mesure la pression atmosphérique absolue, exprimée en millibars (mbar) ou en hectopascals (hPa, identique au mbar). Sa précision étonnante permet de détecter des variations de pression correspondant à des variations d'altitude de quelques centimètres. Tl est donc utilisé non seulement pour des mesures de pression en météo mais aussi pour des applications d'altimétrie (calculs de vitesses ascensionnelles, altimètres de randonnée, etc.).

- *Le capteur de température*. Ce MEMS mesure la température ambiante, exprimée en degrés Celsius (°C). La mise en œuvre d'une mesure de température fiable, notamment lorsqu'on souhaite utiliser le résultat pour des mesures environnementales, est très délicate.

- *Le capteur d'humidité relative*. Ce MEMS donne une estimation de l'humidité relative, exprimée en pourcentage (%). À 100 % d'humidité relative, l'eau en suspension dans l'air se condense et apparaît sous forme de gouttelettes (brouillard). Avec un taux d'humidité moindre, l'air qui s'assèche devient de plus en plus limpide. Le taux d'humidité relative correspond peu ou prou à la probabilité de condensation de l'eau en suspension dans l'air à température et pression données.

### Orientation des axes des accéléromètres, magnétomètres et gyroscopes

Le schéma ci-dessous précise comment sont définis les axes *x*, *y* et *z* pour le LIS2MDL (magnétomètre), le LIS2DW12 (accéléromètre) et le LSM6DSO (accéléromètre et gyroscope). Ces informations sont indispensables à la bonne programmation de tous les algorithmes exploitant ces MEMS pour calculer des orientations dans l'espace.

<div align="center">
<img alt="X-NUCLEO IKS01A3" src="images/IKS01A3_MEMS_axis.jpg" width="450px">
</div>
<br>

## Liste des tutoriels et précisions sur le bus I2C

Le tableau qui suit donne la liste des tutoriels proposés avec la X-NUCLEO IKS01A3.<br>
Vous constaterez qu'ils utilisent **le protocole I2C** et sont donc connectés au bus du même nom sur la carte NUCLEO-WB55.<br>
I2C est l'acronyme de "Inter-Integrated Circuit" (en français : bus de communication intégré inter-circuits). Il s'agit d'un bus série fonctionnant selon un protocole inventé par Philips. Chaque module "esclave" branché sur celui-ci **est identifié par une adresse unique codée sur 7 bits** (rappelée en dernière colonne du tableau).<br>
Avec la prolifération des modules I2C, **il pourrait arriver que deux ou plusieurs modules que vous auriez connectés sur un bus I2C aient la même adresse**, ce qui conduirait inévitablement à des plantages. Pour éviter ce type de conflits, vous devrez consulter les fiches techniques de vos modules et, pour ceux qui le nécessiteraient (et le permettraient), prendre soin de modifier leur adresse I2C, généralement codée dans leur firmware, à l'aide de cavaliers ou encore de points de soudure.

<br>

|**Tutoriel**|**Bus**|**Adresse**|
|:-|:-:|:-:|
[Mise en œuvre de l'accéléromètre LIS2DW12 et programmation d'un inclinomètre](lis2dw12)|I2C|0x19|
[Mise en œuvre de l'accéléromètre et du gyroscope LSM6DSO](lsm6dso)|I2C|0x6B|
[Mise en œuvre du magnétomètre LIS2MDL et programmation d'une boussole](lis2mdl)|I2C|0x1E, 0x19|
[Mise en œuvre du baromètre et thermomètre LPS22HH et programmation d'un altimètre](lps22hh)|I2C|0x5D|
[Mise en œuvre du capteur de température et d'humidité relative HTS221](hts221)|I2C|0x5F|
[Mise en œuvre du capteur de température de précision STTS751](stts751)|I2C|0x4A|
[Mise en œuvre de la fusion de données](fuse)|I2C|0x6B, 0x1E|
