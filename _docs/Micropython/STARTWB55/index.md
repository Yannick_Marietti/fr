---
title: Démarrer avec MicroPython et la NUCLEO-WB55
description: Démarrer avec MicroPython et la NUCLEO-WB55
---

# Démarrer avec MicroPython et la NUCLEO-WB55

Cette section propose quelques exercices pour débuter la programmation en MicroPython avec la carte NUCLEO-WB55. Nous aborderons et expliquerons au gré des tutoriels des notions de programmation embarquée et d'architecture des microcontrôleurs qui vous seront indispensables pour progresser. Lorsque vous rencontrerez un acronyme que vous ne connaissez pas, n'hésitez pas à consulter le [glossaire](../../Kit/glossaire).

## Présentation vidéo de la carte par les élèves du lycée Vilgénis à Massy

Pour commencer, nous vous invitons à visionner cette excellente vidéo Youtube de présentation de la carte :

<br>

<iframe width="800" height="600" src="https://www.youtube.com/embed/882OQxTISWI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<br>

**Nous remercions M. Jacques Taillet, enseignant au lycée Vilgénis à Massy (Académie de Versailles) et les élèves de son club scientifique pour la réalisation et le partage de ce document !**


## Liste des tutoriels 

Les liens suivants donnent quelques exemples de mise en œuvre de la carte NUCLEO-WB55. Nous rappelons que l'ensemble des scripts présentés ou utilisés par la suite pourront être récupérés sur la [**page Téléchargements**](../Telechargement).

- [Appel des broches Arduino](arduino_pins)
- [Faire clignoter une LED](blink)
- [Programmer un chenillard](chenillard)
- [Faire clignoter plusieurs LED simultanément](blink_many)
- [Programmer un ou plusieurs boutons](bouton)
- [Gérer des boutons par interruptions](bouton_it)
- [Echantillonner une valeur analogique](adc)
- [Mise en œuvre du bus I2C avec un afficheur OLED](oled)
- [Mise en œuvre de la PWM avec un buzzer](../grove/buzzer)
- [Utiliser la communication série](uart)
- [Utiliser l'horloge temps-réel (RTC) intégrée au STM32WB55](rtc)
- [Utiliser le chien de garde indépendant (IDWG) intégré au STM322WB55](watchdog)
- [Utiliser le Bluetooth Low Energy](../BLE/index)

