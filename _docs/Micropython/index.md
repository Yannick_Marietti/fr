---
title: MicroPython pour STM32
description: MicroPython pour STM32 sur la NUCLEO-WB55
---

# STM32python : MicroPython pour STM32

L'initiative STM32python illustre comment mettre en œuvre MicroPython sur la famille de microcontrôleurs STM32 de STMicroelectronics.
MicroPython supporte [une longue liste de cartes de prototypage (NUCLEO et DISCOVERY) équipées de STM32](https://MicroPython.org/stm32/) et nous avons choisi dans un premier temps [la NUCLEO-WB55](../Kit/nucleo_wb55rg) qui présente l'avantage d'embarquer le protocole de communication radiofréquence [**Bluetooth Low Energy**](BLE). 

<br>
<div align="center">
<img align="center" src="images/equation_stm32python.jpg" alt="stm32duino" width="400"/>
</div>
<br>


Vous trouverez dans cette partie un grand nombre de tutoriels MicroPython développés pour la carte NUCLEO-WB55. A l'exception de ceux qui utilisent les fonctions BLE spécifiques à cette carte et moyennant quelques ajustements sur les noms des broches utilisées, tous les programmes que nous partageons devraient se transposer sans difficultés aux autres cartes des gammes NUCLEO et DISCOVERY de STMicroelectronics.

<br>

## Quelques mots sur MicroPython

**MicroPython** est une implémentation légère et efficace du langage de programmation Python 3 incluant un petit sous-ensemble de la bibliothèque standard Python et optimisée pour fonctionner sur des microcontrôleurs et dans des environnements contraints.

MicroPython regorge de fonctionnalités avancées telles qu'une invite en lignes de commandes, la capacité de réaliser des opérations mathématique sur des entiers de taille arbitraire, la gestion de listes, la gestion des exceptions et plus encore. Pourtant, il est suffisamment compact pour tenir et fonctionner dans seulement 256 Ko d'espace de code et 16 Ko de RAM.

* [Site web](http://MicroPython.org/)
* [Bibliothèques](http://docs.MicroPython.org/en/latest/library/index.html)

<br>

## Sommaire

* [Guide de démarrage rapide sous Windows](install_win)
* [Guide de démarrage rapide sous Linux](install_linux)
* [Le firmware MicroPython pour la NUCLEO-WB55](firmware)
* [Tutos avec la NUCLEO-WB55](STARTWB55)
* [Tutos avec la X-NUCLEO-IKS01A3](IKS01A3)
* [Tutos avec des modules Grove & autres](grove)
* [Tutos avec le BLE](BLE)
* [Téléchargements](Telechargement)
* [Liens utiles](ressources)
* [Foire aux Questions](../Kit/faq)