---
title: Présentation du BLE
description: Le BLE et ses protocoles
---

# Présentation du Bluetooh Low Energy (BLE)

Cette section est essentiellement une traduction, avec quelques adaptations, des [présentations du BLE par la société Adafruit](https://learn.adafruit.com/introduction-to-bluetooth-low-energy) et par [Digikey](https://www.digikey.fr/fr/articles/comparing-low-power-wireless-technologies). Pour une explication technique plus complète, nous conseillons le livre _Getting Started with Bluetooth Low Energy_ de Kevin Townsend, Carles Cufi Akiba et Robert Davidson, éditions O′Reilly (13 mai 2014), ISBN-10 : 1491949511.

Les termes anglo-saxons (advertising, peripheral, central ...) sont volontairement conservés car leur traduction systématique rendrait les explications techniques inutilement confuses. 


## Origines et objectifs du BLE

Le Bluetooth Low Energy ou « Bluetooth Smart » a vu le jour en 2006 sous le nom de projet Wibree, réalisé dans le Centre de recherche de Nokia. La technologie a été adoptée par le Bluetooth SIG (Special Interest Group), qui l'a présentée comme une forme de Bluetooth à consommation énergétique ultrabasse lors de l'introduction de sa version 4.0 en 2010.

La technologie fonctionne dans la bande ISM (**I**ndustrielle, **S**cientifique et **M**édicale) de 2,4 GHz et est adaptée pour la transmission de données à partir de capteurs sans fil compacts ou d'autres périphériques prenant en charge une communication entièrement asynchrone. Ces dispositifs transmettent de faibles volumes de données (c'est-à-dire quelques octets) par intermittence. Leur rapport cyclique s'étend de quelques fois par seconde à une fois par minute, ou plus.

Le BLE permet des économies d'énergie en optimisant le temps de veille et en utilisant des connexions rapides et une faible puissance d'émission/réception de crête. Sa faible consommation énergétique réside principalement dans le fait que, contrairement au Bluetooth classique qui est une radio à intervalles de connexion fixes, le BLE présente généralement un état « déconnecté » écoénergétique, dans lequel chacune des deux extrémités d'une liaison est consciente de l'autre, mais ne se connecte qu'en cas de nécessité, et ce aussi brièvement que possible. En pratique la consommation du BLE est **entre deux fois et dix fois plus faible que celle du Bluetooth** lorsqu'on compare des fonctionnalités qui peuvent être implémentées avec ces deux protocoles. 

Pour résumer, BLE répond au cahier des charges suivant :
- Fonctionne avec la bande ISM 2.4 GHz 
- Faible complexité donc **faible coût**
- Faible bande passante (débit brut maximum inférieur à 2 Mbps en version 5.x)
- Faible consommation (courants de quelques microampères, alimentation sur pile bouton possible)
- Faible portée maximum (de 20 à 50 mètres selon l'environnement)

Le BLE ne couvre pas les mêmes usages que le Bluetooth (et à fortiori encore moins ceux du Wi-Fi !), mais il y a souvent confusion entre les deux technologies du fait que les composants de nos smartphones sont généralement **dual mode** et contiennent à la fois une radio BLE et une radio Bluetooth en permettant une interopérabilité entre les deux protocoles. 

Le SoC STM32WB55 qui anime la NUCLEO-WB55 est destiné aux objets connectés et implémente de ce fait les normes Bluetooth LE 5.2 et IEEE 802.15.4 mais **il n'est pas dual mode**. Il ne peut donc pas "discuter" avec des modules Bluetooth standard (comme par exemple les HC-05 et HC-06). 

Nous allons à présent aborder les **protocoles GAP et GATT** qui serviront de fondements à nos scripts MicroPython mettant en œuvre le BLE.

## Le Generic Access Profile (GAP)

_Ce protocole permet aux dispositifs BLE (on abrègera par le terme "objets" par la suite) de signaler leur présence ("advertising" ou "notifications" en français) et réglemente comment deux d'entre eux pourront interagir et éventuellement se connecter l'un à l'autre._

GAP attribue des **rôles** aux objets :

- **Peripheral (esclave)** : Objet disposant de peu de ressources (ex. capteur connecté) qui renverra des informations à un central. 
- **Central (maître)** : Objet disposant de ressources plus étendues (ex: smartphone) auquel se connectent les périphériques.
- **Advertiser/broadcaster** : L’objet ne fait qu’émettre des données (31 octets de données utiles au maximum) sans se connecter à un autre objet.
- **Scanner/observer** : L’objet ne fait que recevoir des données, sans se connecter à un autre objet. Il est à l'écoute des messages d'éventuels objets qui tiendraient un rôle d'advertiser.

Les objets en mode advertiser peuvent donc partager une information avec tous les objets en mode scanner. Le graphique suivant montre un 
scanner/observer entouré d'advertisers/broadcasters :

<br>
<div align="center">
<img alt="BLE GAP, network topology" src="images/BLE_1.jpg" width="600px">
</div>
<br>

L'objet en mode scanner reste *passif*, il écoute simplement les émissions à intervalle régulier (*advertising interval*) de l'objet en mode advertiser. Ces émissions, les *advertising data*, contiennent jusqu'à 31 octets de données utiles. Le graphique suivant illustre ce processus : 

<br>
<div align="center">
<img alt="BLE GAP, advertising" src="images/BLE_2.jpg" width="800px">
</div>
<br>

En fait, GAP permet des échanges un peu plus riches, illustrés par le graphique suivant :

<br>
<div align="center">
<img alt="BLE GAP, advertising plus response" src="images/BLE_3.jpg" width="800px">
</div>
<br>

L'objet en mode scanner est, dans ce deuxième cas, *actif*. Il envoie une *scan request* à l'objet en mode advertiser qui lui répondra avec un nouveau message *scan reponse data* contenant 31 octets d'informations différentes de celles contenues dans les messages *advertising data*.

Le processus d'advertising est très utile pour des applications où les objets BLE servent de balises, par exemple pour la géolocalisation ou la recherche d'objets en intérieur (voir [IBeacon de Apple](https://fr.wikipedia.org/wiki/IBeacon)), la publicité dans les magasins, les visites interactives dans les musées, etc. La balise se limite à envoyer à tous les objets du voisinage des messages du genre, *"Salut ! Je suis une balise ! Voici mon nom et je pense que vous devriez visiter cette URL !"*. Aucune connexion ou appairage avec un smartphone ou autre objet ne sont nécessaires.

Une fois qu'une connexion est établie, l’advertising prend généralement fin et le protocole GATT entre en action. Les échanges sont alors limités entre le Périphérique et l’unique Central auquel il est connecté.

## Le Generic Attributes Profile (GATT)

_Ce protocole définit comment deux objets échangent des données à l’aide de services et de caractéristiques. GATT entre en jeu après GAP, une fois que deux objets sont connectés._ 

L'un des objets, en général qui était un *advertiser/broadcaster* avant connexion, adoptera le rôle de *peripheral* et se comportera comme un *serveur* (de caractéristiques). L'autre objet, qui était un *scanner/observer* avant connexion, deviendra un *central*, et se comportera comme un *client* (des caractéristiques mises à sa disposition par le périphérique). Par exemple, lorsque vous connectez un bracelet cardiofréquencemètre à votre smartphone via BLE, le bracelet devient un périphérique serveur de données cardiaques et le smartphone qui les enregistre et les affiche devient un central, client du bracelet.

Un aspect important à conserver à l'esprit concernant GATT, c'est que *les connexions sont exclusives* : un périphérique BLE ne peut pas être connecté à plus d'un central à un moment donné. A l'inverse, un central peut être connecté à plusieurs périphériques simultanément, comme le résume la figure suivante :

<br>
<div align="center">
<img alt="BLE GATT, network topology" src="images/BLE_4.jpg" width="500px">
</div>
<br>

Si des données doivent être échangées entre deux périphériques, il sera donc nécessaire de passer par le central qui servira  de "boîte à lettres".

On dit que l'objet peripheral est un *esclave* du central, qui sera donc son *maître*. A l'établissement de la connexion, le peripheral propose au central un *intervalle de connexion* pour cadencer leurs échanges, mais c'est bien le central qui décidera in-fine de la fréquence de ses échanges futurs avec les différents peripherals auxquels il est connecté. Le graphique suivant illustre le processus des échanges de données entre un objet peripheral (le serveur GATT) et un objet central (le client GATT), qui initie chaque transaction :

<br>
<div align="center">
<img alt="BLE GATT, master-slave exchanges" src="images/BLE_5.jpg" width="800px">
</div>
<br>

Le protocole GATT repose sur un autre protocole d'échange de données bidirectionnel et *structuré* nommé *Attribute Protocol* (ATT) qui met en œuvre les concepts de *Profils*, de *Services* et de *Caractéristiques*, que l'on peut imaginer comme des conteneurs imbriqués selon la figure suivante :

<br>
<div align="center">
<img alt="BLE GATT, profiles, services, caracteristics" src="images/BLE_6.jpg" width="250px">
</div>
<br>

- **Les profils** n'existent pas sur les périphériques, ce sont simplement des collections prédéfinies de services qui ont été compilées soit par le Bluetooth SIG, soit par le concepteur du périphérique.

    Le profil "Heart Rate", par exemple, rassemble le service "Heart Rate" et le service "Device Information". La liste complète des profils GATT officiellement adoptée est disponible [ici](https://www.bluetooth.com/specifications/gatt).

- **Les services** segmentent les données en unités logiques appelées *caractéristiques*. Un service peut contenir une ou plusieurs caractéristiques et chaque service est identifié de façon unique par un entier appelé **UUID** pour "*Universally Unique IDentifier*". L'UUID est codé sur 16 bits pour les services BLE officiels et sur 128 bits pour les services BLE personnalisés. Une liste complète des services BLE officiels peut être consultée [ici](https://www.bluetooth.com/specifications/gatt/services).
    
    Par exemple, le [service "Heart Rate"](https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.service.heart_rate.xml) a comme UUID la valeur 0x180D et contient jusqu'à trois caractéristiques, seule la première étant obligatoire : *Heart Rate Measurement*, *Body Sensor Location* et *Heart Rate Control Point*.

- **Les caractéristiques** sont les éléments de plus bas niveau pour les transactions GATT, elles encapsulent un unique point de mesure (une valeur pour un capteur de température ou bien un triplet (Ax, Ay, Az) pour un accéléromètre trois axes, par exemple). Les caractéristiques disposent toutes, à l'image des services, d'un UUID codé sur 16 ou 128 bits, et vous êtes libre soit d'utiliser [les caractéristiques standard définies par le Bluetooth SIG](https://www.bluetooth.com/specifications/gatt/characteristics), ce qui facilitera une compatibilité entre plusieurs dispositifs BLE, soit de définir vos propres caractéristiques pour un écosystème propriétaire (comme par exemple [Blue-ST](https://www.st.com/resource/en/user_manual/dm00550659-getting-started-with-the-bluest-protocol-and-sdk-stmicroelectronics.pdf) de STMicroelectronics).

    Par exemple, la caractéristique *Heart Rate Measurement* est obligatoire pour le service *Heart Rate*, et utilise un UUID égal à 0x2A37. La trame correspondante commence avec 8 bits qui décrivent le format des mesures cardiaques (entier non signé codé sur 8 bits, sur 16 bits ...) et se termine avec les octets encodant les valeurs mesurées.
    
    Les caractéristiques sont le vecteur d'échange de données en BLE, un central pourra *lire* une caractéristique pour prendre connaissance des mesures d'un périphérique. Mais il pourra aussi *écrire* dans une caractéristique exposée par l'un de ses périphériques pour lui envoyer des informations en retour. C'est par exemple comme cela qu'est implémenté le service UART, avec une caractéristique RX en lecture seule pour réaliser un canal de réception des données et une caractéristique TX en écriture pour réaliser un canal de réponse.

## Pour plus d'informations, les ressources du Bluetooth SIG 

- La [spécification Bluetooth](https://www.bluetooth.com/specifications/bluetooth-core-specification)
- Le [portail des développeurs Bluetooth](https://www.bluetooth.com/develop-with-bluetooth)
- La [liste officielle des profils et services BLE](https://www.bluetooth.com/specifications/gatt)
- La [liste officielle des caractéristiques BLE](https://www.bluetooth.com/specifications/gatt/characteristics)
