---
title: Les technologies de l'embarqué
description: Introduction aux technologies des systèmes embarqués
---

<br>
<div align="center">
<img alt="En chantier" src="images/en_chantier.jpg" width="400px">
</div>
<br>


# Les technologies de l'embarqué

Le [glossaire](../Kit/glossaire) apporte des définitions concises des acronymes les plus souvent rencontrés dans les documentations techniques concernant les microcontrôleurs ainsi que les systèmes embarqués et les systèmes connectés qu'ils animent.


## Sommaire

1. []()
2. []()
3. [Le Bluetooth Low Energy (BLE)](ble)
4. [Glossaire](../Kit/glossaire)

