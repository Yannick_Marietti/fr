---
title: Le Kit pédagogique NUCLEO-WB55
description: Description du kit pédagogique STM32python basé sur la NUCLEO-WB55
categories: stm32 kit nucleo python
---

## Le Kit pédagogique NUCLEO-WB55

Les cartes NUCLEO et Discovery STM32 de STMicroelectronics peuvent être programmées en C/C++ depuis
* L'environnement [STM32duino (Arduino IDE)](https://GitHub.com/stm32duino/wiki/wiki/Getting-Started)
* L'environnement [STM32Cube](https://www.st.com/stm32cube)
* L'environnement en ligne [MBed OS](https://os.mbed.com/platforms/ST-Nucleo-F446RE/)
* L'environnement [RIOT OS](https://GitHub.com/RIOT-OS/RIOT/tree/master/boards/nucleo-f446re)

**Mais ce n'est pas tout !**
Un nombre croissant cartes de STMicroelectronics peuvent également être programmées en [MicroPython](https://MicroPython.org/stm32/).

L'objectif prioritaire de ce site est d'apporter des ressources pour programmer les cartes NUCLEO et Discovery en MicroPython sans pour autant se priver de l'opportunité de fournir du contenu pour STM32duino, Mbed...<br>
Dans cette optique, le kit pédagogique que nous conseillons contient le matériel suivant :

* [NUCLEO-WB55](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html) - Carte contenant un SoC[^1] Bluetooth™ 5 STM32WB55 de STMicroelectronics (en vente chez [Vittascience](https://fr.vittascience.com/shop/263/NUCLEO-WB55RG) ou chez [Farnell](https://fr.farnell.com/stmicroelectronics/p-nucleo-wb55/carte-iot-solut-mcu-arm-cortex/dp/2989052)).
* [X-NUCLEO-IKS01A3 - Carte d'extension avec capteurs mouvement et environnent MEMS pour STM32 NUCLEO, avec connecteurs Arduino UNO Layout R3](https://www.st.com/en/ecosystems/x-nucleo-iks01a3.html) (en vente chez [Vittascience](https://fr.vittascience.com/shop/309/NUCLEO-IKS01A3) ou chez [Farnell](https://fr.farnell.com/stmicroelectronics/x-nucleo-iks01a3/carte-d-extension-stm32-nucleo/dp/3106035)).
* [Grove Base Shield V2 - Carte fille de connection](https://wiki.seeedstudio.com/Base_Shield_V2/) et les différents modules fournis dans le [Grove starter kit](http://wiki.seeedstudio.com/Grove_System/#grove-starter-kit) en vente chez [Vittascience]([Vittascience](https://fr.vittascience.com/shop/109/Shield-Arduino-Grove-) ou chez [Farnell](https://fr.farnell.com/seeed-studio/83-16991/grove-starter-kit-for-arduino/dp/2801858?st=Grove).

Le Grove Base Shield V2 est recommandé car *il fiabilise et simplifie grandement les connexions via la connectique propriétaire Grove de Seeed Studio*. Si vous ne souhaitez pas l'utiliser, il sera bien sûr tout à fait possible de connecter les différents modules et capteurs directement aux connecteurs Arduino de la NUCLEO-WB55 en utilisant des *câbles dupont* et des *platines d'essais*.

Veuillez noter que le Grove Base Shield V2 comporte un *connecteur ICSP* (In Circuit Serial Programming),une interface de programmation pour les microcontrôleurs AVR de la société ATMEL utilisant le protocole SPI. **Dans le cadre de notre utilisation, ce connecteur ne sera JAMAIS utile** ; il pourra même compromettre l'insertion d'autres cartes filles. Donc, si vous disposez d'un Grove Base Shield V2 et que vous ne souhaitez pas l'utiliser par la suite avec des cartes Arduino, **nous vous conseillons de dé-souder et retirer ce connecteur ICSP**.<br>

[^1]: Abbréviation de "System on Chip", littéralement "Système sur Puce". Désigne un système complet embarqué sur une seule puce (« circuit intégré ») et pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d’interface ou tout autre composant nécessaire à la réalisation d’une fonction requise. Les microcontrôleurs sont des SoC.

|P-NUCLEO-WB55 kit|X-NUCLEO IKS01A3|Grove starter kit (avec Grove Base Shield V2)|
|:-:|:-:|:-:|
|<img alt="p-nucleo-wb55" src="images/p-nucleo-wb55.jpg" width="410px">|<img alt="x-nucleo-iks01a3" src="images/x-nucleo-iks01a3.jpg" width="400px">|<img alt="grove_starter_kit" src="images/grove_starter_kit.jpg" width="410px">|


> Crédits des images : [STMicroelectronics](https://www.st.com) et [Seeed Studio](http://wiki.seeedstudio.com/Grove_System)

## Où acheter les cartes NUCLEO-WB55 et autres composants utilisés sur ce site ?

Sous réserve que les références utilisées sur ce site 

- Les cartes STM32 Nucleo peuvent être achetées en ligne ou par bon de commande auprès des fournisseurs de composants électroniques comme [Vittascience](https://fr.vittascience.com/), [Farnell](https://fr.farnell.com), [RS Online](https://fr.rs-online.com/web/), [Conrad](https://www.conrad.fr) ou bien [Mouser](https://www.mouser.fr/).

 - La plupart des modules du système [Grove de Seeed Studio](http://wiki.seeedstudio.com/Grove_System) et les platines I2C [Qwiic de Sparkfun](https://www.sparkfun.com/qwiic) peuvent être achetées en ligne ou par bon de commande auprès des fournisseurs de composants électroniques comme [Vittascience](https://fr.vittascience.com/), [Lextronic](https://www.gotronic.fr/), [GoTronic](https://www.gotronic.fr/) et [Conrad](https://www.conrad.fr) distribuent. _Pensez à commander suffisament de câbles de raccordement_.

## L'environnement et les kits STM32 de Vittascience

La société [**Vittascience**](https://fr.vittascience.com/) propose sur son catalogue, depuis septembre 2020, **quatre kits pédagogiques** à l’attention des collégiens et lycéens, développés en partenariat avec la société STMicroelectronics et basés sur MicroPython :

-	Le [**Starter kit - version Nucleo L476**](https://fr.vittascience.com/shop/product.php?id=310)
-	La [**Station de mesures connectée - version Nucleo L476**]( https://fr.vittascience.com/shop/product.php?id=311)
-	La [**Plante connectée - version Nucleo WB55RG**]( https://fr.vittascience.com/shop/product.php?id=274)
-	Le [**Robot martien - version Nucleo WB55RG**]( https://fr.vittascience.com/shop/product.php?id=275)

Ces kits sont supportés par [**la plateforme de développement de Vittascience**]( https://fr.vittascience.com/code).

Exécutée dans un navigateur, en ligne comme hors ligne, l’IDE de Vittascience permet de « construire » avec des blocs (« façon scratch ») le firmware du microcontrôleur STM32 qui anime le kit.
Plus précisément, le schéma de blocs construit par l’élève dans l’IDE est d’abord traduit par celle-ci en langage Python 4 avant d’être exécuté par l’interpréteur MicroPython installé préalablement sur le microcontrôleur STM32.
Entre autres fonctions très utiles, cette IDE offre aussi *un simulateur*, *un débogueur pas à pas* et *un éditeur de code source MicroPython*.


## Autres cartes et kits possibles

Bien évidemment, d'autres kits sont envisageables pour des tutoriels : [voir cette liste](Kits).
