---
title: Reprogrammer la pile BLE d'un STM32WB sur une carte NUCLEO avec STM32CubeProgrammer
description: Ce tutoriel explique comment reprogrammer la pile BLE d'un STM32WB sur une carte NUCLEO avec STM32CubeProgrammer
---

# Reprogrammer la pile BLE d'un SoC STM32WB sur une carte NUCLEO-WB avec STM32CubeProgrammer

Ce tutoriel explique pas à pas comment reprogrammer le firmware qui gère le Bluetooth Low Energy (BLE) dans [le SoC STM32WB55RG](https://www.st.com/en/microcontrollers-microprocessors/stm32wb-series.html) d'une carte [NUCLEO-WB55RG](https://www.st.com/en/evaluation-tools/nucleo-wb55rg.html). Pour simplifier par la suite on dira que l'on explique comment programmer la "pile BLE HCI".

Nous traitons le cas particulier du chargement d'un firmware qui implémente les commandes Host Controller Interface (HCI) car celles-ci sont indispensables à la mise en œuvre de la communication BLE avec la carte NUCLEO-WB55RG dans les environnements de programmation MicroPython et Arduino qui nous intéressent tout particulièrement.

**Or, depuis quelques temps, les cartes NUCLEO-WB55RG ne sont plus programmées en usine avec la pile BLE HCI !**<br>

Cette modification pose donc **UN PROBLEME MAJEUR** à toute l'activité MicroPython que nous proposons sur ce site, car elle est presque entièrement construite sur la carte NUCLEO-WB55. Aussi, si vous essayez de mettre en œuvre un script parmi ceux de la [section BLE](../Micropython/BLE/index) et que vous obtenez dans le terminal PuTTY le message d'erreur suivant par le REPL du firmware MicroPython ...

```console
tl_ble_wait_resp: timeout.
```
... alors **il est INDISPENSABLE que vous suiviez le présent tutoriel pour corriger le problème**. A l'inverse, si ça marche, respectez la règle d'or en ce qui concerne les technologies du numérique : **NE TOUCHEZ A RIEN !**

## **Configuration de la carte NUCLEO-WB55**

La figure ci-dessous rappelle comment configurer la carte NUCLEO-WB55 avant de lancer STM32CubeProgrammer :

<br>
<div align="center">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

Cette carte possède deux ports USB, celui que nous utiliserons est l'USB ST-LINK, à droite lorsque la carte est vue du dessus. Prenez soin également de positionner le cavalier d'alimentation (celui signalé en rouge) sur la dernière position à droite, comme indiqué sur la figure.

## **Présentation et téléchargements des firmwares pour la FUS et la pile BLE HCI**

Dans un premier temps, nous examinons le dépôt GitHub qui explique la procédure de mise à jour que nous allons suivre et qui met à disposition les différents firmwares ainsi que les adresses auxquelles ils devront être programmés dans le SoC STM32WB55RG. Le site se trouve [**à cette adresse**](https://github.com/STMicroelectronics/STM32CubeWB/blob/master/Projects/STM32WB_Copro_Wireless_Binaries/STM32WB5x/) : 

<br>
<div align="center">
<img alt="GitHub WB5x 1" src="images/github_wb55_1.jpg" width="900px">
</div>
<br>

Quatre fichiers nous intéressent tout particulièrement dans la liste exposée par cette page :
1. *Release_Notes.html* qui explique la procédure que nous allons suivre, nous y reviendrons.
2. *stm32wb5x_BLE_HCILayer_fw.bin* qui contient le microcode de la pile BLE HCI. **Téléchargez le sur votre PC / MAC**.
3. *stm32wb5x_FUS_fw_for_fus_0_5_3.bin* qui contient une version du microcode des FUS.  **Téléchargez le sur votre PC / MAC**.
4. *stm32wb5x_FUS_fw.bin*  qui contient une autre version du microcode des FUS.  **Téléchargez le sur votre PC / MAC**.

Pour visualiser correctement le fichier *Release_Notes.html*, tapez cette URL dans la barre d'adresse de votre navigateur :

>> https://htmlpreview.github.io/?https://github.com/STMicroelectronics/STM32CubeWB/blob/master/Projects/STM32WB_Copro_Wireless_Binaries/STM32WB5x/Release_Notes.html

Le contenu de *Release_Notes.html* devrait s'afficher, comme ceci (aux variations d'aspect près, selon votre navigateur Internet) :

<br>
<div align="center">
<img alt="GitHub WB5x 2" src="images/github_wb55_2.jpg" width="900px">
</div>
<br>

Dans cette page, vous trouverez la version complète de la procédure que nous expliquons ici ainsi que ce tableau qui contient des informations qui seront requises par STM32CubeProgrammer :

<br>
<div align="center">
<img alt="GitHub WB5x 3" src="images/github_wb55_3.jpg" width="900px">
</div>
<br>

Cette copie d'écran montre (entre autres) les deux premières colonnes qui nous intéressent : 
1. *Wireless Coprocessor Binary* qui donne le nom du fichier contenant le firmware.
2. *STM32WB5xxG(1M)* qui contient l'adresse dans la mémoire flash du STM32WB55RG à laquelle le firmware en question doit commencer à être écrit. Les colonnes suivantes sur la droite donnent la même informations pour les autres SoC STM32 de la famille STM32WB (elles ne nous seront pas utiles).

Les informations qui nous seront utiles, extraites de ce tableau, sont les suivantes :

|**Désignation**|**Fichier du firmware**|**Adresse de début en flash**|
|:-|:-|:-:|
|FUS (cas 1)|stm32wb5x_FUS_fw_for_fus_0_5_3.bin|0x080**EC**000|
|FUS (cas 2)|stm32wb5x_FUS_fw.bin|0x080**EC**000|
|Pile BLE HCI|stm32wb5x_BLE_HCILayer_fw.bin|0x080**E0**000|

**Nous pouvons à présent procéder à la mise à la reprogrammation des FUS et de la pile BLE HCI**.

## **Préalables importants**

1. **Installation de STM32CubeProgrammer**<br>
Nous ne détaillerons pas l'installation de [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), qui peut être téléchargé gratuitement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software) ; la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

2. **La séquence des opérations est imposée !**<br>
Le firmware exécutant la pile BLE doit être installé en trois étapes, détaillées ci-après :

    **Etape 1** : Mettre à jour le microcode des [firmware upgrade services (FUS)](https://www.st.com/resource/en/application_note/dm00513965-st-firmware-upgrade-services-for-stm32wb-series-stmicroelectronics.pdf).<br>
    **Etape 2** : Mettre à jour le firmware de la pile BLE HCI.<br>
    **Etape 3** : Valider l'option byte nSWBOOT0.<br>
     
    **Nous vous conseillons vivement réaliser systématiquement ces trois étapes dans cet ordre** même si la mise à jour du firmware pour les FUS paraît inutile (lorsque sa dernière version est déjà installée sur le STM32WB55). Dans le cas contraire, à moins d'être un expert de la CLI de STM32CubeProgrammer, vous n'y arriverez pas !

## **Etape 1 : Programmation du firmware des FUS**

**(1)** Une fois la carte connectée par son ST-Link, lancez STM32CubeProgrammer, cliquez sur le bouton "Connect" en haut à droite. **Sélectionnez ensuite l'onglet *Firmware Upgrade Services* dans le menu vertical à gauche**. Vous devriez obtenir ceci :

<br>
<div align="center">
<img alt="FUS upgrade 1" src="images/fus_1.jpg" width="900px">
</div>
<br>

**(2)** Dans la zone *WB Commands* au milieu, en bas, **cliquez sur le bouton *Start FUS***. Une fenêtre popup confirme alors que le FUS est activé par le message *StartFus activated successfully* (copie d'écran ci-dessous). Cliquez sur *OK* pour fermer la  fenêtre popup.<br>
Il est possible que l'application indique que cette opération a échoué via une autre fenêtre popup. Le cas échéant, cliquez sur le bouton *Start FUS* jusqu'à ce que cette étape soit validée.

<br>
<div align="center">
<img alt="FUS upgrade 2" src="images/fus_2.jpg" width="900px">
</div>
<br>

**(3)** Dans la zone *FUS information* au milieu, en haut, **cliquez sur le bouton *Read FUS infos***. La zone de texte *FUS Version* indique à présent **la version des FUS actuellement installée sur votre carte** (dans notre exemple, la v1.2.0.0). 
- Si cette version est la 0.5.3, alors, le firmware pour le point (4) devra être *tm32wb5x_FUS_fw_for_fus_0_5_3.bin*.
- S'il s'agit d'une autre version, alors le firmware pour le point (4) devra être *stm32wb5x_FUS_fw.bin*.

<br>
<div align="center">
<img alt="FUS upgrade 3" src="images/fus_3.jpg" width="900px">
</div>
<br>

**(4)** Dans la zone *Firmware Upgrade* en haut à droite, renseignez les informations que vous avez déterminées au point (3) concernant le firmware FUS que vous allez charger sur la carte :
- Dans la zone *File path*, le chemin du firmware sélectionné sur votre ordinateur.
- Dans la zone *Start address*, l'adresse donnée pour le firmware sélectionné suivant ce tableau :

  |**Fichier du firmware**|**Adresse de début en flash**|
  |:-|:-:|
  |stm32wb5x_FUS_fw_for_fus_0_5_3.bin|0x080**EC**000|
  |stm32wb5x_FUS_fw.bin|0x080**EC**000|
  
Pour notre cas, cela donne :

<br>
<div align="center">
<img alt="FUS upgrade 4" src="images/fus_4.jpg" width="900px">
</div>
<br>

**(5)** Cliquez ensuite sur le bouton *Firmware Upgrade*, puis validez par *OK* les deux fenêtres popup qui s'affichent consécutivement, une vous indiquant que le précédant firmware BLE vient d'être effacé et l'autre que la mise à jour des FUS est terminée.<br>
**Cliquez alors sur le bouton *Read FUS infos***. La zone de texte *FUS Version* indique à présent la version des FUS que vous venez d'installer (dans notre cas, pas de changements on reste sur v1.2.0.0) mais aussi confirme que la pile BLE a été effacée (*STACK Version v0.0.0.0*) : 

<br>
<div align="center">
<img alt="FUS upgrade 5" src="images/fus_5.jpg" width="900px">
</div>
<br>

**Tout est prêt à présent pour mettre à jour la pile BLE HCI.** Ne déconnectez **SURTOUT PAS** votre carte NUCLEO-WB55 de STM32CubeProgrammer et **passez directement à l'étape 2**.

## **Etape 2 : Programmation du firmware de la pile BLE HCI**

**(1)** Dans la zone *Firmware Upgrade* en haut à droite, renseignez les informations concernant le firmware de la pile BLE HCI que vous allez charger sur la carte :
- Dans la zone *File path*, le chemin du firmware sélectionné sur votre ordinateur.
- Dans la zone *Start address*, l'adresse donnée pour le firmware sélectionné suivant ce tableau :

  |**Fichier du firmware**|**Adresse de début en flash**|
  |:-|:-:|
  |stm32wb5x_BLE_HCILayer_fw.bin|0x080**E0**000|

<br>
<div align="center">
<img alt="HCI BLE stack upgrade 1" src="images/hci_1.jpg" width="900px">
</div>
<br>

**(2)** Cliquez ensuite sur le bouton *Firmware Upgrade*, puis validez par *OK* les deux fenêtres popup qui s'affichent consécutivement, une vous indiquant que le précédant firmware BLE vient d'être effacé et l'autre que la mise à jour des FUS est terminée.<br>
**Cliquez alors sur le bouton *Read FUS infos***. La zone de texte *FUS Version* indique à présent la version de la pile BLE a été mise à jour (*STACK Version v1.16.0.4*) : 

<br>
<div align="center">
<img alt="HCI BLE stack upgrade 2" src="images/hci_2.jpg" width="900px">
</div>
<br>

**Tout est prêt à présent pour valider l'option byte nSWBOOT0.** Ne déconnectez **SURTOUT PAS** votre carte NUCLEO-WB55 de STM32CubeProgrammer et **passez directement à l'étape 2**.

## **Etape 2 : Valider l'option byte nSWBOOT0**

**(1)** Dans la barre d'icônes verticale, à gauche, sélectionnez *Options bytes* :

<br>
<div align="center">
<img alt="Option bytes 1" src="images/option_bytes_1.jpg" width="900px">
</div>
<br>

**(2)** Dans le bandeau *User Configuration* cochez la case *nSWBOOT0* :

<br>
<div align="center">
<img alt="Option bytes 2" src="images/option_bytes_2.jpg" width="900px">
</div>
<br>

Cliquez sur le bouton *Apply* et attendez la fenêtre popup de confirmation. Fermez celle-ci en cliquant sur son bouton *OK*.

**C'est terminé, la pile BLE HCI de votre carte est à jour**. Vous pouvez à présent cliquer sur le bouton *Disconnect* de STM32CubeProgrammer, déconnecter physiquement votre NUCLEO-WB55 du câble USB pour forcer un "reset matériel**.

Si vous souhaitez ensuite procéder à une reprogrammation du firmware "utilisateur" du STM32WB55RG, par exemple [selon la procédure indiquée ici s'il s'agit de MicroPython](../Micropython/install_win/index), **nous vous conseillons de préalablement d'effacer complètement la mémoire flash du MCU [selon ce tutoriel](cube_prog_erase_flash)** (ceci n'effacera pas les firmware FUS et BLE HCI, bien sûr !).
