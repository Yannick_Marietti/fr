---
title: Les microcontrôleurs STM32
description: Présentation de la famille de microcontrôleurs STM32 de STMicroelectronics
---

# Les microcontrôleurs STM32


## MCU STM32 et CPU ARM Cortex

## Les interruptions et le contrôleur d’interruptions

## L’horloge temps réel

## Le chien de garde indépendant

## La mémoire flash embarquée

## Les contrôleurs de bus

## Les modes basse-consommation

## Les chaînes de compilation pour STM32

## Les envrionnement de développement pour STM32