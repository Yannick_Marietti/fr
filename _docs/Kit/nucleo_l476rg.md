---
title: La carte NUCLEO-L476RG
description: Revue de la carte NUCLEO-L476RG
---

# La carte NUCLEO-L476RG

## Anatomie de la NUCLEO-L476RG

Une partie de nos tutoriels utilisent la **carte NUCLEO-L476RG de STMicroelectronics**. Vous trouverez sa description détaillée et sa fiche technique [ici](https://www.st.com/en/evaluation-tools/nucleo-l476rg.htm). Elle est équipée d’un **microcontrôleur STM32L476RG** cadencé à 80 MHz offrant **1 Mb de mémoire Flash**, 128 kb de SRAM et **un grand nombre d’entrées-sorties**. La figure ci-dessous illustre cette carte et sa légende précise un certain nombre de termes techniques que vous rencontrerez souvent par la suite :

<br>
<div align="center">
<img alt="Cartes NUCLEO 64" src="images/nucleo_l476rg.jpg" width="700px">
</div>
<br>

Comme sur toutes les cartes NUCLEO, le port USB **ST-LINK** sert à la programmer et offre également un canal de communication série avec l'ordinateur auquel elle est connectée. On remarquera que pour assurer sa mission de "programmeur de firmware", le ST-LINK est animé par un microcontrôleur STM32L0 (qui a son propre firmware).<br>
Le cristal de quartz a ici la même utilité que dans les montres ; il s’agit d’un oscillateur piézoélectrique très stable qui fournit une « source d’horloge » de qualité pour le microcontrôleur. En l’absence de quartz, on peut utiliser comme références de fréquence des oscillateurs (i.e. des circuits RLC) intégrés dans le STM32L476RG.

## Les connecteurs de la carte NUCLEO-L476RG

Le microcontrôleur STM32L476RG propose un grand nombre de fonctions et 64 broches sur son boitier, qui ne sont donc pas toutes accessibles par les 32 broches des connecteurs Arduino.Les broches des connecteurs Morpho corrigent partiellement ce problème en offrant 32 connexions supplémentaires : 

<br>
<div align="center">
<img alt="Connecteurs cartes NUCLEO 64" src="images/connecteurs_l476rg.jpg" width="700px">
</div>
<br>

Deux précisions s’imposent pour lire correctement les figures qui précèdent :

 1. **Les broches Arduino dupliquent certaines des broches Morpho**. Par exemple, la broche Arduino D1 (broche numéro 2 du connecteur CN9) est connectée à la broche Morpho PA2 (broche 35 du connecteur CN10). De même, la broche Arduino D3, quatrième du connecteur CN9, offre une fonction PWM et est connectée à la broche Morpho PB3 qui expose donc la même PWM.

 2.	**Toutes les broches Morpho ne sont** (évidemment) **pas connectées à des broches Arduino**. C’est notamment le cas pour celles de la colonne de droite pour CN10 et de la colonne de gauche pour CN7. Par exemple, la broche Morpho AGND (broche 32 du connecteur CN10) n’est reliée à aucune broche Arduino. De même pour les broches PD1, PD0, VDD … de CN7. Par exemple, la broche Morpho AGND (broche 32 du connecteur CN10) n’est reliée à aucune broche Arduino. De même pour les broches PD1, PD0, VDD … de CN7.


## Fonctions alternatives (ou secondaires) de la NUCLEO-L476RG

La liste exhaustive des fonctions du microcontrôleur STM32L476RG qui sont câblées sur les broches Morpho de la carte NUCLEO-L476RG est conséquente. Pour ne citer que quelques-une de ces fonctions :
 
- **Entrée analogique (ADC)**. Une entrée analogique permet de convertir un signal extérieur, une tension, en un nombre entier qui lui est proportionnel. L'ADC accepte en entrée des tensions dont les valeurs sont comprises entre 0 et 3,3V et les convertit en nombres entiers proportionnels, compris entre 0 et 2<sup>12</sup>-1 (soit 4095). L'application la plus courante de l'ADC reste la lecture d'un signal fourni par un capteur analogique, par exemple [**une thermistance**](https://stm32python.gitlab.io/fr/docs/MicroPython/grove/thermistance).

- **Bus I2C**. Le bus I2C est un bus série qui permet de connecter une multitude de périphériques (capteurs, actuateurs ...) en utilisant deux fils, une ligne de données (SDA) et un signal d'horloge (SCL) d'une longueur maximum de 1 m. Il n'alimente pas les périphériques en courant / tension, une ligne V<sub>cc</sub> (source de tension) et une ligne GND (masse) sont donc également nécessaires pour chacun d'eux.<br>
Le bus I2C n'est pas approprié pour la manipulation de données en haut débit, par exemple l'écriture dans des mémoires ou la gestion d'afficheurs au-delà d'une certaine résolution. Pour ces applications, on lui préfèrera le bus SPI.<br>
De nombreux périphériques I2C sont disponibles, voir par exemple [**cette centrale à inertie**](https://stm32python.gitlab.io/fr/docs/MicroPython/grove/LSM303D).

- **Bus SPI**. Le bus SPI est un bus série comme le bus I2C, mais d'une architecture plus complexe permettant les transferts rapides de gros volumes de données. Chaque périphérique est connecté au bus par 4 lignes : SS (pour *Slave Select*, permettant de l'activer sur le bus), SCLK (signal d’horloge pour cadencer les échanges), MOSI (pour *Master Out, Slave In*, communication du microcontrôleur vers le périphérique) et MISO (pour *Master In, Slave Out*,communication du périphérique vers le microcontrôleur). Les lignes d'alimentations (V<sup>cc</sup> et GND) restent bien sûr requises.<br>
Compte tenu du câblage plus complexe (une ligne SS nécessaire par périphérique) et pour ne pas diviser son débit, on ne branche en général qu'un seul périphérique sur par bus / contrôleur SPI, par exemple [**un module carte SD**](https://stm32python.gitlab.io/fr/docs/MicroPython/grove/sd_card_module). 

- **Sortie PWM**. Une sortie PWM (pour *Pulse Width Modulation*, soit *modulation de largeur d’impulsion*) génère un signal en tension carré et périodique dans le temps. La génération de signaux PWM est essentiellement utile pour piloter des moteurs électriques ou des [**servomoteurs**](https://stm32python.gitlab.io/fr/docs/MicroPython/grove/servo).<br>
La PWM n'est qu'une fonction offerte par le microcontrôleur, pas un périphérique intégré dans celui-ci. Les broches qui l'exposent sont en fait pilotées par un périphérique complexe capable de fournir bien d'autres fonctions : *un timer* (voir ci-après). 

- **UART ("Port série")**. L’UART, signifiant *Universal Asynchronous Receiver-Transmitter*, permet l’échange bidirectionnel d’informations  entre le microcontrôleur et un unique module/périphérique externe (liaison point à point). L’échange de données se fait à l’aide de deux fils (RX et TX), après une phase de synchronisation.<br>
L'UART est un protocole limité mais robuste et facile d'utilisation ; il permet d'échanger des commandes et/ou des données en mode texte, lisible par tout un chacun. Il est donc très utilisé pour piloter ou exploiter des périphériques qui renvoient des informations complexes tels que les [**modules GPS**](https://stm32python.gitlab.io/fr/docs/MicroPython/grove/gps) ou encore les [**modules WiFi**]().

- **Les timers et leur canaux**. Les timers figurent parmi les périphériques intégrés les plus complexes du STM32WB55. A la base, un timer n'est rien d'autre qu'un compteur qui revient à zéro après un certain nombre de "tics" à l'image d'un chronomètre. Ils sont utilisés pour un grand nombre d'application telles que : générer des signaux PWM (voir ci-dessus), capturer des signaux qui proviennent de l'extérieur, déclencher des actions à intervalles de temps périodiques, etc.<br>
Les timers sont également connectés à des broches via des canaux d’entrée ou de sortie sur lesquels ils peuvent lire ou générer des signaux modulés en tension (tels que les PWM). Une revue exhaustive des timers du STM32 est hors de propos ici, mais nous les utiliserons dans quelques tutoriels, par exemple [**celui-ci**](https://stm32python.gitlab.io/fr/docs/MicroPython/STARTWB55/blink_many).

Tous ces périphériques internes / intégrés, et bien d'autres, se retrouvent dans l'ensemble des microcontrôleurs "modernes". [Le glossaire](../Kit/glossaire) en cite bien d'autres, et chaque nouvelle génération de microcontrôleur en intègre de nouveaux (par exemple, depuis peu, les NPU pour accélérér les firmwares qui réalisent des fonctions d'intelligence artificielle embarquée).<br>
Apprendre à programmer un microcontrôleur c'est essentiellement apprendre à programmer ses périphériques afin que son microprocesseur puisse communiquer avec les LED, les moteurs, les modules GPS... qui constituent l'application (par exemple, un drone) et les piloter. C'est ce que nous allons faire dans tous les exercices, projets et tutoriels proposés sur ce site.

## Cartographie des fonctions alternatives - broches pour la NUCLEO-L476RG

La présente section donne une représentation graphique des mappages "par défaut" des fonctions alternatives (ou pas) pour la carte NUCLEO-L476RG. Veuillez noter que cette correspondance n’est pas imposée « en dur » : il est possible de rediriger les périphériques internes du microcontrôleur vers d’autres broches de son boitier – et donc de la carte NUCLEO. Cette reconfiguration se fait avec [les bibliothèques LL et HAL](https://community.st.com/s/question/0D50X00009XkhQSSAZ/hal-vs-ll) et est facilitée par l’utilisation de l’outil de configuration graphique [STM32Cube MX](https://www.st.com/en/development-tools/stm32cubemx.html).

### **Broches pour le bouton et la LED utilisateur**

<br>
<div align="center">
<img alt="Broches pour le bouton et la LED utilisateur" src="images/fig_a1_1.jpg" width="650px">
</div>
<br>

### **Broches pour les six contrôleurs de ports séries**

<br>
<div align="center">
<img alt="Broches pour les six contrôleurs de ports séries" src="images/fig_a1_2.jpg" width="650px">
</div>
<br>

### **Broches pour les trois contrôleurs I<sup>2</sup>C**

<br>
<div align="center">
<img alt="Broches pour les trois contrôleurs I2C" src="images/fig_a1_3.jpg" width="650px">
</div>
<br>

### **Broches pour les trois contrôleurs SPI**

<br>
<div align="center">
<img alt="Broches pour les trois contrôleurs SPI" src="images/fig_a1_4.jpg" width="650px">
</div>
<br>

### **Broches pour les vingt-sept canaux PWM**

<br>
<div align="center">
<img alt="Broches pour les vingt-sept canaux PWM" src="images/fig_a1_5.jpg" width="650px">
</div>
<br>

### **Broches pour les douze canaux ADC**

<br>
<div align="center">
<img alt="Broches pour les douze canaux ADC" src="images/fig_a1_6.jpg" width="650px">
</div>
<br>

## Pour ne pas détruire votre microcontrôleur et vos modules ...

Cette section est un résumé des conseils avisés disponibles dans l'excellent ouvrage de [*MicroPython et Pyboard - Python sur microcontrôleur : de la prise en main à l'utilisation avancée*](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906) de Dominique MEURISSE.

- **Il ne faut jamais travailler alors que la carte NUCLEO est sous tension**.<br>

Chaque fois que vous branchez ou débranchez quelque chose sur une broche, assurez vous que la carte n'est pas alimentée. La plupart des modules conçus par STMicroelectronics, Seeed studio, DF Robot, Adafruit … résistent remarquablement bien aux branchements / débranchements brutaux sous tension, mais d’autres (notamment les modules Bluetooth HC-05 et HC-06, très utilisés mais fabriqués on ne sait où par on ne sait qui) n’attendent que ça pour « griller ».

- **Vérifiez toujours deux fois vos branchements avant de remettre la carte NUCLEO sous tension**.<br>

Avec les connecteurs normalisés de type Grove, Qwiic stemma, micro:bit ... le risque de vous tromper sur le câblage au point de détruire un module est proche de zéro, c’est pour cela que nous les utilisons dans la plupart des tutoriels de ce site. Leur autre avantage (pour les débutants seulement) c’est qu’ils permettent de réaliser des systèmes complexes même lorsqu'on n'a aucune connaissance (ou presque) sur le fonctionnement d’un circuit électrique / électronique. Dit autrement : ils permettent de se concentrer sur la programmation embarquée plutôt que sur la conception électronique.<br>

En revanche, si vous utilisez des câbles dupont, des résistances et des platines d’essai pour câbler votre système, vous entrez sur le territoire des électroniciens avisés ; vous êtes supposé avoir une bonne connaissance des courants, des tensions, des composants. Soyez extrêmement vigilant à ne pas mettre en court-circuit deux broches du microcontrôleur, à ne pas envoyer une surtension sur une broche d’entrée, à ne pas brancher dans le mauvais sens un composant polarisé, etc. Pour ne pas se tromper sur les polarités, un respect des codes-couleurs des fils est essentiel (rouge pour l'alimentation positive, noir pour la masse).

- **Concernant les broches du microcontrôleur, une petite liste de points de vigilance sur les fausses manipulations fatales** : 

1. Ne pas appliquer des tensions supérieures à 3,3V sur des broches configurées en "entrée" (on l'a déjà dit).
2. Ne pas appliquer une tension supérieure à 3,3V sur la broche +3V3 (Ne pas lui appliquer sciemment de tension du tout d'ailleurs, puisque c'est une alimentation !).
3. Ne pas appliquer une tension supérieure à 5V sur la broche +5V (Idem ci-dessus).
4. Ne pas brancher une broche configurée en sortie directement à la masse. Placée dans l'état `HAUT` elle y injecterait un courant d'intensité égale au maximum de ce qu'elle peut délivrer  (25 mA) et elle "fondrait" immédiatement.
5. Ne pas solliciter une broche configurée en sortie numérique pour délivrer un courant d'une intensité dépassant les 25 mA qu'elle peut délivrer. Par exemple, une broche peut piloter un relais qui commande un moteur, lui même alimenté par un circuit qui lui est dédié. Mais une broche **ne peut pas** fournir le courant nécessaire au moteur et **ne doit pas** être connectée "en direct" dessus.<br>
Ce point de vigilance reste pertinent aussi lorsqu'on utilise **plusieurs broches du microcontrôleur simultanément comme sources de courant**. Le courant d’alimentation maximum du STM32WB55 est de 150 mA, il ne peut donc pas délivrer plus à un instant donné sur l'ensemble de ses broches sans être irrémédiablement détruit.<br>
En pratique, que vous utilisiez une ou plusieurs broches comme alimentations, tenez vous loin de la limite des 25 mA ; **1 à 2 mA par broche** est le maximum raisonnable. Si votre montage nécessite plus de courant il faut prévoir un circuit d'alimentation indépendant.
6. Ne pas brancher des broches ensembles ; en court-circuit.
7. Ne pas se tromper sur le signe (et la valeur !) de la tension d'alimentation de la carte NUCLEO si on décide de passer par la broche V<sub>IN</sub>. Elle doit être positive, bien sûr ; un microcontrôleur est un composant polarisé qui fonctionne en courant continu.

Tout ceci peut paraître évident, mais on a vite fait de se retrouver dans l'une de ces situations lorsqu'on conçoit un circuit complexe avec une platine d'essai si on est débutant.


