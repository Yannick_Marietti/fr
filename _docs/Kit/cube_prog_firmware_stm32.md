---
title: Reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer
description: Ce tutoriel explique comment reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer
---

# Reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer

Ce tutoriel explique pas à pas comment reprogrammer le firmware du microcontrôleur d'une carte NUCLEO avec STM32CubeProgrammer. Il peut être nécessaire d'effacer un firmware déjà installé sur le MCU, dans ce cas vous pourrez dans un premier temps suivre les instructions de [ce tutoriel](cube_prog_erase_flash).

Bien que tous les exemples que nous donnons sur ce site permettent de reprogrammer facilement les MCU STM32 par des "glisser-déplacer" de firmwares au format ".bin" via le gestionnaire de fichiers de votre PC/MAC, la procédure donnée ici est plus générale et permet de programmer des firmwares dans différents formats tels que ".hex", ".bin", etc.

**Précision concernant les fimwares au format .dfu**. Si vous souhaitez installer un firmware MicroPython au format ".dfu" sur une carte NUCLEO, par exemple tel que celui que l'on peut télécharger [ici pour la NUCLEO-L476RG](https://micropython.org/download/NUCLEO_L476RG/) sur le site officiel de MicroPython, il faudra d'abord le convertir au format ".bin". Pour cela, vous devrez suivre la procédure (pour Windows) indiquée dans la section **Annexe 1 : Installation d'un firmware MicroPython au format ".dfu"** de [cette page](../Micropython/install_win/index).

## **Configuration de la carte NUCLEO**

Pour la plupart des cartes NUCLEO (par exemple la [NUCLEO-L476RG](nucleo_l476rg)) qui ne disposent que d'un seul connecteur USB ST-Link, branchez celui-ci au PC/MAC sur lequel est installé STM32CubeProgrammer.
Si la configuration des cavaliers sur la carte est correcte (normalement il s'agit de la configuration par défaut), la carte et son module ST-Link devraient se mettre sous tension.

<br>
<div align="center">
<img alt="Nucleo_L476RG_Config_STL" src="images/Nucleo_L476_Config_STL.png" width="300px">
</div>
<br>

D'autres cartes NUCLEO sont un peu plus complexes, comme par exemple la [NUCLEO-WB55RG](nucleo_wb55rg) qui dispose de deux connecteurs USB. Dans son cas particulier, il faut configurer la carte comme ceci :

<br>
<div align="center">
<img alt="Nucleo_WB55_Config_STL" src="images/Nucleo_WB55_Config_STL.png" width="300px">
</div>
<br>

Dans tous les cas, en cas de doute, reportez-vous à la fiche technique de la carte pour vous assurer que la carte est configurée en mode ST-Link.

## **Installation de STM32CubeProgrammer**

Nous ne détaillerons pas l'installation de [**STM32CubeProgrammer**](https://www.st.com/en/development-tools/stm32cubeprog.html), qui peut être téléchargé gratuitement [ici](https://www.st.com/en/development-tools/stm32cubeprog.html#get-software) ; la procédure est expliquée sur le site de STMicroelectronics pour les environnements Linux, Windows et Macintosh.

## **Manipulation de STM32CubeProgrammer**

**(1) Lancez STM32CubeProgrammer**, l'interface devrait avoir cet apparence :

<br>
<div align="center">
<img alt="Programmer la flash 1" src="images/program_flash_0.jpg" width="900px">
</div>
<br>

Vous remarquerez que la fenêtre active, *Memory & File editing*, peut être sélectionnée dans la barre d'icônes verticale complètement à gauche, en cliquant sur l'icône en forme de crayon.

**(2) Cliquez  sur le bouton *Connect* en haut à droite**. La fenêtre *Log* (en bas) confirme que la lecture de la mémoire flash du microcontrôleur de la carte NUCLEO s'est déroulée correctement
La fenêtre centrale affiche le contenu de la mémoire flash. L'interface représente la mémoire par groupes de 4 octets codés en hexadécimal. Les adresses sont incrémentées par pas de 4 (chaque octet est adressé dans les architectures ARM).

<br>
<div align="center">
<img alt="Programmer la flash 2" src="images/program_flash_1.jpg" width="900px">
</div>
<br>

**(3) Sélectionnez *Erasing & programming***, dans la barre d'icônes verticale complètement à gauche :

<br>
<div align="center">
<img alt="Programmer la flash 3" src="images/program_flash_2.jpg" width="900px">
</div>
<br>

**(4) Avec le bouton *Browse* renseignez l'emplacement du firmware pour le MCU STM32 dans *File path***. Il est également important de préciser dans *Start address* l'adresse du premier mot dans  la flash à partir duquel le fichier binaire doit être écrit. Par défaut, il s'agit comme sur notre exemple de *0x08000000*, mais pas toujours ! Il est important de vérifier ce point avant de procéder à la programmation d'un firmware.

<br>
<div align="center">
<img alt="Programmer la flash 4" src="images/program_flash_3.jpg" width="900px">
</div>
<br>

**(5) Cliquez sur le bouton *Start Programming*** et attendez que la barre de progression en bas atteigne 100%. Vous devrez valider trois fenêtres popup (compte tenu des options par défaut que nous avons chosies) : *"Download verified successfully", *"Start operation achieved successfully"* et *"File download complete"*.

<br>
<div align="center">
<img alt="Programmer la flash 5" src="images/program_flash_4.jpg" width="900px">
</div>
<br>

**(6) Validez en appuyant sur le bouton *OK* de la fenêtre popup** et déconnectez la carte à l'aide du bouton *Disconnect* en haut à droite. **C'est terminé**, votre firmware est installé sur le microcontrôleur. Vous pouvez fermer STM32CubeProgrammer. Nous vous conseillons de déconnecter votre carte du câble USB puis de la reconnecter pour la forcer à faire un reset matériel avant de tenter de la reprogrammer.
