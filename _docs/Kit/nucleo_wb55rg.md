---
title: La carte NUCLEO-WB55
description: La carte NUCLEO-WB55
---

# La carte NUCLEO-WB55

## Aperçu de la NUCLEO-WB55

La carte NUCLEO-WB55 permet de programmer aisément le microcontrôleur STM32WB55 qui l'équipe en donnant accès à toutes ses fonctions.
Les connecteurs d’extensions (Arduino, Morpho) permettent à l'utilisateur de connecter des composants électroniques externes au STM32WB55.
La carte  dispose aussi d’un connecteur pour pile CR2032, de 3 boutons (SW1, SW2, SW3), de 3 LED (rouge, verte et bleue) et de 2 connecteurs micro-USB.

Le microcontrôleur STM32WB55 rassemble sur la même puce en silicium deux microprocesseurs ARM Cortex ultra - basse consommation et offre une connectivité radio Bluetooth à basse consommation (BLE pour "Bluetooth Low Energy"). Vous trouverez une présentation du BLE [sur cette page](../Embedded/ble).

## Anatomie de la carte NUCLEO-WB55

<br>
<div align="center">
<img alt="NUCLEO-WB55" src="images/board.png" width="1000px">
</div>
<br>

- **Antenne PCB** : C’est l’antenne Bluetooth pour le microcontrôleur STM32WB55. Elle est dite "PCB" (pour "Printed Circuit Board") car elle est intégrée directement dans la plaque d'époxy de la carte. Elle est conçue de sorte à optimiser la réception et l’émission des ondes radio pour la fréquence du Bluetooth soit 2,4 GHz. 

- **Connecteurs Arduino et connecteurs MORPHO** : Des kits d’extensions peuvent être ajoutés simplement grâce à ces connecteurs normalisés. On pourra, par exemple, brancher le kit MEMS inertiel et environnemental X-NUCLEO-IKS01A3 sur la NUCLEO-WB55 :

<br>
<div align="center">
<img alt="Carte Nucleo WB55 et shield X-NUCLEO-IKS01A3" src="images/WB55_IKS01A3.png" width="300px">
</div>
<br>

 Les connecteurs MORPHO permettent d'accéder à la plupart des fonctions du microcontrôleur STM32WB55. Cependant nous utiliserons essentiellement les connecteurs Arduino. Les cartographies ci-dessous situent les fonctions secondaires qui nous seront utiles sur ces derniers. Le brochage de notre NUCLEO-WB55 est presque identique à celui des Arduino UNO **à l'exception notable des sorties PWM** (pour les néophytes, on expliquera un peu plus loin ce qu'est la PWM). Attention donc aux potentielles incompatibilités avec les shields conçus pour les Arduino UNO qui ont besoin de signaux PWM sur des broches bien précises !

<br>
<div align="center">
<img alt="Arduino mapping" src="images/arduino_connectors_mapping.png" width="900px">
</div>
<br>

- **STM32WB55** : C'est le Microcontrôleur qui anime la carte. Comme déjà évoqué, c'est un système sur puce (SoC) avec deux cœurs ARM Cortex, un dédié à l'application de l'utilisateur (un Cortex M4) et l'autre (un Cortex M0+) à la gestion de la communication selon le protocole BLE.

- **Bouton reset** : Lorsqu'on appuie dessus, le microcontrôleur interrompt son travail en cours et lance sa séquence d'initialisation.

- **Level Shifter** : Ce circuit est spécialisé dans la conversion entre deux niveaux de tensions par exemple pour offrir des alimentations en 5V sur certaines broches et en 3.3V sur d'autres.

- **USB User** : C’est le port USB qui nous servira à:

  1. Communiquer avec la machine virtuelle MicroPython
  2. Programmer le système
  3. Alimenter le kit de développement

- **Compartiment CR2032** : Une fois le système programmé, il sera possible d’alimenter le kit par une pile CR2032 afin de rendre l'ensemble portable. Notez bien qu'une "power bank" rechargeable branchée sur le port USB User fera tout aussi bien (en fait, mieux) l'affaire si l'encombrement de votre montage n'est pas contraint.

- **ST-LINK** : C’est l’outil qui permet de programmer le microcontrôleur, notamment d'y installer le [**Firmware MicroPython**](../MicroPython/firmware/index).
   - [**La procédure d'installation du firmware sous Linux est ici**](../MicroPython/install_linux/index).
   - [**La procédure d'installation du firmware sous Windows est ici**](../Micropython/install_win/index).

- **LED utilisateur (User LED)** : Ces trois LED, une rouge, une bleue et une verte, sont accessibles au développeur et pourront être commutées grâce à MicroPython ; nous les utiliserons tout au long des exercices par la suite. Précisons que :

   - La LED **rouge** est sérigraphiée *LED3* sur le PCB
   - La LED **verte** est sérigraphiée *LED2* sur le PCB
   - La LED **bleue** est sérigraphiée *LED1* sur le PCB

## Désignation des broches pour les connecteurs Arduino

Le schémas qui suivent font la correspondance entre les broches de la NUCLEO-WB55 et les fonctions / connecteurs de l'Arduino UNO. Hormis pour les sorties PWM, les connecteurs Arduino des deux cartes offrent les mêmes fonctions sur les mêmes broches.

<br>
<div align="center">
<img alt="Arduino mapping" src="images/pins.png" width="400px">
<img alt="Arduino mapping" src="images/table_Arduino_mapping.png" width="400px"></div>
<br>

## Tension et niveau logique, attention à la tolérance 5V !

Le broches de la NUCLEO-WB55 peuvent être utilisées comme des entrées/sorties numériques :

- **Configurée en sortie numérique**, une broche de la NUCLEO-WB55 est au niveau logique `HAUT` (1, ou encore `True`) lorsque la tension qu'elle délivre est 3,3 V (le maximum possible). Elle est au niveau logique `BAS` (0, ou encore `False`) lorsque la tension qu'elle délivre est 0 V (le minimum possible).<br>
En pratique, vous pourrez piloter un périphérique ayant une tension logique de 5V à l'aide d'un microcontrôleur STM32 de tension logique 3,3V.

- **Configurée en entrée numérique**, une broche est forcée au niveau HAUT lorsque la tension qui lui est appliquée est comprise entre 2V et 3.3V. Toute tension inférieure à 0.8 V produira un niveau BAS.<br>
La plupart des broches d'un microcontrôleur STM32, **mais pas toutes**, tolèrent une tension externe allant jusqu'à 5V. **Nous vous déconseillons donc d'appliquer des tensions supérieures à 3.3V sur les broches à moins d'avoir bien étudié la fiche technique du microcontrôleur !** En cas d'erreur, vous risqueriez purement et simplement de le détruire.


## Fonctions alternatives (ou secondaires) de la NUCLEO-WB55

En plus des entrées/sorties numériques, la NUCLEO-WB55 peut aussi exposer sur ses broches (voire même **sans aucune broche** dans le cas du BLE !) des fonctions alternatives nettement plus complexes, par exemple :

- **Entrée analogique (ADC)**. Une entrée analogique permet de convertir un signal extérieur, une tension, en un nombre entier qui lui est proportionnel. L'ADC accepte en entrée des tensions dont les valeurs sont comprises entre 0 et 3,3V et les convertit en nombres entiers proportionnels, compris entre 0 et 2<sup>12</sup>-1 (soit 4095). L'application la plus courante de l'ADC reste la lecture d'un signal fourni par un capteur analogique, par exemple [**une thermistance**](../MicroPython/grove/thermistance).

- **Bus I2C**. Le bus I2C est un bus série qui permet de connecter une multitude de périphériques (capteurs, actuateurs ...) en utilisant deux fils, une ligne de données (SDA) et un signal d'horloge (SCL) d'une longueur maximum de 1 m. Il n'alimente pas les périphériques en courant / tension, une ligne V<sub>cc</sub> (source de tension) et une ligne GND (masse) sont donc également nécessaires pour chacun d'eux.<br>
Le bus I2C n'est pas approprié pour la manipulation de données en haut débit, par exemple l'écriture dans des mémoires ou la gestion d'afficheurs au-delà d'une certaine résolution. Pour ces applications, on lui préfèrera le bus SPI.<br>
De nombreux périphériques I2C sont disponibles, voir par exemple [**cette centrale à inertie**](../MicroPython/grove/LSM303D).

- **Bus SPI**. Le bus SPI est un bus série comme le bus I2C, mais d'une architecture plus complexe permettant les transferts rapides de gros volumes de données. Chaque périphérique est connecté au bus par 4 lignes : SS (pour *Slave Select*, permettant de l'activer sur le bus), SCLK (signal d’horloge pour cadencer les échanges), MOSI (pour *Master Out, Slave In*, communication du microcontrôleur vers le périphérique) et MISO (pour *Master In, Slave Out*,communication du périphérique vers le microcontrôleur). Les lignes d'alimentations (V<sup>cc</sup> et GND) restent bien sûr requises.<br>
Compte tenu du câblage plus complexe (une ligne SS nécessaire par périphérique) et pour ne pas diviser son débit, on ne branche en général qu'un seul périphérique sur par bus / contrôleur SPI, par exemple [**un module carte SD**](../MicroPython/grove/sd_card_module). 

- **Sortie PWM**. Une sortie PWM (pour *Pulse Width Modulation*, soit *modulation de largeur d’impulsion*) génère un signal en tension carré et périodique dans le temps. La génération de signaux PWM est essentiellement utile pour piloter des moteurs électriques ou des [**servomoteurs**](../MicroPython/grove/servo).<br>
La PWM n'est qu'une fonction offerte par le microcontrôleur, pas un périphérique intégré dans celui-ci. Les broches qui l'exposent sont en fait pilotées par un périphérique complexe capable de fournir bien d'autres fonctions : *un timer* (voir ci-après). 

- **BLE**. Le protocole BLE (pour *Bluetooth Low Energy*, en français *Bluetooth faible consommation énergétique*) permet l’échange bidirectionnel de données à courte distance (une dizaine de mètres) en utilisant des ondes radio sur la bande de fréquence 2,4 GHz, optimisé logiciellement et matériellement pour consommer très peu d'énergie.<br>
Le microcontrôleur STM32WB55 qui anime la NUCLEO-WB55 contient un émetteur-récepteur radio BLE et permet donc de créer des projets et objets communicants. Une présentation détaillée du protocole BLE est disponible [**sur cette page**](../Embedded/ble). Vous trouverez des applications du BLE avec la NUCLEO-WB55 sur [**cette page**](../MicroPython/BLE/index).

- **UART ("Port série")**. L’UART, signifiant *Universal Asynchronous Receiver-Transmitter*, permet l’échange bidirectionnel d’informations  entre le microcontrôleur et un unique module/périphérique externe (liaison point à point). L’échange de données se fait à l’aide de deux fils (RX et TX), après une phase de synchronisation.<br>
L'UART est un protocole limité mais robuste et facile d'utilisation ; il permet d'échanger des commandes et/ou des données en mode texte, lisible par tout un chacun. Il est donc très utilisé pour piloter ou exploiter des périphériques qui renvoient des informations complexes tels que les [**modules GPS**](../MicroPython/grove/gps) ou encore les [**modules WiFi**](../MicroPython/grove/grove_wifi).

- **Les timers et leur canaux**. Les timers figurent parmi les périphériques intégrés les plus complexes du STM32WB55. A la base, un timer n'est rien d'autre qu'un compteur qui revient à zéro après un certain nombre de "tics" à l'image d'un chronomètre. Ils sont utilisés pour un grand nombre d'application telles que : générer des signaux PWM (voir ci-dessus), capturer des signaux qui proviennent de l'extérieur, déclencher des actions à intervalles de temps périodiques, etc.<br>
Les timers sont également connectés à des broches via des canaux d’entrée ou de sortie sur lesquels ils peuvent lire ou générer des signaux modulés en tension (tels que les PWM). Une revue exhaustive des timers du STM32 est hors de propos ici, mais nous les utiliserons dans quelques tutoriels, par exemple [**celui-ci**](../MicroPython/STARTWB55/blink_many).

Tous ces périphériques internes / intégrés, et bien d'autres, se retrouvent dans l'ensemble des microcontrôleurs "modernes". [Le glossaire](../Kit/glossaire) en cite bien d'autres, et chaque nouvelle génération de microcontrôleur en intègre de nouveaux (par exemple, depuis peu, les NPU pour accélérér les firmwares qui réalisent des fonctions d'intelligence artificielle embarquée).<br>
Apprendre à programmer un microcontrôleur c'est essentiellement apprendre à programmer ses périphériques afin que son microprocesseur puisse communiquer avec les LED, les moteurs, les modules GPS... qui constituent l'application (par exemple, un drone) et les piloter. C'est ce que nous allons faire dans tous les exercices, projets et tutoriels proposés sur ce site.

## Mise à jour du microprogramme pour le BLE sur le Cortex M0+

Le **STM32WB55** qui anime notre carte est système sur puce (SoC) avec deux cœurs ARM Cortex, un dédié à l'application de l'utilisateur (un Cortex M4) et l'autre (un Cortex M0+) à la gestion de la communication selon le protocole radiofréquence du BLE.

Tous les exercices sur ce site vont consister à programmer le Cortex M4, qui est facilement accessible avec les outils fournis par MicroPython, par Arduino, par STMicroelectronics, etc. En revanche, le firmware (ou microprogramme) BLE  du Cortex M0+ n'est pas supposé être modifié aisément ou régulièrement par l'utilisateur compte tenu de sa fonction critique pour le bon fonctionnement du SoC.

Vous pourrez cependant être amené à charger un nouveau firmware BLE (appelé aussi "pile BLE") dans le Cortex M0+ pour diverses raisons, notamment pour que la fonction BLE continue de fonctionner correctement avec vos applications suite aux évolutions apportées par STMicroelectronics sur la NUCLEO-WB55. Le cas échéant, la procédure pour reprogrammer la pile BLE est expliquée [par ce tutoriel](../Kit/cube_prog_firmware_ble_hci).


## Pour ne pas détruire votre microcontrôleur et vos modules ...

Cette section est un résumé des conseils avisés disponibles dans l'excellent ouvrage de [*MicroPython et Pyboard - Python sur microcontrôleur : de la prise en main à l'utilisation avancée*](https://www.editions-eni.fr/livre/MicroPython-et-pyboard-python-sur-microcontroleur-de-la-prise-en-main-a-l-utilisation-avancee-9782409022906) de Dominique MEURISSE.

- **Il ne faut jamais travailler alors que la carte NUCLEO est sous tension**.<br>

Chaque fois que vous branchez ou débranchez quelque chose sur une broche, assurez vous que la carte n'est pas alimentée. La plupart des modules conçus par STMicroelectronics, Seeed studio, DF Robot, Adafruit … résistent remarquablement bien aux branchements / débranchements brutaux sous tension, mais d’autres (notamment les modules Bluetooth HC-05 et HC-06, très utilisés mais fabriqués on ne sait où par on ne sait qui) n’attendent que ça pour « griller ».

- **Vérifiez toujours deux fois vos branchements avant de remettre la carte NUCLEO sous tension**.<br>

Avec les connecteurs normalisés de type Grove, Qwiic stemma, micro:bit ... le risque de vous tromper sur le câblage au point de détruire un module est proche de zéro, c’est pour cela que nous les utilisons dans la plupart des tutoriels de ce site. Leur autre avantage (pour les débutants seulement) c’est qu’ils permettent de réaliser des systèmes complexes même lorsqu'on n'a aucune connaissance (ou presque) sur le fonctionnement d’un circuit électrique / électronique. Dit autrement : ils permettent de se concentrer sur la programmation embarquée plutôt que sur la conception électronique.<br>

En revanche, si vous utilisez des câbles dupont, des résistances et des platines d’essai pour câbler votre système, vous entrez sur le territoire des électroniciens avisés ; vous êtes supposé avoir une bonne connaissance des courants, des tensions, des composants. Soyez extrêmement vigilant à ne pas mettre en court-circuit deux broches du microcontrôleur, à ne pas envoyer une surtension sur une broche d’entrée, à ne pas brancher dans le mauvais sens un composant polarisé, etc. Pour ne pas se tromper sur les polarités, un respect des codes-couleurs des fils est essentiel (rouge pour l'alimentation positive, noir pour la masse).

- **Concernant les broches du microcontrôleur, une petite liste de points de vigilance sur les fausses manipulations fatales** : 

1. Ne pas appliquer des tensions supérieures à 3,3V sur des broches configurées en "entrée" (on l'a déjà dit).
2. Ne pas appliquer une tension supérieure à 3,3V sur la broche +3V3 (Ne pas lui appliquer sciemment de tension du tout d'ailleurs, puisque c'est une alimentation !).
3. Ne pas appliquer une tension supérieure à 5V sur la broche +5V (Idem ci-dessus).
4. Ne pas brancher une broche configurée en sortie directement à la masse. Placée dans l'état `HAUT` elle y injecterait un courant d'intensité égale au maximum de ce qu'elle peut délivrer  (25 mA) et elle "fondrait" immédiatement.
5. Ne pas solliciter une broche configurée en sortie numérique pour délivrer un courant d'une intensité dépassant les 25 mA qu'elle peut délivrer. Par exemple, une broche peut piloter un relais qui commande un moteur, lui même alimenté par un circuit qui lui est dédié. Mais une broche **ne peut pas** fournir le courant nécessaire au moteur et **ne doit pas** être connectée "en direct" dessus.<br>
Ce point de vigilance reste pertinent aussi lorsqu'on utilise **plusieurs broches du microcontrôleur simultanément comme sources de courant**. Le courant d’alimentation maximum du STM32WB55 est de 150 mA, il ne peut donc pas délivrer plus à un instant donné sur l'ensemble de ses broches sans être irrémédiablement détruit.<br>
En pratique, que vous utilisiez une ou plusieurs broches comme alimentations, tenez vous loin de la limite des 25 mA ; **1 à 2 mA par broche** est le maximum raisonnable. Si votre montage nécessite plus de courant il faut prévoir un circuit d'alimentation indépendant.
6. Ne pas brancher des broches ensembles ; en court-circuit.
7. Ne pas se tromper sur le signe (et la valeur !) de la tension d'alimentation de la carte NUCLEO si on décide de passer par la broche V<sub>IN</sub>. Elle doit être positive, bien sûr ; un microcontrôleur est un composant polarisé qui fonctionne en courant continu.

Tout ceci peut paraître évident, mais on a vite fait de se retrouver dans l'une de ces situations lorsqu'on conçoit un circuit complexe avec une platine d'essai si on est débutant.


