---
title: Glossaire
description: Glossaire
---

# Glossaire

**ADC** : Analog to Digital Converter (fr : Convertisseur Analogique vers Numérique). Circuit électronique spécialisé dans la conversion de signaux d’entrée analogiques (des tensions éventuellement variables dans le temps, comprises entre 0 et +3,3V) en signaux numériques (des nombres entiers proportionnels à la tension d’entrée à chaque instant et compris entre 0 et 4096 pour un ADC avec une résolution de 12 bits).

**ALU** : Arithmetic and Logic Unit (fr : Unité d’opérations arithmétiques et logiques). L’unité arithmétique et logique est un circuit électronique intégré au CPU et chargé d'effectuer les calculs sur des nombres entiers et/ou binaires.

**Library** (fr : bibliothèque). Une bibliothèque est un ensemble de fichiers qui contiennent le code source (en langages C/C++, Python, etc. ) de fonctions utilisées de façon récurrente par les firmwares : pilotes de périphériques, manipulation de texte, fonctions mathématiques, manipulation de fichiers, etc.

**Arduino** : Écosystème initialement éducatif développé par la société du même nom. Arduino désigne à la fois les cartes programmables (la carte Arduino UNO étant la plus connue), la *bibliothèque* en langage C++ qui simplifie (mais restreint par là même) leur programmation et une IDE épurée. Arduino développe également depuis peu des outils de programmation et des cartes (pour certaines équipées de microcontrôleurs STM32) à l'attention des professionnels. 

**Assembler** (fr : assembleur). Un *programme assembleur* convertit un listing de programme écrit en *langage d'assemblage*  en un fichier binaire, en vue de créer, par exemple, un fichier objet ou un *firmware*.

**ARM Cortex** : Microprocesseur offrant un excellent compromis performances / consommation conçu par la société ARM, pour des applications embarquées. Le microcontrôleur STM32WB55, par exemple, est équipé de deux microprocesseurs de cette famille, un Cortex M0 pour la gestion du BLE et un Cortex M4 qui traite les programmes utilisateur.

**BLE** : Bluetooth Low Energy (fr : Bluetooth faible consommation énergétique). C'est une technique de transmission sans fil créée par Nokia en 2006 sous la forme d’un standard ouvert basé sur le Bluetooth, qu'il complète mais sans le remplacer. Comparé au Bluetooth, le BLE permet un débit du même ordre de grandeur (1 Mbit/s) pour une consommation d'énergie 10 fois moindre.

**Bluetooth** : C'est une norme de communication permettant l'échange bidirectionnel de données à courte distance en utilisant des ondes radio UHF sur la bande de fréquence de 2,4 GHz. Son but est de simplifier les connexions entre les appareils électroniques à proximité en supprimant des liaisons filaires.

**Bootloader** : Système logiciel de démarrage d'un microcontrôleur, permettant la mise à jour de son firmware.

**CAN** : Controller Area Network (fr : pas de version française usuelle). Contrôleur de bus série multi-maître et multiplexé conçu par Bosch pour augmenter la fiabilité et abaisser le coût des réseaux embarqués dans les automobiles. Le protocole CAN gère matériellement les erreurs, les priorités des messages et l’ajout ou la suppression dynamique et « à chaud » de nœuds / stations sur son réseau

**Compiler** (fr : compilateur). Programme qui transforme un code source en un code objet. Généralement, le code source est écrit dans un langage de programmation (le langage source), il est de haut niveau d'abstraction, et facilement compréhensible par l'humain.
Le code objet est généralement écrit en langage de plus bas niveau (appelé langage cible), par exemple un langage d'assemblage ou langage machine, afin de créer un programme exécutable par une machine, comme, par exemple un *firmware*. 
Un langage qui peut être in fine transformé en code binaire après compilation, comme C ou C++ est qualifié de *langage compilé*. MicroPython *n'est pas* un langage compilé, c'est un langage *interprété* (voir *Interpreter*).

**Cortex M** : Famille de cœurs pour microcontrôleurs développée par la société ARM. Les Cortex M sont des microprocesseurs offrant un excellent compromis entre performances et consommation d'énergie pour des applications embarquées et/ou gérant des systèmes électroniques qui doivent rester réactifs (dits "temps-réel") en certaines circonstances.

**CPU** : Central Processing Unit (fr : Unité Centrale de Traitement). Unité centrale de traitement du microcontrôleur, en l’occurrence, pour la NUCLEO-WB55, un microprocesseur Cortex M4 conçu par la société ARM et optimisé pour être intégré dans les microcontrôleurs et autres SoC.

**DAC** : Digital to Analog Converter (fr : Convertisseur Numérique vers Analogique). Un convertisseur numérique-analogique est un circuit électronique dont la fonction est de traduire à la volée une valeur numérique qu'il reçoit en une valeur analogique qui lui est proportionnelle et qu'il transmet aux circuits électroniques suivants, capables uniquement de traiter un signal analogique encodé sous forme d'une tension variable.

**Debugger** (fr : débugueur ou débogueur). Un débogueur est un logiciel qui aide un développeur à analyser les erreurs d'un programme. Pour cela, il permet d'exécuter le programme pas-à-pas — c'est-à-dire le plus souvent ligne par ligne —, d'afficher la valeur des variables à tout moment et de mettre en place des points d'arrêt sur des conditions ou sur des lignes du programme.

**DFU** : Device Firmware Update (fr : Mise à jour firmware du système). Voir également *Bootloader*.

**DMA** : Direct Memory Access (fr : Contrôleur d'accès direct à la mémoire). Circuit électronique connecté sur le même bus interne que le CPU et spécialisé dans les transferts d’informations d’une zone mémoire vers une autre. Le contrôleur DMA fonctionne en parallèle avec le CPU et est nettement plus rapide que celui-ci pour les opérations de copie qu’il réalise.

**DRAM** : Dynamic Random Access Memory (fr : mémoire à accès aléatoire dynamique ou mémoire vive dynamique). La DRAM est un type de mémoire vive compacte et peu dispendieuse. Sans alimentation, la DRAM perd ses données, ce qui la range dans la famille des mémoires volatiles. Sa consommation énergétique élevée en proscrit l'intégration au sein d'un microcontrôleur. La DRAM est essentiellement utilisée dans les "barrettes de mémoires" de nos ordinateurs de bureau.

**Eclipse**. Selon Wikipédia, Eclipse est un projet, décliné et organisé en un ensemble de sous-projets de développements logiciels, de la fondation Eclipse, visant à développer un environnement de production de logiciels libres qui soit extensible, universel et polyvalent, en s'appuyant principalement sur Java.

**EEPROM** : Electrically-Erasable Programmable Read-Only Memory (fr : mémoire morte effaçable électriquement et programmable, aussi appelée E<sup>2</sup>PROM).L'EEPROM est un type de mémoire morte, c'est à dire utilisée pour enregistrer des informations qui ne doivent pas être perdues lorsque l'appareil qui les contient n'est plus alimenté en électricité.

**EXTI** : Extended Interrupts and Events Controller (fr : gestionnaire d'interruption et d'évènements externes). Circuit dédié à la gestion des interruptions et évènements provenant des GPIO et de circuits externes au microcontrôleur.

**Firmware** : Nom donné au fichier binaire qui contient un programme compilé destiné à un microcontrôleur.

**Flash (mémoire)** : Type de mémoire non volatile dans laquelle est placé le code compilé d'un microcontrôleur. Cette mémoire ne s’efface pas lorsque son alimentation électrique est éteinte. Elle est approximativement quatre fois plus dense, càd qu’elle peut stocker plus de bits par unité de surface, que la *RAM*. En contrepartie, la mémoire flash est plus lente à lire et à écrire que la RAM.

**GAP** : Generic Access Profile (fr : Profil d'accès générique). Partie du protocole de communication Bluetooth Low Energy (BLE) chargée du contrôle des connections et du mode "advertising" ("publicité" en traduction littérale). GAP rend votre objet BLE visible au reste du monde et détermine comment il pourra (ou ne pourra pas) interagir avec un autre objet BLE.

**GATT** : Generic Attribute Profile (fr : Profil d'attribut générique). Partie du protocole de communication Bluetooth Low Energy (BLE) chargée des transferts de données entre deux objets connectés, sur la base de concepts appelés *services* et *caractéristiques*.

**GPIO** : General Purpose Input Output (fr : Port D’entrée Sortie à usage Général). Les Ports d’entrée-sortie à usage général désignent l’ensemble des fonctions (programmables) que peuvent remplir les broches du microcontrôleur. Ils permettent de sélectionner les circuits internes au microcontrôleur que l’on souhaite utiliser afin d’interagir avec des composants externes et connectés à celui-ci tels que mémoires, moteurs, capteurs …

**HAL (Library)** : Hardware Abstraction Layer (fr: couche d'abstraction matérielle). *Bibliothèque* développée par STMicroelectronics pour programmer ses microcontrôleurs de la famille *STM32*.

**HCI** : Host Controller Interface (fr :Interface contrôleur hôte). C'est une couche logicielle qui transporte les commandes et les événements entre le Cortex M4 et le Cortex  M0+ contrôleur de la pile de protocoles *BLE* d'un MCU de la famille STM32WB. 

**I2C** : Inter-Integrated Circuit (fr : bus de communication intégré inter-circuits). Contrôleur de bus série fonctionnant selon un protocole inventé par Philips. Tout comme le bus SPI, le bus I2C a été créé pour fournir un moyen simple de transférer des informations numériques entre des capteurs, des actuateurs, des afficheurs et un microcontrôleur.

**IDE** : Integrated Development Environment (fr : environnement de développement intégré). Un environnement de développement intégré est un ensemble d'outils logiciels qui permet d'augmenter la productivité des programmeurs qui développent de nouveaux logiciels.

**IWDG** : Independant Watchdog (fr : chien de garde indépendant). Le chien de garde indépendant est un circuit électronique qui fonctionne avec une horloge dédiée. Il réalise un compte à rebours depuis une valeur maximum programmable jusqu’à zéro. L’IWDG génère un redémarrage (reset) du microcontrôleur lorsque le compte à rebours atteint zéro, sauf si celui-ci est relancé avant cette échéance par une instruction du programme utilisateur.

**Internal RC oscillator** (fr : oscillateur RC interne). Oscillateur à résistance - capacité intégré au microcontrôleur. Ce circuit RC sera utilisé comme référence pour les horloges internes du microcontrôleur en l'absence d'un cristal de quartz plus performant mais aussi plus onéreux (voir *XTAL oscillator*). La fréquence de sortie du circuit RC est généralement multipliée par un circuit *PLL* également intégré au microcontrôleur.

**Interpreter** (fr : interpréteur).  Programme informatique ayant pour tâche d'analyser, de traduire et d'exécuter d'autres programmes informatiques. Les langages dont les programmes sont exécutés par un interpréteur sont généralement qualifiés de "langages interprétés". MicroPython est un langage interprété. Son interpréteur est un firmware installé dans le microcontrôleur hôte, chargé de compiler à la volée les scripts MicroPython rédigés en mode texte afin de les traduire en instructions binaires exécutables. 

**Interrupt** (fr : Interruption). Mécanisme d'exécution intégré à un microcontrôleur qui lui permet de réagir presque instantanément à des signaux provenant de périphériques (des capteurs par exemple) en exécutant de façon prioritaire des programmes répondant à ces signaux. Plus précisément, le programme exécuté par le microcontrôleur juste avant l'interruption est mis en pause,  puis le microcontrôleur exécute un programme spécialement dédié à celle-ci, appelé "routine de service de l'interruption" (ou ISR pour "Interrupt Service Routine", en anglais). Une fois que l'ISR a terminé, l'exécution du programme principal reprend son cours.

**I/O** : Inputs / Outputs (fr : entrées / sorties). Voir *GPIO*.

**ISR** : Interrupt Service Routine (fr : routine de service d'une interruption). Programme lancé par le microcontrôleur lorsqu'une *interruption* survient, renvoyée par un périphérique (par exemple : le signal d'un capteur, la fin d'une communication avec un UART, etc.). Le code de l'ISR est écrit par le programmeur pour traiter un évènement particulier et "asynchrone" au sens "non planifié" (par exemple : allumer les phares d'un robot lorsque la luminosité ambiante devient plus faible qu'un seuil donné, déployer un airbag en cas de choc, etc.). Ce code doit être aussi simple que possible pour ne pas retarder l'exécution d'autres ISR qui pourraient survenir ou paralyser durablement le programme principal, qui est mis en pause en attendant que toutes les ISR soient traitées.

**JTAG/SWD** : Joint Test Action Group / Serial Wire Debug (fr : pas de version française usuelle). Interfaces (bus) de communication avec les fonctions de débogage en temps réel intégrées dans l’architecture ARM Cortex.

**LCD** : Liquid Crystals Display (fr : afficheur à cristaux liquides).

**LED** : Luminescent Electronic Diode (fr : Diode Électroluminescente). **Attention**, les LED sont polarisées ; si vous les branchez incorrectement, vous les détruirez probablement. La patte la plus longue d'une LED doit être connectée au "+" et la plus courte au "-".

**Library (bibliothèque)**. Une bibliothèque est un ensemble de fonctions et définitions (pour les langages C/C++ ou autres) qui contiennent des fonctions utilisées de façon récurrente par les firmwares : pilotes de périphériques, manipulation de texte, fonctions mathématiques, manipulation de fichiers, etc.

**Linker** (fr : éditeur de lien). L’édition des liens permet de créer un *firmware* à partir de fichiers objets (binaires) produits par la *compilation* ou *l'assemblage* et des routines provenant de bibliothèques statiques.

**LL (Library)** : Low Layer (fr: couche d'abstraction de bas niveau). *Bibliothèque* développée par STMicroelectronics pour programmer ses microcontrôleurs de la famille *STM32*.

**LoRa** : Long Range (fr : longue portée). LoRa est le nom donné à la technologie de modulation des ondes radios sur laquelle sont basés les réseaux longue portée et bas débit *LoRaWAN*. *LoRaWAN* est un protocole tandis que LoRa fait référence à la couche physique du réseau.

**LoRaWAN** :  Long-range wide area network (fr: réseau étendu et longue portée à basse consommation). Protocole de communication radiofréquence grande portée et faible consommation développé pour les objets connectés fondé sur la technologie LoRa. LoRaWAN appartient à la famille plus large des technologies *Low Power Wide Area Networks* (LPWAN) ou *réseau étendu à basse consommation* en français.

**LPUART** : *Low Power* Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel *à basse consommation*). Voir *UART*.

**MCU** : Microcontroler Unit (fr : Unité Microcontrôleur), également **µC**. Un microcontrôleur est un « système sur puce » (SoC) car il rassemble sur une même puce de silicium un microprocesseur et d’autres composants et périphériques : des bus, des horloges, de la mémoire, des ports entrées-sorties généralistes, etc.

**MBED/MBED OS** : *IDE* et *bibliothèques* reposant sur le langage C/C++ développées ARM ayant comme objectif, tout comme Arduino, de simplifier la tâche des programmeurs de systèmes embarqués mais aussi des *objets connectés*. MBED permet de tirer mieux parti des ressources des microcontrôleurs que ne le fait Arduino, au prix d'une programmation un peu plus complexe. 

**MEMS** : Micro Electro Mechanical System (fr : système micro-électro-mécanique). Capteurs physiques miniaturisés. Les capteurs MEMS ont révolutionné les applications électroniques et sont à la base – avec des capteurs d’image et de distance sans cesse plus performants – de toutes les fonctions ludiques et avancées des smartphones et des objets connectés en général. Ils sont également les composants clés des centrales inertielles des drones et des casques et manettes de réalité virtuelle. Les MEMS les plus communs sont :

- L'accéléromètre tridimensionnel
- Le gyroscope tridimensionnel
- Le magnétomètre tridimensionnel
- Le capteur de pression
- Le capteur de température
- Le capteur d'humidité relative

**MicroPython** : Implémentation légère et efficace du langage de programmation Python 3 incluant un petit sous-ensemble de la bibliothèque standard Python et optimisée pour fonctionner sur des microcontrôleurs disposant de peu de mémoire.

**MORPHO** : Type de connecteur propriétaire de STMicroelectronics sur les cartes NUCLEO, donnant accès à presque toutes les fonctions du microcontrôleur considéré.

**MOSFET** : Metal-Oxide-Semiconductor Field Effect Transistor (fr : Transistor Métal-Oxyde-Semiconducteur à effet de champ). Famille de composants miniaturisés encore majoritairement utilisés dans l'architecture des puces électroniques. Les MOSFET se comportent comme des interrupteurs minuscules commandés par une tension électrique. Les MOSFET sont peu à peu remplacés par des transistors d'autres types pour les besoins d'une miniaturisation encore plus poussée.

**NUCLEO** : Nom commercial donné aux cartes de développement conçues par STMicroelectronics, qui permettent d'évaluer divers composants développés par cette société (microcontrôleurs, capteurs MEMS ...).

**NVIC** : Nested Vectored Interrupts Controller (fr : Contrôleur d'Interruptions Vectorisées et Imbriquées). Circuit intégré au CPU ARM Cortex chargé de la gestion des interruptions. Il optimise en particulier la réactivité du système aux interruptions en leur assignant des priorités et en gérant les situations où plusieurs interruptions surviennent dans un intervalle de temps très court.

**NVM** : Non Volatile Memories (fr : mémoires non volatiles). Une mémoire non volatile est une mémoire qui conserve ses données en l'absence d'alimentation électrique. Parmi les NVM, on distingue les mémoires mortes (*ROM*), certaines mémoires de type RAM non volatiles (NVRAM pour non-volatile RAM) ou encore les *mémoires flash* et les *EEPROM*.

**OLED** : Organic Luminescent Electronic Diode (fr : Diode Électroluminescente Organique).

**One-Wire** (fr : "Un-câble"). Aussi connu sous le nom de bus Dallas ou 1-Wire, c’est un bus conçu par Dallas Semiconductor qui permet de connecter (en série, parallèle ou en étoile) des composants avec seulement deux fils (un fil de données et un fil de masse).

**PLL** : Phase-Locked Loop (fr : boucle à phase asservie, ou boucle à verrouillage de phase). Une PLL est un montage électronique permettant d'asservir la phase ou la fréquence de sortie d'un système sur la phase ou la fréquence d'un signal d'entrée. Elle peut aussi asservir une fréquence de sortie sur un multiple de la fréquence d'entrée. Les PLL intégrées dans les microcontrôleurs servent de *multiplicateurs de fréquence d'horloge*, fréquence fournie par un circuit intégré oscillant RC (voir *Int. RC oscillator*) ou par un cristal oscillateur piézoélectrique de quartz (voir *XTAL oscillator*).

**PWM** : Pulse Width Modulation (fr : modulation en largeur d’impulsion). La modulation en largeur d’impulsion est une technique permettant de faire varier le potentiel électrique d’une broche entre 0 et +3,3V (pour le microcontrôleur concerné) selon un signal rectangulaire de fréquence et de rapport cyclique (ratio entre le temps où le signal est à 0V et celui où il est à +3,3V au sein d’une période) dynamiquement ajustables. La génération de signaux PWM est l’une des fonctions assurées par les timers, essentiellement pour piloter des moteurs électriques.

**Power Supply ou PWR** (fr : Alimentation). Désigne toute la partie de l’architecture d'un MCU dédiée à la gestion de l'alimentation et de ses modes basse consommation. Les circuits PWR offrent les fonctions pour préserver certains registres ainsi que la RTC lorsque le CPU est placé dans des modes avancés d’économie d’énergie.

**RAM** : Random Access Memory (fr : mémoire à accès aléatoire ou mémoire vive). La mémoire vive intégrée aux microcontrôleurs est nettement plus performante que leur mémoire flash mais est disponible en quantité plus réduite du fait de son architecture plus complexe et moins dense. Toutes les informations enregistrées dans une mémoire RAM sont perdues lorsque son alimentation électrique est interrompue ; c'est une mémoire *volatile*. Elle se décline essentiellement en deux architectures, *SRAM* - plus adaptée à l'intégration dans les microcontrôleurs - et *DRAM* - moins chère, plus dense, plus lente et plus énergivore.<br>
Il existe également des *NVRAM*, pour *non-volatile RAM*, qui ne s'effacent pas hors alimentation électrique.

**RCC** : Reset and Clock Control (fr : contrôle des reset et horloges). Désigne toute la logique et la circuiterie dédiées à la génération et à la distribution de signaux d’horloges et d’alimentations dans le CPU et pour les périphériques du microcontrôleur. 

**Registers** (fr : registres). Mémoires très rapides constituées de bascules de transistors et de tailles limitées (8, 16 ou 32 bits en général) intégrées dans un CPU. Les registres sont utilisés par le microprocesseur pour calculer et manipuler instructions et données des programmes pendant leur exécution, mais aussi pour interagir avec les périphériques du microcontrôleur.

**REPL** : Read Evaluate Print Loop (fr : Boucle de lecture, évaluation et impression). Le mode REPL permet d'envoyer des instructions ou des lignes d'instructions à l'interpréteur MicroPython en les tapant dans un terminal série (type PuTTY) connecté à un UART/USART du microcontrôleur hôte. L'interpréteur renvoie des messages d'acquittement, d'information, d'erreur ... dans le terminal.

**RFID** : Radio Frequency Identification (fr : radio-identification). La radio-identification est une méthode pour mémoriser et récupérer des données à distance en utilisant des marqueurs appelés « radio-étiquettes » (« RFID tag » ou « RFID transponder » en anglais).

**ROM** : Read Only Memory (fr : mémoire en lecture seule). Mémoire qui peut être extrêmement dense car potentiellement encodée dans la structure même du microcontrôleur lors de sa fabrication. Les instructions exécutées par le CPU sont par exemple répertoriées dans une ROM. Une ROM est donc une *mémoire non-volatile (NVM)* ; elle ne s'efface pas lorsque le système n'est plus alimenté électriquement.

**RTC** : Real-Time Clock (fr : Horloge Temps-Réel). Circuit électronique remplissant les fonctions d’une horloge très précise pour des problématiques d’horodatage. Suivant sa complexité, la RTC pourra aussi gérer des fonctions plus complexes comme des alarmes déclenchées suivant un calendrier, voire mettre le microcontrôleur en sommeil et le réveiller si elle est intégrée dans celui-ci.

**SoC** : System on Chip (fr : Système sur Puce). Désigne un système complet embarqué sur une seule puce (« circuit intégré ») et pouvant comprendre de la mémoire, un ou plusieurs microprocesseurs, des périphériques d'interface ou tout autre composant nécessaire à la réalisation d’une fonction requise. Les microcontrôleurs sont des SoC.

**SPI** : Serial Peripheral Interface (fr : Interface Série pour Périphériques). Contrôleur de bus série fonctionnant selon un protocole inventé par Motorola.  Un bus série de ce type permet la connexion, de type maître-esclave, de plusieurs circuits disposant d’interfaces compatibles avec trois fils de liaisons pour les données. Une ligne supplémentaire est requise pour chaque esclave connecté au bus, afin de l'adresser (un seul esclave est activé à chaque transaction sur le bus).

**SRAM** : Static Random Access Memory (fr : mémoire à accès aléatoire statique ou mémoire vive statique). La SRAM est un type de mémoire vive utilisant des bascules (de transistors) pour mémoriser les données. Mais contrairement à la mémoire dynamique *DRAM*, elle n'a pas besoin de rafraîchir périodiquement son contenu. Comme la mémoire dynamique, elle est *volatile* : elle ne peut se passer d'alimentation sous peine de voir les informations effacées irrémédiablement. La SRAM est plus onéreuse et moins dense, mais beaucoup moins énergivore et plus rapide que la DRAM. Elle est donc réservée aux applications qui requièrent soit des temps d'accès courts, soit une faible consommation comme les microcontrôleurs.

**STM32** : Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour des processeurs 32 bits ARM Cortex M.

**STM32WB** :  Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour des processeurs 32 bits ARM Cortex M intégrant un émetteur-récepteur radiofréquence pour, notamment, le protocole *BLE*.

**STM32WL** :  Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour des processeurs 32 bits ARM Cortex M intégrant un émetteur-récepteur radiofréquence pour, notamment, le protocole *LoRa*.

**STM32Cube** : Ensemble de bibliothèques et de logiciels conçus pour programmer les microcontrôleurs de la famille *STM32* de la société franco-italienne STMicroelectronics.

**STM32CubeIDE** : *IDE* conçue sur la base du projet open source *Eclipse* pour programmer les microcontrôleurs de la famille *STM32* de la société franco-italienne STMicroelectronics.

**STM32duino** : Extension des bibliothèques *Arduino* aux cartes de prototypage produites par la société franco-italienne STMicroelectronics ainsi qu'à de nombreuses cartes produites par d'autres sociétés utilisant des microcontrôleurs de la famille *STM32*. 

**STM8** : Famille de microcontrôleurs produits par la société franco-italienne STMicroelectronics, construits autour de microprocesseurs 8 bits propriétaires.

**ST-LINK** : Programmeur fabriqué par STMicroelectronics pour transférer un firmware dans la mémoire non-volatile (FLASH en général) d’un microcontrôleur.

**SysTick timer** (fr : Timer système). Timer précis et intégré au ARM Cortex, capable de générer une base de temps précise et régulière, dont les performances sont normalisées. Il est essentiellement conçu pour cadencer les éventuels systèmes d’exploitation en temps réel qui pourraient être installés sur le microcontrôleur.

**Timer** ou TIM (fr : Compteur). Circuit électronique basé sur un compteur qui coordonne des fonctions variées (selon son architecture) telles que la lecture de signaux extérieurs variables dans le temps, la génération de signaux modulés en largeur d’impulsion (PWM & contrôle de moteurs électriques), la génération d’évènements périodiques, etc.

**Toolchain** (fr : Chaîne de compilation). Une chaîne de compilation désigne l'ensemble des logiciels utilisés dans le processus de compilation d'un programme, pour un processeur donné. Le compilateur n'est qu'un élément de cette chaîne, laquelle varie selon l'architecture matérielle cible. On y trouve généralement un *assembleur*, un *compilateur*, un *éditeur de liens*, un *débogueur*, un *simulateur*, un *programmeur de firmware* (voir *ST-LINK*) ...

**UART** : Universal Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Asynchrone Universel). En langage courant, l'UART un composant utilisé pour faire la liaison entre le microcontrôleur et l’un de ses ports série pour échanger des messages (souvent du texte) avec certains modules au fonctionnement complexe tels que les récepteurs GPS ou les modules radiofréquence Wi-Fi.

**USART** : Universal Synchronous/Asynchronous Receiver Transmitter (fr : Récepteur-Transmetteur Synchrone/Asynchrone Universel). Voir *UART*.

**USB** : Universal Serial Bus (fr : contrôleur de bus série universel). Contrôleur intégré au microcontrôleur pour gérer le célèbre protocole série éponyme que l'on trouve désormais sur quasiment tous les objets connectés, les ordinateurs, etc.

**Wi-Fi** : Wireless Fidelity (fr : fidélité sans fil). Le Wi-Fi, ou Wifi, est un réseau local lancé en 1999 qui utilise des ondes radio pour relier entre eux, sans fil, plusieurs appareils informatiques dans le but de faciliter la transmission de données. Régi par les normes IEEE 802.11, le Wifi est principalement utilisé pour relier des appareils (ordinateurs portables, smartphones, etc.) à des liaisons haut débit (d'après [cette source](https://www.journaldunet.fr/web-tech/dictionnaire-de-l-iot/1203421-wifi-definition-signification-et-role-avec-la-5g-20200716))

**WWDG** : Window Watchdog (fr : Chien de garde sur Intervalle). Circuit intégré au microcontrôleur chargé de veiller au respect du temps d'exécution d'un programme ou d'une portion de programme. Si le temps d'exécution du code surveillé est plus court qu'une valeur inférieure précisée, ou plus long qu'une valeur supérieure précisée, le WWDG génère un redémarrage (reset) du microcontrôleur.

**XTAL oscillator** (fr : oscillateur à cristal de quartz piézoélectrique). Composant intégrant un cristal de quartz externe au microcontrôleur. On utilise la propriété piézoélectrique du quartz cristallisé pour générer une source de fréquence de qualité qui cadencera les horloges internes du microcontrôleur. Elle est généralement multipliée par un circuit *PLL* intégré dans celui-ci.
