# Comment ajouter ou éditer un exercice ?

## La structure du dépôt Gitlab

* Le répertoire `_docs` contient les différents exercices.
* Le document `_data/toc.yml` contient la description du sommaire (à gauche) du site.
* Le document `_config.yml` contient les paramètres du site utilisés pour la génération.
* Le répertoire `pages` contient les patrons des pages HTML générées.
* Le répertoire `_includes` contient les patrons des fragments HTML utilisés pour la génération des pages HTML.
* Les pages HTML sont générées dans le répertoire `_site`
* Le répertoire `assets` contient tous les _assets_ (`.css`, `.js`, `.svg`, `.png` ...) référencés dans les pages HTML générées. Il contient les images qui sont réutilisables un peu partout dans le site.

> A noter: le dépôt https://gitlab.com/stm32python/assets contient les assets communs à tous les sites (fr, en ...)

## Corrections, ajouts et améliorations d'exercices

Créez dans le répertoire `_docs` un nouveau répertoire contenant les documents Markdown et les images de votre exercice.

Les images peuvent être placées dans le sous-répertoire de `_docs` créé pour l'exercice ou bien mis dans le répertoire `assets/images` si les images sont réutilisables un peu partout dans le site.

Complétez le document `_data/toc.yml` pour ajouter votre exercice au sommaire (à gauche) du site. 

## Edition depuis votre poste de travail

L'édition des documents peut se faire directement depuis le site https://gitlab.com/stm32python/fr . Cependant, chaque modification déclenche le pipeline CI de regénération des pages. Cette opération est lente, consomme le crédit mensuel de CI/CD (il y a un risque d'atteindre la limite) et consomme de l'énergie.

Vous pouvez éditer depuis votre poste de travail plusieurs pages du site et vérifier le rendu depuis votre poste de travail.

> Pour éditer les documents, il est recommander d'utiliser un IDE comme [VSC (Visual Code Studio)](https://code.visualstudio.com/download).

Il faut préalablement [générer une paire clé SSH (publique et privée)](https://docs.gitlab.com/ee/user/ssh.html#generate-an-ssh-key-pair).

```bash
ssh-keygen -t ed25519 -C "$USER@gitlab.com"
```
Il faut copier la clé privée dans le répertoire `~/.ssh` et s'assurer que la clé n'est accessible que de vous avec `chmod 600 ~/.ssh/$USER.gitlab`

Il faut ensuite ajouter la clé publique dans [votre profil Gitlab](https://gitlab.com/-/profile/keys).

Il faut ensuite lancer l'agent et lui ajouter l'identité après chaque redémarrage de la machine (Linux ou WSL)
```bash
eval $(ssh-agent)
ssh-add -i ~/.ssh/$USER.gitlab
```



Pour cela, vous devez préalablement [installer `ruby` version `2.7` (`ruby@2.7`)](https://www.ruby-lang.org/fr/documentation/installation/) puis le générateur `jekyll` de la façon suivante:

```bash
git clone git@gitlab.com:stm32python/fr.git
cd fr
ruby -v
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Ouvrez la page `http://127.0.0.1:4000/fr/`


Récupérez les dernières modifications avec de commencer des éditions :

```bash
cd fr
git pull
```

Editez les pages; le générateur Jekyll sera relancé à chaque sauvegarde. Fraichissez la page web dans votre navigateur pour constater les modifications effectuées.

Une fois que les ajouts et les modifications sont satisfaisantes, poussez les modifications vers le serveur central.
```bash
git add *
git commit -m "amélioration de ceci cela"
git push
```

Une fois le `push` effectué, vérifiez l'état du [pipeline CI/CD](https://gitlab.com/stm32python/fr/pipelines).

Visualisez les pages du site en ligne https://stm32python.gitlab.io/fr/

Une autre possibilité est d'utiliser l'[image Docker de `jekyll`](https://GitHub.com/envygeeks/jekyll-docker/blob/master/README.md) pour tester le site en local.

```bash
export JEKYLL_VERSION=3.5
docker run --name jekyll \
    --publish 127.0.0.1:4000:4000/tcp  \
    --volume="$PWD:/srv/jekyll:Z" \
    --volume="$PWD/vendor/bundle:/usr/local/bundle:Z" \
    -it jekyll/jekyll:$JEKYLL_VERSION \
    jekyll serve  --incremental
``` 

Quand le serveur est prêt, ouvrez la page `http://127.0.0.1:4000/fr/`

Pour relancer le serveur arrêté, redémarrez le conteneur précédenment créé avec :
```bash
docker start -i jekyll
```

Pour mettre à jour les dépendances, démarrez le conteneur suivant avec :
```bash
export JEKYLL_VERSION=3.5
docker run --rm \
  --volume="$PWD:/srv/jekyll:Z" \
  --volume="$PWD/vendor/bundle:/usr/local/bundle:Z" \  
  -it jekyll/jekyll:$JEKYLL_VERSION \
  bundle update
```

## Que faire si le [pipeline CI/CD](https://gitlab.com/stm32python/fr/pipelines) échoue ?

Il faut regarder le job qui est en erreur

Les deux principale raisons sont : 
* Erreur de la génération : Il y a une erreur de syntaxe dans un des fichiers MD du site
* "No more CI minutes available » : Le crédit mensuel de minutes de CI/CD est épuisé [détail](https://about.gitlab.com/pricing/faq-consumption-cicd/).

Pour ce deuxième cas, ll faut globalement éviter de pousser (`git push`) de petites modifications/retouches car cela épuise le crédit mensuel de minutes de CI.

La bonne pratique est :
* de travailler le plus possible en local (sans passer par le web editor)
* de visualiser le résultat en local
* de pousser (`git push`) plusieurs commits ensemble. Il n’y aura alors qu’un seul déclenchement de la CI.
