# Pages de documentation de STM32Python

## Getting Started
* [Install Jekyll](https://jekyllrb.com/docs/installation/)


### Clone the repo
```bash
mkdir ~/gitlab/stm32python
git clone git@gitlab.com:stm32python/fr.git
cd fr
```

### Start locally
```bash
cd ~/gitlab/stm32python/fr
bundle update
bundle install
bundle exec jekyll build
bundle exec jekyll serve
```

Browse http://127.0.0.1:4000/fr/

### Start locally with Docker

```bash
cd ~/gitlab/stm32python/fr
```

Move the `.git/` directory since the Jekyll container can not `chown` it (a fix may be helpful)
```bash
mv .git ../fr_dot_git_svg
```

Build the site
```bash
export JEKYLL_VERSION=3.5
docker run --rm \
  --volume="$PWD:/srv/jekyll" \
  --volume="$PWD/vendor/bundle:/usr/local/bundle" \
  -it jekyll/jekyll:$JEKYLL_VERSION \
  jekyll build
```

Build the site then serve it
```bash
export JEKYLL_VERSION=3.5
docker run --rm \
  -p 127.0.0.1:4000:4000 \
  --volume="$PWD:/srv/jekyll" \
  --volume="$PWD/vendor/bundle:/usr/local/bundle" \
  -it jekyll/jekyll:$JEKYLL_VERSION \
  jekyll serve  --incremental
```

Browse http://127.0.0.1:4000/fr/

Build incrementally the site and serve it
```bash
export JEKYLL_VERSION=3.4
docker run --rm \
  -p 127.0.0.1:4000:4000 \
  --volume="$PWD:/srv/jekyll" \
  --volume="$PWD/vendor/bundle:/usr/local/bundle" \
  -it jekyll/jekyll:$JEKYLL_VERSION \
  jekyll serve  --incremental
```

Browse http://127.0.0.1:4000/fr/

Restore the `.git/` directory before `git add, git commit, git push` your changes.
```bash
mv ../fr_dot_git_svg .git/
```

### Alternate with Ruby Docker image 

```bash
cd ~/gitlab/stm32python/fr
docker run -it --rm -v $PWD:/home/fr ruby:2.7 bash
```

```bash
cd /home/fr
bundle update --bundler
bundle install
mkdir public
bundle exec jekyll build -d public
exit
```

```bash
cd public
ls -al
```


### Deploy

Push your repository and changes to GitLab. Then check the CI pipeline https://gitlab.com/stm32python/fr/-/pipelines

## References
* [Contributeurs](CONTRIBUTORS.md)
* [TODO List](TODO.md)
* [CHANGELOG](CHANGELOG.md)
* [LICENSE](LICENSE.md)

## Troubleshooting

[ci]: https://about.gitlab.com/gitlab-ci/
[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages
[jekyll-docker]: https://GitHub.com/envygeeks/jekyll-docker/blob/master/README.md