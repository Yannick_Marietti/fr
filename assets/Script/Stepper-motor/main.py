import pyb
from pyb import Pin, Timer
import time

#Variables globales
etat_moteur = 0
sens_moteur = 0

#BP (en entrée + pull up)
sw1 = pyb.Pin('SW1')
sw1.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)
sw2 = pyb.Pin('SW2')
sw2.init(pyb.Pin.IN, pyb.Pin.PULL_UP, af=-1)

#LED (LED de la carte NUCLEO)
led_bleu = pyb.LED(3)
led_vert = pyb.LED(2)
led_rouge = pyb.LED(1)

#GPIO qui controle le relais/transistor
A = pyb.Pin('D0', Pin.OUT_PP)
B = pyb.Pin('D1', Pin.OUT_PP)
C = pyb.Pin('D2', Pin.OUT_PP)
D = pyb.Pin('D3', Pin.OUT_PP)

moteur = (A, B, C, D)

#Interruption de SW1
def ITbutton1(line):
    #Variables globales
    global etat_moteur
    #Etat moteur à 0 ou 1
    if(etat_moteur == 1):
        etat_moteur = 0
    else:
        etat_moteur = 1

def ITbutton2(line):
    #Variables globales
    global sens_moteur
    #Sens moteur à 0 (sens horaire) ou 1 (sens anti-horaire)
    if(sens_moteur == 1):
        sens_moteur = 0
    else:
        sens_moteur = 1
    #Faire un reset du moteur
    moteur_stop()

#Initialisation des vecteurs d'interruption
irq_1 = pyb.ExtInt(sw1, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton1)
irq_2 = pyb.ExtInt(sw2, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, ITbutton2)

def moteur_stop():
    #Gestion des LED
    led_bleu.off()
    led_vert.off()
    led_rouge.on()
    #Eteind toutes les sorties
    for i in range(0, 4, 1):
        moteur[i].low()

def moteur_tourne(sens, vitesse):
    i = 0

    #Tourner dans le sens horaire
    if(sens == 0):
        #Gestion des LED
        led_bleu.off()
        led_vert.on()
        led_rouge.off()
        #Dans le sens horaire
        for i in range(0, 4, 1):
            #Allume le segment 1
            moteur[i].on()
            time.sleep_ms(vitesse)
            #Allume le segment 2
            if(i == 3):
                moteur[0].on()
            else:
                moteur[i+1].on()
            time.sleep_ms(vitesse)
            #Eteind le segment 1
            moteur[i].off()
            time.sleep_ms(vitesse)
    
    #Tourner dans le sens anti-horaire
    if(sens == 1):
        #Gestion des LED
        led_bleu.on()
        led_vert.off()
        led_rouge.off()
        #Dans le sens anti-horaire
        for i in range(3, -1, -1):
            #Allume le segment 2
            moteur[i].on()
            time.sleep_ms(vitesse)
            #Allume le segment 1
            if(i == 0):
                moteur[3].on()
            else:
                moteur[i-1].on()
            time.sleep_ms(vitesse)
            #Eteind le segment 2
            moteur[i].off()
            time.sleep_ms(vitesse)

#Vitesse de rotation du moteur. /!\ ne peut être inférieure à 3ms /!\
vitesse = 3

#Boucle infinie
while True:
    if(etat_moteur == 0):
        moteur_stop()
        
    if(etat_moteur == 1):
        moteur_tourne(sens_moteur, vitesse)
        